# coding: utf-8
from com.ziclix.python.sql import zxJDBC

jdbc_uri = "jdbc:odbc:sqpub"
jdbc_driver = "sun.jdbc.odbc.JdbcOdbcDriver"

conn = zxJDBC.connect(jdbc_uri, None, None, jdbc_driver)
cursor = conn.cursor()

query = u"select 기본Size, 기본Size단가, 평당단가 from tb단가_스캔"
cursor.execute(query)

output = open("tblCostScan_insert.sql", "w")

for entry in cursor:
    new_query = "insert into \"tblCostScan\" (size, \"sizeUnitCost\", \"pUnitCost\") values ({0}, {1}, {2});\n".format(*entry)

    output.write(new_query)

output.close()