# coding: utf-8
from com.ziclix.python.sql import zxJDBC

jdbc_uri = "jdbc:odbc:sqpub"
jdbc_driver = "sun.jdbc.odbc.JdbcOdbcDriver"

conn = zxJDBC.connect(jdbc_uri, None, None, jdbc_driver)
cursor = conn.cursor()

query = u"select 판형, 종류, 단가, 기본꼭지, 기본단가, 기본수량 from tb단가_제본_중철"
cursor.execute(query)

output = open("tblCostBindingChul_insert.sql", "w")

for entry in cursor:
    new_query = "insert into \"tblCostBindingChul\" (format, method, \"unitCost\", \"dltPage\", \"dltCost\", \"dltQuantity\") values ('{0}', '{1}', {2}, {3}, {4}, {5});\n".format(*entry)

    output.write(new_query)

output.close()