# coding: utf-8
from com.ziclix.python.sql import zxJDBC

jdbc_uri = "jdbc:odbc:sqpub"
jdbc_driver = "sun.jdbc.odbc.JdbcOdbcDriver"

conn = zxJDBC.connect(jdbc_uri, None, None, jdbc_driver)
cursor = conn.cursor()

query = u"select 도수, 단가 from tb단가_교정인쇄"
cursor.execute(query)

output = open("tblCostProofreading_insert.sql", "w")

for entry in cursor:
    new_query = "insert into \"tblCostProofreading\" (\"colorCnt\", \"unitCost\") values ({0}, {1});\n".format(*entry)

    output.write(new_query)

output.close()