# coding: utf-8
from com.ziclix.python.sql import zxJDBC

jdbc_uri = "jdbc:odbc:sqpub"
jdbc_driver = "sun.jdbc.odbc.JdbcOdbcDriver"

conn = zxJDBC.connect(jdbc_uri, None, None, jdbc_driver)
cursor = conn.cursor()

trade_company_query = u"select 상호, 전화번호, 팩스, EMail, 담당자, 핸드폰, 등록번호, 성명, 주소, 업태, 종목, 금액계산방법, 부가세종류, 부가세계산방법, 구분, 비고, 거래처id from tb거래처 where 거래처id > 0 order by 거래처id asc"
cursor.execute(trade_company_query)

output = open("tblTradeCompanyInfo_insert.sql", "w")

for entry in cursor:
    new_query = "insert into \"tblTradeCompanyInfo\" (idx, name, ceo, tel, fax, num, email, addr, manager, mobile, uptae, jm, \"moneyCalcWay\", \"vatCalcWay\", \"vatCalcSel\", gubun, etc) values ({16}, '{0}','{7}', '{1}', '{2}', '{6}', '{3}', '{8}', '{4}', '{5}', '{9}', '{10}', {11}, {13}, {12}, '{14}', '{15}');\n".format(*entry)

    output.write(new_query)

output.close()