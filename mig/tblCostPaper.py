# coding: utf-8
from com.ziclix.python.sql import zxJDBC

jdbc_uri = "jdbc:odbc:sqpub"
jdbc_driver = "sun.jdbc.odbc.JdbcOdbcDriver"

conn = zxJDBC.connect(jdbc_uri, None, None, jdbc_driver)
cursor = conn.cursor()

query = u"select 종이id, 종이종류, 계열, 단위, 공장도가, 할인율_원가, 단가_원가, 비고 from tb단가_종이"
cursor.execute(query)

output = open("tblCostPaper_insert.sql", "w")

for entry in cursor:
    new_query = "insert into \"tblCostPaper\" (idx, name, faction, paper_unit, free_at_factory, discount, \"unitCost\", etc) values ({0}, '{1}', '{2}', '{3}', {4}, {5}, {6}, '{7}');\n".format(*entry)

    output.write(new_query)

output.close()