# coding: utf-8
from com.ziclix.python.sql import zxJDBC

jdbc_uri = "jdbc:odbc:sqpub"
jdbc_driver = "sun.jdbc.odbc.JdbcOdbcDriver"

conn = zxJDBC.connect(jdbc_uri, None, None, jdbc_driver)
cursor = conn.cursor()

cmbProcessCompany_query = u"select 공정id, 거래처id from cmb공정_거래처"
cursor.execute(cmbProcessCompany_query)

output = open("tblProcessCompany_insert.sql", "w")

for entry in cursor:
    new_query = "insert into \"tblProcessCompany\" (process_idx, trade_idx) values ({0}, {1});\n".format(*entry)

    output.write(new_query)

output.close()