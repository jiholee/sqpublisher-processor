# coding: utf-8
from com.ziclix.python.sql import zxJDBC

jdbc_uri = "jdbc:odbc:sqpub"
jdbc_driver = "sun.jdbc.odbc.JdbcOdbcDriver"

conn = zxJDBC.connect(jdbc_uri, None, None, jdbc_driver)
cursor = conn.cursor()

base_company_query = u"select 상호, 전화번호, 팩스, EMail, 담당자, 핸드폰, 등록번호, 성명, 주소, 업태, 종목, 금액계산방법, 부가세종류, 부가세계산방법 from tb거래처 where 거래처id=0"
cursor.execute(base_company_query)
base_company_record = cursor.fetchone()

output = open("tblBaseCompanyInfo_insert.sql", "w")

new_query = "insert into \"tblBaseCompanyInfo\" (name, ceo, tel, fax, num, email, addr, manager, mobile, uptae, jm, \"moneyCalcWay\", \"vatCalcWay\", \"vatCalcSel\") values ('{0}','{7}', '{1}', '{2}', '{6}', '{3}', '{8}', '{4}', '{5}', '{9}', '{10}', {11}, {13}, {12});\n".format(*base_company_record)

output.write(new_query)
output.close()