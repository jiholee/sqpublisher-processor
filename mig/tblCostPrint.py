# coding: utf-8
from com.ziclix.python.sql import zxJDBC

jdbc_uri = "jdbc:odbc:sqpub"
jdbc_driver = "sun.jdbc.odbc.JdbcOdbcDriver"

conn = zxJDBC.connect(jdbc_uri, None, None, jdbc_driver)
cursor = conn.cursor()

query = u"select 계열, 절수, 시작연, 단가 from tb단가_인쇄"
cursor.execute(query)

output = open("tblCostPrint_insert.sql", "w")

for entry in cursor:
    new_query = "insert into \"tblCostPrint\" (faction, julsu, start, \"unitCost\") values ('{0}', {1}, {2}, {3});\n".format(*entry)

    output.write(new_query)

output.close()