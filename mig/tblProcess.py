# coding: utf-8
from com.ziclix.python.sql import zxJDBC

jdbc_uri = "jdbc:odbc:sqpub"
jdbc_driver = "sun.jdbc.odbc.JdbcOdbcDriver"

conn = zxJDBC.connect(jdbc_uri, None, None, jdbc_driver)
cursor = conn.cursor()

cmbProcess_query = u"select 공정id, 공정, Type, 단가표, 작업일수 from cmb공정"
cursor.execute(cmbProcess_query)

output = open("tblProcess_insert.sql", "w")

for entry in cursor:
    new_query = "insert into \"tblProcess\" (idx, name, p_type, cost_using, work_day) values ({0}, '{1}', '{2}', '{3}', {4});\n".format(*entry)

    output.write(new_query)

output.close()