# coding: utf-8
from com.ziclix.python.sql import zxJDBC

jdbc_uri = "jdbc:odbc:sqpub"
jdbc_driver = "sun.jdbc.odbc.JdbcOdbcDriver"

conn = zxJDBC.connect(jdbc_uri, None, None, jdbc_driver)
cursor = conn.cursor()

cmb_query = u"select 구분id, 구분 from cmb구분"
cursor.execute(cmb_query)

output = open("tblGubun_insert.sql", "w")

for entry in cursor:
    new_query = "insert into \"tblGubun\" (no, name) values ({0}, '{1}');\n".format(*entry)

    output.write(new_query)

output.close()