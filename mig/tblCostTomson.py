# coding: utf-8
from com.ziclix.python.sql import zxJDBC

jdbc_uri = "jdbc:odbc:sqpub"
jdbc_driver = "sun.jdbc.odbc.JdbcOdbcDriver"

conn = zxJDBC.connect(jdbc_uri, None, None, jdbc_driver)
cursor = conn.cursor()

query = u"select 계열, 절수, 기본수량, 단위, 단가, 톰슨종류 from tb단가_톰슨"
cursor.execute(query)

output = open("tblCostTomson_insert.sql", "w")

for entry in cursor:
    new_query = "insert into \"tblCostTomson\" (faction, julsu, quantity, unit, \"unitCost\", method) values ('{0}', {1}, {2}, '{3}', {4}, '{5}');\n".format(*entry)

    output.write(new_query)

output.close()