# coding: utf-8
from com.ziclix.python.sql import zxJDBC

jdbc_uri = "jdbc:odbc:sqpub"
jdbc_driver = "sun.jdbc.odbc.JdbcOdbcDriver"

conn = zxJDBC.connect(jdbc_uri, None, None, jdbc_driver)
cursor = conn.cursor()

cmb_query = u"select 간접비, 비율 from cmb제조간접비"
cursor.execute(cmb_query)

output = open("tblBreakEvenRatio_insert.sql", "w")

for entry in cursor:
    new_query = "insert into \"tblBreakEvenRatio\" (name, ratio) values ('{0}', {1});\n".format(*entry)

    output.write(new_query)

output.close()