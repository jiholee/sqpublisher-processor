# coding: utf-8
from flask import Flask, render_template, request, session, redirect, url_for, jsonify, json
from sqlalchemy import asc
from sqpublisher.database import db_session
from sqpublisher.models import User, BaseCompanyMdl, TradeCompanyMdl, ProcessMdl, ProcessCompanyMdl, JulsuMdl, FormatMdl, \
    GubunMdl, CoatingMdl, BreakEvenRatioMdl, SeriesMdl, BranchMdl, CostPaperMdl, CostBindingChulMdl, CostBindingWireMdl, \
    CostCoatingMdl, CostDaechubMdl, CostDraftMdl, CostEditMdl, CostFilmPrintMdl, CostFoldingMdl, CostMasterPlateMdl, \
    CostMasterPrintMdl, CostOshiMdl, CostPrintMdl, CostProofreadingMdl, CostScanMdl, CostSewingMdl, CostSobuMdl, \
    CostTomsonMdl

app = Flask(__name__)
app.config.update(SECRET_KEY='pfnuI8vM]q?zAIE.HK')


@app.route("/")
def sqpublisher_index():
    if 'logined_id' in session:
        return redirect(url_for("dashboard"))
    else:
        return render_template("index.html")


@app.route("/dashboard", endpoint="dashboard")
def dashboard():
    if 'logined_id' not in session:
        return redirect(url_for("sqpublisher_index"))

    return render_template("dashboard.html")


@app.route("/req_login", methods=["POST"])
def req_login():
    try_login_id = request.form.get('username', None)
    try_login_pass = request.form.get('password', None)

    user = User.query.filter_by(uid=try_login_id)

    if user.count() > 0:
        u = user.first()

        if u.password_eq(try_login_pass):
            session['logined_id'] = try_login_id
            session['logined_email'] = u.email
            session['logined_name'] = u.name

            return jsonify(success=True)
        else:
            return jsonify(success=False, message=u'비밀번호가 일치하지 않습니다')
    else:
        return jsonify(success=False, message=u'아이디가 일치하지 않습니다')


@app.route("/req_logout", methods=["POST"])
def req_logout():
    del session['logined_id']
    del session['logined_email']
    del session['logined_name']

    return jsonify(success=True, message=u'로그아웃 되었습니다.')


# 자사정보 가져오기
@app.route("/baseCompanyInfo/<company_pk>")
def retrieveBasicCompanyInfo(company_pk):
    # company_pk는 형식적으로 받습니다. 실제 사용하지 않는 파라메터
    model = db_session.query(BaseCompanyMdl).filter(BaseCompanyMdl.idx == 1).first()
    if model:
        return jsonify(model.to_dict())
    else:
        return jsonify(dict())


# 자사정보 기록
@app.route("/baseCompanyInfo/<company_pk>", methods=["PUT"])
def postBasicCompanyInfo(company_pk):
    json_data = request.get_json()

    model = db_session.query(BaseCompanyMdl).filter(BaseCompanyMdl.idx == 1).first()
    if model is None:
        model = BaseCompanyMdl()

    model.name = json_data.get('name', '')
    model.ceo = json_data.get('ceo', '')
    model.tel = json_data.get('tel', '')
    model.fax = json_data.get('fax', '')
    model.num = json_data.get('num', '')
    model.email = json_data.get('email', '')
    model.addr = json_data.get('addr', '')
    model.manager = json_data.get('manager', '')
    model.mobile = json_data.get('mobile', '')
    model.uptae = json_data.get('uptae', '')
    model.jm = json_data.get('jm', '')
    model.moneyCalcWay = json_data.get('moneyCalcWay', '')
    model.vatCalcWay = json_data.get('vatCalcWay', '')
    model.vatCalcSel = json_data.get('vatCalcSel', '')

    return_status = dict(success=True)
    try:
        db_session.add(model)
        db_session.commit()
    except Exception:
        return_status['success'] = False
        db_session.rollback()

    return jsonify(return_status)


# 거래처 목록 반환
@app.route("/tradeCompany")
def tradeCompany():
    tradeCompany_q = db_session.query(TradeCompanyMdl).order_by(asc(TradeCompanyMdl.idx))

    ret_dict = dict(success=True, items=[])

    for entry in tradeCompany_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 거래처 정보 수정
@app.route("/tradeCompany/<int:company_pk>", methods=["PUT"])
def putTradeCompanyInfo(company_pk):
    json_data = request.get_json()

    model = db_session.query(TradeCompanyMdl).filter(TradeCompanyMdl.idx == company_pk).first()
    model.name = json_data.get('name', '')
    model.ceo = json_data.get('ceo', '')
    model.tel = json_data.get('tel', '')
    model.fax = json_data.get('fax', '')
    model.num = json_data.get('num', '')
    model.email = json_data.get('email', '')
    model.addr = json_data.get('addr', '')
    model.manager = json_data.get('manager', '')
    model.mobile = json_data.get('mobile', '')
    model.uptae = json_data.get('uptae', '')
    model.jm = json_data.get('jm', '')
    model.etc = json_data.get('etc', '')
    model.gubun = json_data.get('gubun', '')
    model.moneyCalcWay = json_data.get('moneyCalcWay', '')
    model.vatCalcWay = json_data.get('vatCalcWay', '')
    model.vatCalcSel = json_data.get('vatCalcSel', '')

    return_status = dict(success=True)
    try:
        db_session.add(model)
        db_session.commit()
    except:
        return_status['success'] = False
        db_session.rollback()

    return jsonify(return_status)


# 거래처 정보 추가
@app.route("/tradeCompany", methods=["POST"])
def postTradeCompanyInfo():
    json_data = request.get_json()

    model = TradeCompanyMdl(**dict(
        name=json_data.get('name', ''),
        ceo=json_data.get('ceo', ''),
        tel=json_data.get('tel', ''),
        fax=json_data.get('fax', ''),
        num=json_data.get('num', ''),
        email=json_data.get('email', ''),
        addr=json_data.get('addr', ''),
        manager=json_data.get('manager', ''),
        mobile=json_data.get('mobile', ''),
        uptae=json_data.get('uptae', ''),
        jm=json_data.get('jm', ''),
        etc=json_data.get('etc', ''),
        gubun=json_data.get('gubun', ''),
        moneyCalcWay=json_data.get('moneyCalcWay', ''),
        vatCalcWay=json_data.get('vatCalcWay', ''),
        vatCalcSel=json_data.get('vatCalcSel', '')
    ))

    return_status = dict(success=True)
    try:
        db_session.add(model)
        db_session.commit()
        return_status['items'] = [model.to_dict()]
    except:
        return_status['success'] = False
        db_session.rollback()

    return jsonify(return_status)


# 거래처 삭제
@app.route("/tradeCompany/<int:company_pk>", methods=["DELETE"])
def deleteTradeCompanyInfo(company_pk):

    model = db_session.query(TradeCompanyMdl).filter(TradeCompanyMdl.idx == company_pk).first()

    # TODO
    # 이후 작업이 이루어질 때마다 이 거래처와 관련된 모든 자료 단가표, 견적서, 거래명세표
    # 매입매출, 작업지시, 인쇄, 용지수불도 함께 삭제해야 함

    return_status = dict(success=True)
    try:
        db_session.delete(model)
        db_session.commit()
    except:
        return_status['success'] = False
        db_session.rollback()

    return jsonify(return_status)


# 공정 리스트
@app.route("/basicinfo/process")
def getBasicinfoProcess():
    basicInfo_q = db_session.query(ProcessMdl).order_by(asc(ProcessMdl.idx))

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 공정 수정
@app.route("/basicinfo/process", methods=["PUT"])
def putBasicinfoProcess():
    json_data = request.get_json()

    basicInfo = db_session.query(ProcessMdl).filter(ProcessMdl.idx == json_data.get('idx')).first()
    # 기본 작업 일수를 지정하지 않으면 1이 기본 작업 일수
    basicInfo.work_day = json_data.get('work_day', 1)

    db_session.add(basicInfo)
    db_session.commit()

    return jsonify(dict(success=True))


# 공정별 작업업체
@app.route("/basicinfo/process/company")
def getBasicinfoProcessCompany():
    process_company_q = db_session.query(ProcessCompanyMdl)

    ret_dict = dict(success=True, items=[])

    for entry in process_company_q:
        ret_dict['items'].append(entry.to_dict())

    if len(ret_dict['items']) == 0:
        ret_dict['success'] = False

    return jsonify(ret_dict)


# 공정별 작업업체 추가
@app.route("/basicinfo/process/company", methods=["POST"])
def postBasicinfoProcessCompanyAdd():
    json_data = request.get_json()

    model = ProcessCompanyMdl()
    model.process_idx = json_data.get('process_idx')
    model.trade_idx = json_data.get('trade_idx')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 공정별 작업업체 수정
@app.route("/basicinfo/process/company", methods=["PUT"])
def putBasicinfoProcessCompanyModify():
    json_data = request.get_json()

    model = db_session.query(ProcessCompanyMdl).filter(ProcessCompanyMdl.idx == json_data.get('idx')).first()
    model.trade_idx = json_data.get('trade_idx')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 공정별 작업업체 삭제
@app.route("/basicinfo/process/company", methods=["DELETE"])
def deleteBasicinfoProcessCompanyModify():
    json_data = request.get_json()
    print(json_data)

    model = db_session.query(ProcessCompanyMdl).filter(ProcessCompanyMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True))


# 절수 목록
@app.route("/basicinfo/julsu")
def getBasicinfoJulsu():
    julsu_q = db_session.query(JulsuMdl)

    ret_dict = dict(success=True, items=[])

    for entry in julsu_q:
        ret_dict['items'].append(entry.to_dict())

    if len(ret_dict['items']) == 0:
        ret_dict['success'] = False

    return jsonify(ret_dict)


# 절수 추가
@app.route("/basicinfo/julsu", methods=["POST"])
def postBasicinfoJulsu():
    json_data = request.get_json()

    model = JulsuMdl()
    model.julsu = json_data.get('julsu')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 절수 수정
@app.route("/basicinfo/julsu", methods=["PUT"])
def putBasicinfoJulsu():
    json_data = request.get_json()

    model = db_session.query(JulsuMdl).filter(JulsuMdl.idx == json_data.get('idx')).first()
    model.julsu = json_data.get('julsu')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 절수 삭제
@app.route("/basicinfo/julsu", methods=["DELETE"])
def deleteBasicinfoJulsu():
    json_data = request.get_json()

    model = db_session.query(JulsuMdl).filter(JulsuMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 판형 리스트
@app.route("/basicinfo/format")
def getBasicinfoFormat():
    sort_condition = json.loads(request.args.get('sort'))

    sortDirection = FormatMdl.sort_direction(sort_condition[0].get('direction'))
    sortColumn = FormatMdl.sort_column(sort_condition[0].get('property'))

    basicInfo_q = db_session.query(FormatMdl).order_by(
        sortDirection(sortColumn)
    )

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 판형 추가
@app.route("/basicinfo/format", methods=["POST"])
def postBasicinfoFormat():
    json_data = request.get_json()

    model = FormatMdl()
    model.name = json_data.get('name')
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.no = json_data.get('no')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 판형 수정
@app.route("/basicinfo/format", methods=["PUT"])
def putBasicinfoFormat():
    json_data = request.get_json()

    model = db_session.query(FormatMdl).filter(FormatMdl.idx == json_data.get('idx')).first()
    model.name = json_data.get('name')
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.no = json_data.get('no')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 판형 삭제
@app.route("/basicinfo/format", methods=["DELETE"])
def deleteBasicinfoFormat():
    json_data = request.get_json()

    model = db_session.query(FormatMdl).filter(FormatMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))

# 구분 리스트
@app.route("/basicinfo/gubun")
def getBasicinfoGubun():
    basicInfo_q = db_session.query(GubunMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 구분 추가
@app.route("/basicinfo/gubun", methods=["POST"])
def postBasicinfoGubun():
    json_data = request.get_json()

    model = GubunMdl()
    model.name = json_data.get('name')
    model.no = json_data.get('no')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 구분 수정
@app.route("/basicinfo/gubun", methods=["PUT"])
def putBasicinfoGubun():
    json_data = request.get_json()

    model = db_session.query(GubunMdl).filter(GubunMdl.idx == json_data.get('idx')).first()
    model.name = json_data.get('name')
    model.no = json_data.get('no')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 구분 삭제
@app.route("/basicinfo/gubun", methods=["DELETE"])
def deleteBasicinfoGubun():
    json_data = request.get_json()

    model = db_session.query(GubunMdl).filter(GubunMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 코팅 리스트
@app.route("/basicinfo/coating")
def getBasicinfoCoating():
    basicInfo_q = db_session.query(CoatingMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 코팅 추가
@app.route("/basicinfo/coating", methods=["POST"])
def postBasicinfoCoating():
    json_data = request.get_json()

    model = CoatingMdl()
    model.name = json_data.get('name')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 코팅 수정
@app.route("/basicinfo/coating", methods=["PUT"])
def putBasicinfoCoating():
    json_data = request.get_json()

    model = db_session.query(CoatingMdl).filter(CoatingMdl.idx == json_data.get('idx')).first()
    model.name = json_data.get('name')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 코팅 삭제
@app.route("/basicinfo/coating", methods=["DELETE"])
def deleteBasicinfoCoating():
    json_data = request.get_json()

    model = db_session.query(CoatingMdl).filter(CoatingMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 손익분기 리스트
@app.route("/basicinfo/evenRatio")
def getBasicinfoEvenRatio():
    basicInfo_q = db_session.query(BreakEvenRatioMdl).order_by(asc(BreakEvenRatioMdl.idx))

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 손익분기 추가
@app.route("/basicinfo/evenRatio", methods=["POST"])
def postBasicinfoEvenRatio():
    json_data = request.get_json()

    model = BreakEvenRatioMdl()
    model.name = json_data.get('name')
    model.ratio = json_data.get('ratio')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 손익분기 수정
@app.route("/basicinfo/evenRatio", methods=["PUT"])
def putBasicinfoEvenRatio():
    json_data = request.get_json()

    model = db_session.query(BreakEvenRatioMdl).filter(BreakEvenRatioMdl.idx == json_data.get('idx')).first()
    model.name = json_data.get('name')
    model.ratio = json_data.get('ratio')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 손익분기 삭제
@app.route("/basicinfo/evenRatio", methods=["DELETE"])
def deleteBasicinfoEvenRatio():
    json_data = request.get_json()

    model = db_session.query(BreakEvenRatioMdl).filter(BreakEvenRatioMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 분야 리스트
@app.route("/basicinfo/branch")
def getBasicinfoBranch():
    basicInfo_q = db_session.query(BranchMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 분야 추가
@app.route("/basicinfo/branch", methods=["POST"])
def postBasicinfoBranch():
    json_data = request.get_json()

    model = BranchMdl()
    model.name = json_data.get('name')
    model.no = json_data.get('no')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 분야 수정
@app.route("/basicinfo/branch", methods=["PUT"])
def putBasicinfoBranch():
    json_data = request.get_json()

    model = db_session.query(BranchMdl).filter(BranchMdl.idx == json_data.get('idx')).first()
    model.name = json_data.get('name')
    model.no = json_data.get('no')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 분야 삭제
@app.route("/basicinfo/branch", methods=["DELETE"])
def deleteBasicinfoBranch():
    json_data = request.get_json()

    model = db_session.query(BranchMdl).filter(BranchMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 시리즈 리스트
@app.route("/basicinfo/series")
def getBasicinfoSeries():
    basicInfo_q = db_session.query(SeriesMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 시리즈 추가
@app.route("/basicinfo/series", methods=["POST"])
def postBasicinfoSeries():
    json_data = request.get_json()

    model = SeriesMdl()
    model.branch_idx = json_data.get('branch_idx')
    model.name = json_data.get('name')
    model.no = json_data.get('no')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 시리즈 수정
@app.route("/basicinfo/series", methods=["PUT"])
def putBasicinfoSeries():
    json_data = request.get_json()

    model = db_session.query(SeriesMdl).filter(SeriesMdl.idx == json_data.get('idx')).first()
    model.name = json_data.get('name')
    model.no = json_data.get('no')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 시리즈 삭제
@app.route("/basicinfo/series", methods=["DELETE"])
def deleteBasicinfoSeries():
    json_data = request.get_json()

    model = db_session.query(SeriesMdl).filter(SeriesMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 종이단가 리스트
@app.route("/basicinfo/cost/paper")
def getBasicinfoCostPaper():
    basicInfo_q = db_session.query(CostPaperMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 종이단가 추가
@app.route("/basicinfo/cost/paper", methods=["POST"])
def postBasicinfoCostPaper():
    json_data = request.get_json()

    model = CostPaperMdl()
    model.name = json_data.get('name')
    model.faction = json_data.get('faction')
    model.paper_unit = json_data.get('paper_unit')
    model.free_at_factory = json_data.get('free_at_factory')
    model.discount = json_data.get('discount')
    model.unitCost = json_data.get('unitCost')
    model.etc = json_data.get('etc')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 종이단가 수정
@app.route("/basicinfo/cost/paper", methods=["PUT"])
def putBasicinfoCostPaper():
    json_data = request.get_json()

    model = db_session.query(CostPaperMdl).filter(CostPaperMdl.idx == json_data.get('idx')).first()
    model.name = json_data.get('name')
    model.faction = json_data.get('faction')
    model.paper_unit = json_data.get('paper_unit')
    model.free_at_factory = json_data.get('free_at_factory')
    model.discount = json_data.get('discount')
    model.unitCost = json_data.get('unitCost')
    model.etc = json_data.get('etc')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 종이단가 삭제
@app.route("/basicinfo/cost/paper", methods=["DELETE"])
def deleteBasicinfoCostPaper():
    json_data = request.get_json()

    model = db_session.query(CostPaperMdl).filter(CostPaperMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 제본_중철 리스트
@app.route("/basicinfo/cost/bindingChul")
def getBasicinfoCostBindingChul():
    basicInfo_q = db_session.query(CostBindingChulMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 제본_중철 추가
@app.route("/basicinfo/cost/bindingChul", methods=["POST"])
def postBasicinfoCostBindingChul():
    json_data = request.get_json()

    model = CostBindingChulMdl()
    model.format = json_data.get('format')
    model.method = json_data.get('method')
    model.unitCost = json_data.get('unitCost')
    model.dltPage = json_data.get('dltPage')
    model.dltCost = json_data.get('dltCost')
    model.dltQuantity = json_data.get('dltQuantity')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 제본_중철 수정
@app.route("/basicinfo/cost/bindingChul", methods=["PUT"])
def putBasicinfoCostBindingChul():
    json_data = request.get_json()

    model = db_session.query(CostBindingChulMdl).filter(CostBindingChulMdl.idx == json_data.get('idx')).first()
    model.format = json_data.get('format')
    model.method = json_data.get('method')
    model.unitCost = json_data.get('unitCost')
    model.dltPage = json_data.get('dltPage')
    model.dltCost = json_data.get('dltCost')
    model.dltQuantity = json_data.get('dltQuantity')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 제본_중철 삭제
@app.route("/basicinfo/cost/bindingChul", methods=["DELETE"])
def deleteBasicinfoCostBindingChul():
    json_data = request.get_json()

    model = db_session.query(CostBindingChulMdl).filter(CostBindingChulMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 제본_무선 리스트
@app.route("/basicinfo/cost/bindingWire")
def getBasicinfoCostBindingWire():
    basicInfo_q = db_session.query(CostBindingWireMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 제본_무선 추가
@app.route("/basicinfo/cost/bindingWire", methods=["POST"])
def postBasicinfoCostBindingWire():
    json_data = request.get_json()

    model = CostBindingWireMdl()
    model.format = json_data.get('format')
    model.method = json_data.get('method')
    model.unitCost = json_data.get('unitCost')
    model.dltPage = json_data.get('dltPage')
    model.dltCost = json_data.get('dltCost')
    model.dltQuantity = json_data.get('dltQuantity')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 제본_무선 수정
@app.route("/basicinfo/cost/bindingWire", methods=["PUT"])
def putBasicinfoCostBindingWire():
    json_data = request.get_json()

    model = db_session.query(CostBindingWireMdl).filter(CostBindingWireMdl.idx == json_data.get('idx')).first()
    model.format = json_data.get('format')
    model.method = json_data.get('method')
    model.unitCost = json_data.get('unitCost')
    model.dltPage = json_data.get('dltPage')
    model.dltCost = json_data.get('dltCost')
    model.dltQuantity = json_data.get('dltQuantity')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 제본_무선 삭제
@app.route("/basicinfo/cost/bindingWire", methods=["DELETE"])
def deleteBasicinfoCostBindingWire():
    json_data = request.get_json()

    model = db_session.query(CostBindingWireMdl).filter(CostBindingWireMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 코팅 리스트
@app.route("/basicinfo/cost/coating")
def getBasicinfoCostCoating():
    basicInfo_q = db_session.query(CostCoatingMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 코팅 추가
@app.route("/basicinfo/cost/coating", methods=["POST"])
def postBasicinfoCostCoating():
    json_data = request.get_json()

    model = CostCoatingMdl()
    model.name = json_data.get('name')
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.quantity = json_data.get('quantity')
    model.unit = json_data.get('unit')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 코팅 수정
@app.route("/basicinfo/cost/coating", methods=["PUT"])
def putBasicinfoCostCoating():
    json_data = request.get_json()

    model = db_session.query(CostCoatingMdl).filter(CostCoatingMdl.idx == json_data.get('idx')).first()
    model.name = json_data.get('name')
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.quantity = json_data.get('quantity')
    model.unit = json_data.get('unit')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 코팅 삭제
@app.route("/basicinfo/cost/coating", methods=["DELETE"])
def deleteBasicinfoCostCoating():
    json_data = request.get_json()

    model = db_session.query(CostCoatingMdl).filter(CostCoatingMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 대첩 리스트
@app.route("/basicinfo/cost/daechub")
def getBasicinfoCostDaechub():
    basicInfo_q = db_session.query(CostDaechubMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 대첩 추가
@app.route("/basicinfo/cost/daechub", methods=["POST"])
def postBasicinfoCostDaechub():
    json_data = request.get_json()

    model = CostDaechubMdl()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 대첩 수정
@app.route("/basicinfo/cost/daechub", methods=["PUT"])
def putBasicinfoCostDaechub():
    json_data = request.get_json()

    model = db_session.query(CostDaechubMdl).filter(CostDaechubMdl.idx == json_data.get('idx')).first()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 대첩 삭제
@app.route("/basicinfo/cost/daechub", methods=["DELETE"])
def deleteBasicinfoCostDaechub():
    json_data = request.get_json()

    model = db_session.query(CostDaechubMdl).filter(CostDaechubMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 시안 리스트
@app.route("/basicinfo/cost/draft")
def getBasicinfoCostDraft():
    basicInfo_q = db_session.query(CostDraftMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 시안 추가
@app.route("/basicinfo/cost/draft", methods=["POST"])
def postBasicinfoCostDraft():
    json_data = request.get_json()

    model = CostDraftMdl()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 시안 수정
@app.route("/basicinfo/cost/draft", methods=["PUT"])
def putBasicinfoCostDraft():
    json_data = request.get_json()

    model = db_session.query(CostDraftMdl).filter(CostDraftMdl.idx == json_data.get('idx')).first()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 시안 삭제
@app.route("/basicinfo/cost/draft", methods=["DELETE"])
def deleteBasicinfoCostDraft():
    json_data = request.get_json()

    model = db_session.query(CostDraftMdl).filter(CostDraftMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 편집 리스트
@app.route("/basicinfo/cost/edit")
def getBasicinfoCostEdit():
    basicInfo_q = db_session.query(CostEditMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 편집 추가
@app.route("/basicinfo/cost/edit", methods=["POST"])
def postBasicinfoCostEdit():
    json_data = request.get_json()

    model = CostEditMdl()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 편집 수정
@app.route("/basicinfo/cost/edit", methods=["PUT"])
def putBasicinfoCostEdit():
    json_data = request.get_json()

    model = db_session.query(CostEditMdl).filter(CostEditMdl.idx == json_data.get('idx')).first()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 편집 삭제
@app.route("/basicinfo/cost/edit", methods=["DELETE"])
def deleteBasicinfoCostEdit():
    json_data = request.get_json()

    model = db_session.query(CostEditMdl).filter(CostEditMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 필름출력 리스트
@app.route("/basicinfo/cost/filmPrint")
def getBasicinfoCostFilmPrint():
    basicInfo_q = db_session.query(CostFilmPrintMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 필름출력 추가
@app.route("/basicinfo/cost/filmPrint", methods=["POST"])
def postBasicinfoCostFilmPrint():
    json_data = request.get_json()

    model = CostFilmPrintMdl()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 필름출력 수정
@app.route("/basicinfo/cost/filmPrint", methods=["PUT"])
def putBasicinfoCostFilmPrint():
    json_data = request.get_json()

    model = db_session.query(CostFilmPrintMdl).filter(CostFilmPrintMdl.idx == json_data.get('idx')).first()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 필름출력 삭제
@app.route("/basicinfo/cost/filmPrint", methods=["DELETE"])
def deleteBasicinfoCostFilmPrint():
    json_data = request.get_json()

    model = db_session.query(CostFilmPrintMdl).filter(CostFilmPrintMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 접지 리스트
@app.route("/basicinfo/cost/folding")
def getBasicinfoCostFolding():
    basicInfo_q = db_session.query(CostFoldingMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 접지 수정
@app.route("/basicinfo/cost/folding", methods=["PUT"])
def putBasicinfoCostFolding():
    json_data = request.get_json()

    model = db_session.query(CostFoldingMdl).filter(CostFoldingMdl.idx == json_data.get('idx')).first()
    model.name = json_data.get('name')
    model.quantity = json_data.get('quantity')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 마스타제본 리스트
@app.route("/basicinfo/cost/masterPlate")
def getBasicinfoCostMasterPlate():
    basicInfo_q = db_session.query(CostMasterPlateMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 마스타제본 추가
@app.route("/basicinfo/cost/masterPlate", methods=["POST"])
def postBasicinfoCostMasterPlate():
    json_data = request.get_json()

    model = CostMasterPlateMdl()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 마스타제본 수정
@app.route("/basicinfo/cost/masterPlate", methods=["PUT"])
def putBasicinfoCostMasterPlate():
    json_data = request.get_json()

    model = db_session.query(CostMasterPlateMdl).filter(CostMasterPlateMdl.idx == json_data.get('idx')).first()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 마스타제본 삭제
@app.route("/basicinfo/cost/masterPlate", methods=["DELETE"])
def deleteBasicinfoCostMasterPlate():
    json_data = request.get_json()

    model = db_session.query(CostMasterPlateMdl).filter(CostMasterPlateMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 마스타인쇄 리스트
@app.route("/basicinfo/cost/masterPrint")
def getBasicinfoCostMasterPrint():
    basicInfo_q = db_session.query(CostMasterPrintMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 마스타인쇄 추가
@app.route("/basicinfo/cost/masterPrint", methods=["POST"])
def postBasicinfoCostMasterPrint():
    json_data = request.get_json()

    model = CostMasterPrintMdl()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 마스타인쇄 수정
@app.route("/basicinfo/cost/masterPrint", methods=["PUT"])
def putBasicinfoCostMasterPrint():
    json_data = request.get_json()

    model = db_session.query(CostMasterPrintMdl).filter(CostMasterPrintMdl.idx == json_data.get('idx')).first()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 마스타인쇄 삭제
@app.route("/basicinfo/cost/masterPrint", methods=["DELETE"])
def deleteBasicinfoCostMasterPrint():
    json_data = request.get_json()

    model = db_session.query(CostMasterPrintMdl).filter(CostMasterPrintMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 오시 리스트
@app.route("/basicinfo/cost/oshi")
def getBasicinfoCostOshi():
    basicInfo_q = db_session.query(CostOshiMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 오시 추가
@app.route("/basicinfo/cost/oshi", methods=["POST"])
def postBasicinfoCostOshi():
    json_data = request.get_json()

    model = CostOshiMdl()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.quantity = json_data.get('quantity')
    model.unit = json_data.get('unit')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 오시 수정
@app.route("/basicinfo/cost/oshi", methods=["PUT"])
def putBasicinfoCostOshi():
    json_data = request.get_json()

    model = db_session.query(CostOshiMdl).filter(CostOshiMdl.idx == json_data.get('idx')).first()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.quantity = json_data.get('quantity')
    model.unit = json_data.get('unit')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 오시 삭제
@app.route("/basicinfo/cost/oshi", methods=["DELETE"])
def deleteBasicinfoCostOshi():
    json_data = request.get_json()

    model = db_session.query(CostOshiMdl).filter(CostOshiMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 인쇄 리스트
@app.route("/basicinfo/cost/print")
def getBasicinfoCostPrint():
    basicInfo_q = db_session.query(CostPrintMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 인쇄 추가
@app.route("/basicinfo/cost/print", methods=["POST"])
def postBasicinfoCostPrint():
    json_data = request.get_json()

    model = CostPrintMdl()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.start = json_data.get('start')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 인쇄 수정
@app.route("/basicinfo/cost/print", methods=["PUT"])
def putBasicinfoCostPrint():
    json_data = request.get_json()

    model = db_session.query(CostPrintMdl).filter(CostPrintMdl.idx == json_data.get('idx')).first()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.start = json_data.get('start')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 인쇄 삭제
@app.route("/basicinfo/cost/print", methods=["DELETE"])
def deleteBasicinfoCostPrint():
    json_data = request.get_json()

    model = db_session.query(CostPrintMdl).filter(CostPrintMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 교정인쇄 리스트
@app.route("/basicinfo/cost/proofreading")
def getBasicinfoCostProofreading():
    basicInfo_q = db_session.query(CostProofreadingMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 교정인쇄 수정
@app.route("/basicinfo/cost/proofreading", methods=["PUT"])
def putBasicinfoCostProofreading():
    json_data = request.get_json()

    model = db_session.query(CostProofreadingMdl).filter(CostProofreadingMdl.idx == json_data.get('idx')).first()
    model.colorCnt = json_data.get('colorCnt')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 스캔 리스트
@app.route("/basicinfo/cost/scan")
def getBasicinfoCostScan():
    basicInfo_q = db_session.query(CostScanMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 스캔 수정
@app.route("/basicinfo/cost/scan", methods=["PUT"])
def putBasicinfoCostScan():
    json_data = request.get_json()

    model = db_session.query(CostScanMdl).filter(CostScanMdl.idx == json_data.get('idx')).first()
    model.size = json_data.get('size')
    model.sizeUnitCost = json_data.get('sizeUnitCost')
    model.pUnitCost = json_data.get('pUnitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 미싱 리스트
@app.route("/basicinfo/cost/sewing")
def getBasicinfoCostSewing():
    basicInfo_q = db_session.query(CostSewingMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 미싱 추가
@app.route("/basicinfo/cost/sewing", methods=["POST"])
def postBasicinfoCostSewing():
    json_data = request.get_json()

    model = CostSewingMdl()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.quantity = json_data.get('quantity')
    model.unit = json_data.get('unit')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 미싱 수정
@app.route("/basicinfo/cost/sewing", methods=["PUT"])
def putBasicinfoCostSewing():
    json_data = request.get_json()

    model = db_session.query(CostSewingMdl).filter(CostSewingMdl.idx == json_data.get('idx')).first()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.quantity = json_data.get('quantity')
    model.unit = json_data.get('unit')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 미싱 삭제
@app.route("/basicinfo/cost/sewing", methods=["DELETE"])
def deleteBasicinfoCostSewing():
    json_data = request.get_json()

    model = db_session.query(CostSewingMdl).filter(CostSewingMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 소부 리스트
@app.route("/basicinfo/cost/sobu")
def getBasicinfoCostSobu():
    basicInfo_q = db_session.query(CostSobuMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 소부 추가
@app.route("/basicinfo/cost/sobu", methods=["POST"])
def postBasicinfoCostSobu():
    json_data = request.get_json()

    model = CostSobuMdl()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 소부 수정
@app.route("/basicinfo/cost/sobu", methods=["PUT"])
def putBasicinfoCostSobu():
    json_data = request.get_json()

    model = db_session.query(CostSobuMdl).filter(CostSobuMdl.idx == json_data.get('idx')).first()
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 소부 삭제
@app.route("/basicinfo/cost/sobu", methods=["DELETE"])
def deleteBasicinfoCostSobu():
    json_data = request.get_json()

    model = db_session.query(CostSobuMdl).filter(CostSobuMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 톰슨 리스트
@app.route("/basicinfo/cost/tomson")
def getBasicinfoCostTomson():
    basicInfo_q = db_session.query(CostTomsonMdl)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 톰슨 추가
@app.route("/basicinfo/cost/tomson", methods=["POST"])
def postBasicinfoCostTomson():
    json_data = request.get_json()

    model = CostTomsonMdl()
    model.method = json_data.get('method')
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.quantity = json_data.get('quantity')
    model.unit = json_data.get('unit')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 톰슨 수정
@app.route("/basicinfo/cost/tomson", methods=["PUT"])
def putBasicinfoCostTomson():
    json_data = request.get_json()

    model = db_session.query(CostTomsonMdl).filter(CostTomsonMdl.idx == json_data.get('idx')).first()
    model.method = json_data.get('method')
    model.faction = json_data.get('faction')
    model.julsu = json_data.get('julsu')
    model.quantity = json_data.get('quantity')
    model.unit = json_data.get('unit')
    model.unitCost = json_data.get('unitCost')

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 톰슨 삭제
@app.route("/basicinfo/cost/tomson", methods=["DELETE"])
def deleteBasicinfoCostTomson():
    json_data = request.get_json()

    model = db_session.query(CostTomsonMdl).filter(CostTomsonMdl.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))


# 사원 리스트
@app.route("/basicinfo/member")
def getBasicinfoMember():
    basicInfo_q = db_session.query(User)

    ret_dict = dict(success=True, items=[])

    for entry in basicInfo_q:
        ret_dict['items'].append(entry.to_dict())

    return jsonify(ret_dict)


# 사원 추가
@app.route("/basicinfo/member", methods=["POST"])
def postBasicinfoMember():
    json_data = request.get_json()

    model = User(
        uid=json_data.get('uid'),
        password=json_data.get('password'),
        name=json_data.get('name'),
        dept=json_data.get('dept'),
        position=json_data.get('position'),
        tel=json_data.get('tel'),
        hp=json_data.get('hp'),
        email=json_data.get('email'),
        addr=json_data.get('addr'),
        post=json_data.get('post')
    )

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 사원 수정
@app.route("/basicinfo/member", methods=["PUT"])
def putBasicinfoMember():
    json_data = request.get_json()

    model = db_session.query(User).filter(User.idx == json_data.get('idx')).first()
    model.populate(
        uid=json_data.get('uid'),
        name=json_data.get('name'),
        dept=json_data.get('dept'),
        position=json_data.get('position'),
        tel=json_data.get('tel'),
        hp=json_data.get('hp'),
        email=json_data.get('email'),
        addr=json_data.get('addr'),
        post=json_data.get('post')
    )

    # 새로운 비밀번호가 입력된 경우에만 수정
    if json_data.get('password') != "":
        model.password_encryption(json_data.get('password'))

    db_session.add(model)
    db_session.commit()

    return jsonify(dict(success=True, items=[model.to_dict()]))


# 사원 삭제
@app.route("/basicinfo/member", methods=["DELETE"])
def deleteBasicinfoMember():
    json_data = request.get_json()

    model = db_session.query(User).filter(User.idx == json_data.get('idx')).first()

    db_session.delete(model)
    db_session.commit()

    return jsonify(dict(success=True, data=model.to_dict()))
