/**
 * Created by jiho on 14. 11. 18..
 */
Ext.application({
    requires: ['Ext.container.Viewport'],
    name: 'sqpub',

    controllers: ['Dashboard', 'Basic', 'BreakEven', 'BuySale', 'Order',
        'OutsideOrder', 'Paper', 'PrimeCost', 'main'],

    appFolder: '/static/app',

    launch: function() {
        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            id: 'dashboard',
            items: [{
                region: 'north',
                title: 'SQPublisher - 출판 편집자와 기획자가 쓰기 쉬운 출판 공정 관리 프로그램입니다',

                tbar: [{
                    xtype: 'button',
                    text: '원가목록',
                    action: 'btnPrimeCostListOpen'
                }, {
                    xtype: 'button',
                    text: '발주목록',
                    action: 'btnOrderListOpen'
                }, {
                    xtype: 'splitbutton',
                    text: '외주발주',
                    menu: Ext.create('Ext.menu.Menu', {
                        items: [{
                            text: '전체',
                            action: 'btnOutsideOrderTotalListOpen'
                        }, {
                            text: '거래처별',
                            action: 'btnOutsideOrderTradeListOpen'
                        }]
                    })
                }, {
                    xtype: 'button',
                    text: '손익분기',
                    action: 'btnBreakEvenListOpen'
                }, {
                    xtype: 'button',
                    text: '매입매출',
                    action: 'btnBuySaleListOpen'
                }, {
                    xtype: 'splitbutton',
                    text: '용지',
                    menu: Ext.create('Ext.menu.Menu', {
                        items: [{
                            text: '수불',
                            action: 'btnPaperBuy'
                        }, {
                            text: '재고',
                            action: 'btnPaperInventory'
                        }]
                    })
                }, {
                    xtype: 'splitbutton',
                    text: '기초정보',
                    menu: Ext.create('Ext.menu.Menu', {
                        items: [{
                            text: '자사정보',
                            action: 'btnCompanyInfoWin'
                        }, {
                            text: '기초자료',
                            action: 'btnBasicInfoTab'
                        }, {
                            text: '분야/시리즈',
                            action: 'btnBranchSeries'
                        }, {
                            text: '단가',
                            action: 'btnUnitCost'
                        }, {
                            text: '거래처 자료',
                            action: 'btnClient'
                        }, {
                            text: '사원정보',
                            action: 'btnEmployee'
                        }]
                    })
                }, {
                    xtype: 'splitbutton',
                    text: '도움말',
                    menu: Ext.create('Ext.menu.Menu', {
                        items: [{
                            text: 'About'
                        }, {
                            text: 'TOC'
                        }]
                    })
                },
                '->',
                {
                    xtype: 'splitbutton',
                    text: Ext.Element(Ext.dom.Query.select('body')[0]).getAttribute("data-login_id") + "님",
                    menu: Ext.create('Ext.menu.Menu', {
                        items: [{
                            text: '회원정보 수정',
                            action: 'btn_member_modify'
                        }, {
                            text: '로그아웃',
                            action: 'btn_logout'
                        }]
                    })
                }]
            }, {
                region: 'center',
                xtype: 'tabpanel',
                layout: 'fit',
                id: 'center',
                items: [{
                    xtype: 'Dashboard'
                }]
            }, {
                region: 'east',
                xtype: 'panel',
                layout: 'fit',
                width: 300,
                title: 'Optional',
                collapsible: true,
                collapsed: true,
                id: 'east',
                items: []
            }]
        });
    }
});