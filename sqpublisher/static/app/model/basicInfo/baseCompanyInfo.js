/**
 * Created by jiho on 15. 7. 10..
 *
 * 자사정보
 */
Ext.define('sqpub.model.basicInfo.baseCompanyInfo', {
    extend: 'Ext.data.Model',

    fields: [
        'name',
        'ceo',
        'tel',
        'fax',
        'num',
        'email',
        'addr',
        'manager',
        'mobile',
        'uptae',
        'jm',
        'moneyCalcWay',
        'vatCalcWay',
        'vatCalcSel'
    ],

    proxy: {
        type: 'rest',
        url: '/baseCompanyInfo',
        reader: {
            type: 'json'
        }
    }
});