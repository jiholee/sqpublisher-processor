/**
 * Created by jiho on 14. 11. 24..
 *
 * 거래처 보기에서 외주/고객 구분
 */
Ext.define('sqpub.model.basicInfo.tradeGubun', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx'},
        { name: 'gubun' }
    ]
});
