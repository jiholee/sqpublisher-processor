/**
 * Created by jiho on 14. 11. 25..
 *
 * 사원목록
 */
Ext.define('sqpub.model.basicInfo.employee', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' },
        { name: 'uid' },
        { name: 'password' },
        { name: 'name' },
        { name: 'dept' },
        { name: 'position' },
        { name: 'tel' },
        { name: 'hp' },
        { name: 'email' },
        { name: 'addr' },
        { name: 'post' },
        { name: 'etc' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/member',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});
