/**
 * Created by jiho on 14. 11. 22..
 */
Ext.define('sqpub.model.basicInfo.series', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idx', type: 'int'},
        {name: 'branch_idx', type: 'int'},
        {name: 'no', type: 'int'},
        {name: 'name'}
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/series',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});