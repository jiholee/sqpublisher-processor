/**
 * Created by jiho on 15. 07. 18..
 *
 * 필름출력
 */
Ext.define('sqpub.model.basicInfo.cost.filmPrint', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' },
        { name: 'faction' },
        { name: 'julsu', type: 'int' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/filmPrint',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});