/**
 * Created by jiho on 15. 07. 18..
 *
 * 시안
 */
Ext.define('sqpub.model.basicInfo.cost.draft', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' },
        { name: 'faction' },
        { name: 'julsu', type: 'int' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/draft',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});