/**
 * Created by jiho on 14. 11. 22..
 *
 * 스캔
 */
Ext.define('sqpub.model.basicInfo.cost.scan', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' } ,
        { name: 'size', desc: '기본 Size', type: 'int' },
        { name: 'sizeUnitCost', desc: '기본 Size 단가', type: 'int' },
        { name: 'pUnitCost', desc: '평당 단가', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/scan',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});