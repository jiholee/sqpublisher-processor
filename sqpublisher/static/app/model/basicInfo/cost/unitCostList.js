/**
 * Created by jiho on 14. 11. 22..
 */
Ext.define('sqpub.model.basicInfo.cost.unitCostList', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name'}
    ]
});