/**
 * Created by jiho on 15. 07. 18..
 *
 * 오시
 */
Ext.define('sqpub.model.basicInfo.cost.oshi', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' },
        { name: 'faction' },
        { name: 'julsu', type: 'int' },
        { name: 'quantity', type: 'int' },
        { name: 'unit' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/oshi',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});