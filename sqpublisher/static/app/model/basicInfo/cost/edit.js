/**
 * Created by jiho on 15. 07. 18..
 *
 * 편집
 */
Ext.define('sqpub.model.basicInfo.cost.edit', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' },
        { name: 'faction' },
        { name: 'julsu', type: 'int' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/edit',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});