/**
 * Created by jiho on 15. 07. 18..
 *
 * 미싱
 */
Ext.define('sqpub.model.basicInfo.cost.sewing', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' },
        { name: 'faction' },
        { name: 'julsu', type: 'int' },
        { name: 'quantity', type: 'int' },
        { name: 'unit' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/sewing',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});