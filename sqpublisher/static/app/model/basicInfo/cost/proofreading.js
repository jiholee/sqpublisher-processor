/**
 * Created by jiho on 14. 11. 22..
 *
 * 교정인쇄
 */
Ext.define('sqpub.model.basicInfo.cost.proofreading', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' } ,
        { name: 'colorCnt', desc: '도수', type: 'int' },
        { name: 'unitCost', desc: '단가', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/proofreading',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});