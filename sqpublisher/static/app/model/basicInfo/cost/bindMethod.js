/**
 * Created by jiho on 14. 11. 24..
 *
 * 단가 > 제본에서 사용하는 상철, 좌철 표시
 */
Ext.define('sqpub.model.basicInfo.cost.bindMethod', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'method' }
    ]
});
