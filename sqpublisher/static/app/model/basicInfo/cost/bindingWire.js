/**
 * Created by jiho on 14. 11. 22..
 *
 * 단가 > 제본_무선
 */
Ext.define('sqpub.model.basicInfo.cost.bindingWire', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' } ,
        { name: 'format' },
        { name: 'method' },
        { name: 'unitCost', type: 'float' },
        { name: 'dltPage', type: 'int' },
        { name: 'dltCost', type: 'int' },
        { name: 'dltQuantity', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/bindingWire',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});