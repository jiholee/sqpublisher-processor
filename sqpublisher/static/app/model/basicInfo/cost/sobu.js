/**
 * Created by jiho on 15. 07. 18..
 *
 * 소부
 */
Ext.define('sqpub.model.basicInfo.cost.sobu', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' },
        { name: 'faction' },
        { name: 'julsu' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/sobu',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});