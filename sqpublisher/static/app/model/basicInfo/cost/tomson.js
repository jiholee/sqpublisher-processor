/**
 * Created by jiho on 14. 11. 22..
 *
 * 톰슨
 */
Ext.define('sqpub.model.basicInfo.cost.tomson', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' } ,
        { name: 'method' },
        { name: 'faction' },
        { name: 'julsu', type: 'int' },
        { name: 'quantity', type: 'int' },
        { name: 'unit' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/tomson',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});