/**
 * Created by jiho on 14. 11. 22..
 *
 * 단가 > 인쇄
 */
Ext.define('sqpub.model.basicInfo.cost.print', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' } ,
        { name: 'faction' },
        { name: 'julsu', type: 'int' },
        { name: 'start', type: 'int' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/print',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});