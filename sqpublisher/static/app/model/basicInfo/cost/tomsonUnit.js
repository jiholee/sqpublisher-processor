/**
 * Created by jiho on 14. 11. 24..
 *
 * 단가 > 톰슨 종류(자동, 수동)
 */
Ext.define('sqpub.model.basicInfo.cost.tomsonUnit', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'method' }
    ]
});