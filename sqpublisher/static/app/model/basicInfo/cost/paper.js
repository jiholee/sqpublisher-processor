/**
 * Created by jiho on 14. 11. 22..
 *
 * 용지
 */
Ext.define('sqpub.model.basicInfo.cost.paper', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' },
        { name: 'name', desc: '종이종류' },
        { name: 'faction', desc: '계열' },
        { name: 'paper_unit', desc: '단위' },
        { name: 'free_at_factory', desc: '공장도가', type: 'int' },
        { name: 'discount', desc: '할인율', type: 'float' },
        { name: 'unitCost', desc: '단가', type: 'int' },
        { name: 'etc', desc: '비고(가로*세로)' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/paper',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});