/**
 * Created by jiho on 15. 07. 18..
 *
 * 마스타제판
 */
Ext.define('sqpub.model.basicInfo.cost.masterPlate', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' },
        { name: 'faction' },
        { name: 'julsu', type: 'int' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/masterPlate',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});