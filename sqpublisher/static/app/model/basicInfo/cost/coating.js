/**
 * Created by jiho on 14. 11. 22..
 *
 * 코팅
 */
Ext.define('sqpub.model.basicInfo.cost.coating', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' } ,
        { name: 'name' },
        { name: 'faction' },
        { name: 'julsu', type: 'int' },
        { name: 'quantity', type: 'int' },
        { name: 'unit' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/coating',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});