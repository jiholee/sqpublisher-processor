/**
 * Created by jiho on 15. 07. 18..
 *
 * 대첩
 */
Ext.define('sqpub.model.basicInfo.cost.daechub', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' },
        { name: 'faction' },
        { name: 'julsu', type: 'int' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/daechub',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});