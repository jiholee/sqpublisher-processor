/**
 * Created by jiho on 14. 11. 22..
 *
 * 접지
 */
Ext.define('sqpub.model.basicInfo.cost.folding', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' },
        { name: 'name' },
        { name: 'quantity', type: 'int' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/folding',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});