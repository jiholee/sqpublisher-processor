/**
 * Created by jiho on 15. 07. 18..
 *
 * 마스타인쇄
 */
Ext.define('sqpub.model.basicInfo.cost.masterPrint', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', type: 'int' },
        { name: 'faction' },
        { name: 'julsu', type: 'int' },
        { name: 'unitCost', type: 'int' }
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/cost/masterPrint',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});