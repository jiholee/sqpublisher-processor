/**
 * Created by jiho on 14. 11. 23..
 *
 * 미싱, 오시, 코팅, 톰슨에서 연당단가/장당단가 필터링에 사용되는 모델
 */
Ext.define('sqpub.model.basicInfo.cost.unitFilter', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'value'},
        {name: 'name'}
    ]
});
