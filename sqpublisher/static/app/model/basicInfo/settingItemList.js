/**
 * Created by jiho on 14. 11. 22..
 *
 * 기초자료 설정 항목을 담아두기 위한 모델 선언
 *
 * 아이템:
 *      공정
 *      절수
 *      판형
 *      구분
 *      코팅
 *      손익분기
 *      기초자료
 */
Ext.define('sqpub.model.basicInfo.settingItemList', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name'}
    ]
});