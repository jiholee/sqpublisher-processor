/**
 * Created by jiho on 14. 11. 24..
 *
 * 거래처 목록
 */
Ext.define('sqpub.model.basicInfo.tradeCompany', {
    extend: 'Ext.data.Model',
    idProperty: 'idx',
    fields: [
        { name: 'idx' },
        { name: 'name' },
        { name: 'tel' },
        { name: 'fax' },
        { name: 'email' },
        { name: 'manager' },
        { name: 'mobile' },
        { name: 'etc' },
        { name: 'num' },
        { name: 'ceo' },
        { name: 'addr' },
        { name: 'uptae' },
        { name: 'jm' },
        { name: 'moneyCalcWay' },
        { name: 'vatCalcWay' },
        { name: 'vatCalcSel' },
        { name: 'gubun' }
    ],
    setRecord: function(record) {
        var obj = this;
        var fields = sqpub.model.basicInfo.tradeCompany.getFields();

        Ext.Object.each(fields, function(key, value, object) {
            obj.set(value.name, record.get(value.name));
        });

        obj = null;
    },
    proxy: {
        type: 'rest',
        url: '/tradeCompany',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});