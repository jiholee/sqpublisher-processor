/**
 * Created by jiho on 14. 11. 22..
 */
Ext.define('sqpub.model.basicInfo.material.coating', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idx', type: 'int'},
        {name: 'name'}
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/coating',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});