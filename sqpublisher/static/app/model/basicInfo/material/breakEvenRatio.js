/**
 * Created by jiho on 14. 11. 24..
 */
Ext.define('sqpub.model.basicInfo.material.breakEvenRatio', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idx'},
        {name: 'name'},
        {name: 'ratio'}
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/evenRatio',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});