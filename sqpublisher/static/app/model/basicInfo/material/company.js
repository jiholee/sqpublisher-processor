/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.model.basicInfo.material.company', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idx'},
        {name: 'process_idx', type: 'int'},
        {name: 'trade_idx', type: 'int'},
        {name: 'trade_name'}
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/process/company',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});