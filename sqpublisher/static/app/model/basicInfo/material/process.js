/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.model.basicInfo.material.process', {
    extend: 'Ext.data.Model',
    idProperty: 'idx',
    fields: [
        {name: 'idx'},
        {name: 'name'},
        {name: 'p_type'},
        {name: 'work_day', type: 'int'}
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/process',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});