/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.model.basicInfo.material.format', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idx', type: 'int'},
        {name: 'no', type: 'int'},
        {name: 'name'},
        {name: 'faction'},
        {name: 'julsu', type: 'int'}
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/format',
        reader: {
            type: 'json',
            root: 'items'
        }
    }
});