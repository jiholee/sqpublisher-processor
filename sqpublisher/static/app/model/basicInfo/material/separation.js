/**
 * Created by jiho on 14. 11. 22..
 */
Ext.define('sqpub.model.basicInfo.material.separation', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idx', type: 'int'},
        {name: 'no', type: 'int'},
        {name: 'name'}
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/gubun',
        reader: {
            type: 'json',
            root: 'items'
        },
        idParam: 'idx'
    }
});