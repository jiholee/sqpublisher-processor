/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.model.basicInfo.material.julsu', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idx', type: 'int'},
        {name: 'julsu', type: 'int'}
    ],
    proxy: {
        type: 'rest',
        url: '/basicinfo/julsu',
        reader: {
            type: 'json',
            root: 'items'
        },
        idParam: 'idx'
    }
});