/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.model.common.faction', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'name'}
    ]
});