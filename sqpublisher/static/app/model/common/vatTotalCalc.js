/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.model.common.vatTotalCalc', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idx', type: 'string', convert: null},
        {name: 'name', type: 'string'},
        {name: 'desc', type: 'string'}
    ]
});