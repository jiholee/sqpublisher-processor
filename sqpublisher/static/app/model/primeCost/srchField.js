/**
 * Created by jiho on 14. 12. 2..
 *
 * 리스트에서 검색 필드 지정
 */
Ext.define('sqpub.model.primeCost.srchField', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'text' },
        { name: 'field' }
    ]
});