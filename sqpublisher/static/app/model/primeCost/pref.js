/**
 * Created by jiho on 14. 11. 25..
 */
Ext.define('sqpub.model.primeCost.pref', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idx', desc: '고유번호', type: 'int' },
        { name: 'name', desc: '품명' },
        { name: 'unit', desc: '단위' },
        { name: 'cost', desc: '가격', type: 'int' },
        { name: 'page', desc: '쪽수', type: 'int' },
        { name: 'format', desc: '판형' },
        { name: 'royaltyRatio', desc: '인세율', type: 'float' },
        { name: 'faction', desc: '계열' },
        { name: 'julsu', desc: '절수', type: 'int' },
        { name: 'author', desc: '저자' },
        { name: 'bind', desc: '제본' },
        { name: 'branch', desc: '분야', type: 'int' },
        { name: 'series', desc: '시리즈', type: 'int' },
        { name: 'branch_repr', desc: '분야' },
        { name: 'series_repr', desc: '시리즈' },
        { name: 'inji', desc: '인지' },
        { name: 'isbn', desc: 'ISBN' },
        { name: 'clerk', desc: '견적인' },
        { name: 'etc', desc: '비고' },
        { name: 'reg_date', desc: '등록일' },
        { name: 'vatMethod', desc: '부가세종류', type: 'int' }
    ]
});