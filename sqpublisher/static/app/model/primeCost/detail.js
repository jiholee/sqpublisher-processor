/**
 * Created by jiho on 14. 11. 25..
 */
Ext.define('sqpub.model.primeCost.detail', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'no', desc: 'No' },
        { name: 'process', desc: '공정' },
        { name: 'gubun', desc: '구분' },
        { name: 'faction', desc: '계열' },
        { name: 'julsu', desc: '절수' },
        { name: 'cnt', desc: '수량' },
        { name: 'unitCost', desc: '단가' },
        { name: 'etc', desc: '비고' },
        { name: 'paperSubul', desc: '종이수불' },
        { name: 'daesu', desc: '대수' },
        { name: 'doneDng', desc: '돈땡' }
    ]
});