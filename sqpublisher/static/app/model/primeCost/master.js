/**
 * Created by jiho on 14. 11. 25..
 */
Ext.define('sqpub.model.primeCost.master', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'gubun', desc: '구분' },
        { name: 'faction', desc: '계열' },
        { name: 'julsu', desc: '절수' },
        { name: 'page', desc: '쪽수' },
        { name: 'paper', desc: '종이종류' },
        { name: 'paper_faction', desc: '계열' },
        { name: 'fPriColor', desc: '전면 원색' },
        { name: 'fSpcColor', desc: '전면 별색' },
        { name: 'fVedColor', desc: '전면 베다' },
        { name: 'fFluColor', desc: '전면 형광' },
        { name: 'bPriColor', desc: '후면 원색' },
        { name: 'bSpcColor', desc: '후면 별색' },
        { name: 'bVedColor', desc: '후면 베다' },
        { name: 'bFluColor', desc: '후면 형광' },
        { name: 'prtJulsu', desc: '인쇄절수' },
        { name: 'daesu', desc: '대수', displayArea: 'detail' },
        { name: 'yeonsu', desc: '연수', displayArea: 'detail' },
        { name: 'extra', desc: '여분', displayArea: 'detail' },
        { name: 'totDosu', desc: '총도수', displayArea: 'detail' },
        { name: 'extra_do', desc: '여분 도당(매)', displayArea: 'detail' },
        { name: 'extra_yeon', desc: '여분_연당(%)', displayArea: 'detail' },
        { name: 'width', desc: '가로(mm)'},
        { name: 'height', desc: '세로(mm)'}
    ]
});