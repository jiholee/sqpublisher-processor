/**
 * Created by jiho on 15. 7. 20..
 */
Ext.define('sqpub.helper.BasicHelper', {
    setRecord: function(record) {
        this.record = record;
    },
    getRecord: function() {
        return this.record;
    },
    setParent: function(parent) {
        this.parent = parent;
    },
    getParent: function() {
        return this.parent;
    }
});