/**
 * Created by jiho on 14. 11. 24..
 *
 * 단가 > 제본_중철
 */
Ext.define('sqpub.store.basicInfo.cost.storeBindingChul', {
    extend: 'Ext.data.Store',
    storeId: 'storeBindingChul',
    model: 'sqpub.model.basicInfo.cost.bindingChul',
    autoLoad: true
});