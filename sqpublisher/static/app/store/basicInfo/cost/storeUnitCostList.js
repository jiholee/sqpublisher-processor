/**
 * Created by jiho on 14. 11. 22..
 */
Ext.define('sqpub.store.basicInfo.cost.storeUnitCostList', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.unitCostList',
    storeId: 'storeUnitCostList',
    data : [
        {name: '용지'},
        {name: '마스타제판'},
        {name: '마스타인쇄'},
        {name: '스캔'},
        {name: '편집'},
        {name: '시안'},
        {name: '교정인쇄'},
        {name: '필름출력'},
        {name: '대첩'},
        {name: '소부'},
        {name: '인쇄'},
        {name: '미싱'},
        {name: '오시'},
        {name: '코팅'},
        {name: '톰슨'},
        {name: '접지'},
        {name: '제본_무선'},
        {name: '제본_중철'}
    ]
});