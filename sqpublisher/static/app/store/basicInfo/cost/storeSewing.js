/**
 * Created by jiho on 14. 11. 22..
 *
 * 미싱
 */
Ext.define('sqpub.store.basicInfo.cost.storeSewing', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.sewing',
    storeId: 'storeSewing',
    autoLoad: true
});