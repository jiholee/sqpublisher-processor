/**
 * Created by jiho on 14. 11. 22..
 *
 * 오시
 */
Ext.define('sqpub.store.basicInfo.cost.storeOshi', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.oshi',
    storeId: 'storeOshi',
    autoLoad: true
});