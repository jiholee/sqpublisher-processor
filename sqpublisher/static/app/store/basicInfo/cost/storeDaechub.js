/**
 * Created by jiho on 14. 11. 22..
 *
 * 대첩
 */
Ext.define('sqpub.store.basicInfo.cost.storeDaechub', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.daechub',
    storeId: 'storeDaechub',
    autoLoad: true
});