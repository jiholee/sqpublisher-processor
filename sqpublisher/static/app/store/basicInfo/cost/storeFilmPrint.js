/**
 * Created by jiho on 14. 11. 22..
 *
 * 필름출력
 */
Ext.define('sqpub.store.basicInfo.cost.storeFilmPrint', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.filmPrint',
    storeId: 'storeFilmPrint',
    autoLoad: true
});