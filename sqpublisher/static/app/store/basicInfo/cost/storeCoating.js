/**
 * Created by jiho on 14. 11. 24..
 *
 * 코팅
 */
Ext.define('sqpub.store.basicInfo.cost.storeCoating', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.coating',
    storeId: 'storeCoating',
    autoLoad: true
});