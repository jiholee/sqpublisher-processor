/**
 * Created by jiho on 14. 11. 22..
 *
 * 편집
 */
Ext.define('sqpub.store.basicInfo.cost.storeEdit', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.edit',
    storeId: 'storeEdit',
    autoLoad: true
});
