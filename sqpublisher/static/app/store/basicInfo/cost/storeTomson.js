/**
 * Created by jiho on 14. 11. 24..
 *
 * 톰슨
 */
Ext.define('sqpub.store.basicInfo.cost.storeTomson', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.tomson',
    storeId: 'storeTomson',
    autoLoad: true
});
