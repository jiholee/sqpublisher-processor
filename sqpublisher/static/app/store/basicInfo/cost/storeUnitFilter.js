/**
 * Created by jiho on 14. 11. 23..
 *
 * 단가 > 미싱, 오시, 코팅, 톰슨에서 사용되는 연당, 장당 단가 콤보박스에 사용
 */
Ext.define('sqpub.store.basicInfo.cost.storeUnitFilter', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.unitFilter',
    storeId: 'storeUnitFilter',
    data: [
        { name: '연당단가', value: '연' },
        { name: '장당단가', value: '장' }
    ]
});
