/**
 * Created by jiho on 14. 11. 24..
 *
 * 단가 > 제본에서 상철, 좌철 표시
 */
Ext.define('sqpub.store.basicInfo.cost.storeBindMethod', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.bindMethod',
    storeId: 'storeBindMethod',
    data: [
        { method: '상철' },
        { method: '좌철' }
    ]
});
