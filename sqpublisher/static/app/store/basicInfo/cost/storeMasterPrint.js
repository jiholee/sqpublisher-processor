/**
 * Created by jiho on 14. 11. 22..
 *
 * 마스터제판
 */
Ext.define('sqpub.store.basicInfo.cost.storeMasterPrint', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.masterPrint',
    storeId: 'storeMasterPrint',
    autoLoad: true
});