/**
 * Created by jiho on 14. 11. 22..
 *
 * 소부
 */
Ext.define('sqpub.store.basicInfo.cost.storeSobu', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.sobu',
    storeId: 'storeDaechub',
    autoLoad: true
});