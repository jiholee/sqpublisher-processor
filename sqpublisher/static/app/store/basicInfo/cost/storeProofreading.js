/**
 * Created by jiho on 14. 11. 22..
 *
 * 교정인쇄
 */
Ext.define('sqpub.store.basicInfo.cost.storeProofreading', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.proofreading',
    storeId: 'storeProofreading',
    autoLoad: true,
    autoSync: true
});
