/**
 * Created by jiho on 14. 11. 24..
 *
 * 단가 > 제본_무선
 */
Ext.define('sqpub.store.basicInfo.cost.storeBindingWire', {
    extend: 'Ext.data.Store',
    storeId: 'storeBindingWire',
    model: 'sqpub.model.basicInfo.cost.bindingWire',
    autoLoad: true
});