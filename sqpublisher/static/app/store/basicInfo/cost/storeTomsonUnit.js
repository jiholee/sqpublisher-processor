/**
 * Created by jiho on 14. 11. 24..
 *
 * 단가 > 톰슨 종류
 */
Ext.define('sqpub.store.basicInfo.cost.storeTomsonUnit', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.tomsonUnit',
    storeId: 'storeTomsonUnit',
    data: [
        { method: '자동' },
        { method: '수동' }
    ]
});
