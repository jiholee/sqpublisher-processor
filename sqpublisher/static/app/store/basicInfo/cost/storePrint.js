/**
 * Created by jiho on 14. 11. 22..
 *
 * 단가 > 인쇄
 */
Ext.define('sqpub.store.basicInfo.cost.storePrint', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.print',
    storeId: 'storePrint',
    autoLoad: true
});