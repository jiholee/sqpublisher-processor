/**
 * Created by jiho on 14. 11. 22..
 *
 * 시안
 */
Ext.define('sqpub.store.basicInfo.cost.storeDraft', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.draft',
    storeId: 'storeDraft',
    autoLoad: true
});
