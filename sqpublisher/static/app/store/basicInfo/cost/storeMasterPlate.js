/**
 * Created by jiho on 14. 11. 22..
 *
 * 마스터인쇄
 */
Ext.define('sqpub.store.basicInfo.cost.storeMasterPlate', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.masterPlate',
    storeId: 'storeMasterPlate',
    autoLoad: true
});