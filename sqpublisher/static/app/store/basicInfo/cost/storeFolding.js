/**
 * Created by jiho on 14. 11. 24..
 *
 * 접지
 */
Ext.define('sqpub.store.basicInfo.cost.storeFolding', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.folding',
    storeId: 'storeFolding',
    autoLoad: true,
    autoSync: true
});