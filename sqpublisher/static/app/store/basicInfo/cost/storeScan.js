/**
 * Created by jiho on 14. 11. 22..
 *
 * 스캔
 */
Ext.define('sqpub.store.basicInfo.cost.storeScan', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.scan',
    storeId: 'storeScan',
    autoLoad: true,
    autoSync: true
});
