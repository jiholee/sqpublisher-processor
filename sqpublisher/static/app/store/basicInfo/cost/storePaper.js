/**
 * Created by jiho on 14. 11. 22..
 *
 * 용지
 */
Ext.define('sqpub.store.basicInfo.cost.storePaper', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.cost.paper',
    storeId: 'storePaper',
    autoLoad: true
});