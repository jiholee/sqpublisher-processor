/**
 * Created by jiho on 14. 11. 24..
 *
 * 거래처 구분
 */
Ext.define('sqpub.store.basicInfo.storeTradeGubun', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.tradeGubun',
    storeId: 'storeTradeGubun',
    data: [
        { idx: 1, gubun: '고객' },
        { idx: 2, gubun: '외주' }
    ]
});
