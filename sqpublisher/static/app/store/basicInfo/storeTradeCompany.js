/**
 * Created by jiho on 14. 11. 24..
 *
 * 거래처
 */
Ext.define('sqpub.store.basicInfo.storeTradeCompany', {
    extend: 'Ext.data.Store',
    storeId: 'storeTradeCompany',
    model: 'sqpub.model.basicInfo.tradeCompany',
    autoLoad: true
});