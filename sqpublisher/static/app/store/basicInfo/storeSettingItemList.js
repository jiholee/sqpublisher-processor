/**
 * Created by jiho on 14. 11. 22..
 *
 * * 기초자료 설정 항목을 담아두기 위한 스토어 선언
 *
 * 아이템:
 *      공정
 *      절수
 *      판형
 *      구분
 *      코팅
 *      손익분기
 *      기초자료
 */
Ext.define('sqpub.store.basicInfo.storeSettingItemList', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.settingItemList',
    storeId: 'storeBasicSettingItemList',
    data : [
        {name: '공정'},
        {name: '절수'},
        {name: '판형'},
        {name: '구분'},
        {name: '코팅'},
        {name: '손익분기'}
    ]
});