/**
 * Created by jiho on 14. 11. 22..
 */
Ext.define('sqpub.store.basicInfo.branchSeries.storeSeries', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.series',
    storeId: 'storeSeries',
    autoLoad: true
});