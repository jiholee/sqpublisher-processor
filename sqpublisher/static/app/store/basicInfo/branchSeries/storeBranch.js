/**
 * Created by jiho on 14. 11. 22..
 */
Ext.define('sqpub.store.basicInfo.branchSeries.storeBranch', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.branch',
    storeId: 'storeBranch',
    autoLoad: true
});