/**
 * Created by jiho on 14. 11. 22..
 */
Ext.define('sqpub.store.basicInfo.material.storeSeparation', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.material.separation',
    storeId: 'storeSeparation',
    autoLoad: true
});