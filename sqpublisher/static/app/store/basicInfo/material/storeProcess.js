/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.store.basicInfo.material.storeProcess', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.material.process',
    storeId: 'storeProcess',
    autoLoad: true,
    autoSync: true
});