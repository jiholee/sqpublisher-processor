/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.store.basicInfo.material.storeFormat', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.material.format',
    storeId: 'storeFormat',
    sorters: {
        property: 'no',
        direction: 'ASC'
    },
    sortRoot: 'data',
    remoteSort: false,
    autoLoad: true
});