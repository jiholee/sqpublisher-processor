/**
 * Created by jiho on 14. 11. 22..
 */
Ext.define('sqpub.store.basicInfo.material.storeCoating', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.material.coating',
    storeId: 'storeCoating',
    autoLoad: true
});