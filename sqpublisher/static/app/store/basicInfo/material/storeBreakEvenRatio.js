/**
 * Created by jiho on 14. 11. 22..
 */
Ext.define('sqpub.store.basicInfo.material.storeBreakEvenRatio', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.material.breakEvenRatio',
    storeId: 'storeBreakEvenRatio',
    autoLoad: true
});