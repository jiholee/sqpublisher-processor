/**
 * Created by jiho on 15. 07. 10..
 */
Ext.define('sqpub.store.basicInfo.material.storeCompany', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.material.company',
    storeId: 'processCompanyStore',
    autoLoad: true,
    setProcess: function(record) {
        this.record = record;
    },
    getProcess: function() {
        return this.record;
    }
});