/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.store.basicInfo.material.storeJulsu', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.material.julsu',
    storeId: 'storeJulsu',
    autoLoad: true
});