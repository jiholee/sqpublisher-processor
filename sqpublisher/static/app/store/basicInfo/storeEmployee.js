/**
 * Created by jiho on 14. 11. 25..
 *
 * 직원목록
 */
Ext.define('sqpub.store.basicInfo.storeEmployee', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.basicInfo.employee',
    storeId: 'storeEmployee',
    autoLoad: true
});
