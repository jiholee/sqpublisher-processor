/**
 * Created by jiho on 14. 12. 2..
 *
 * 리스트 검색 필드 지정
 */
Ext.define('sqpub.store.primeCost.storeSrchField', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.primeCost.srchField',
    storeId: 'storePrimeCostSrchField',
    data: [
        { text: '품명', field: 'name' },
        { text: '저자', field: 'author' }
    ]
});
