/**
 * Created by jiho on 14. 11. 28..
 */
Ext.define('sqpub.store.primeCost.storeDetail', {
    extend: 'Ext.data.Store',
    storeId: 'storePrimeCostDetail',
    model: 'sqpub.model.primeCost.detail',
    data: [
        {
            no: '1',
            process: '용지',
            gubun: '본문',
            faction: '',
            julsu: '',
            cnt: '1',
            unitCost: '0',
            etc: '160실키카펫/국횡',
            paperSubul: '',
            daesu: '',
            doneDng: ''
        }
    ]
});