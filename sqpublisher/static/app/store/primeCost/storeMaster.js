/**
 * Created by jiho on 14. 11. 28..
 */
Ext.define('sqpub.store.primeCost.storeMaster', {
    extend: 'Ext.data.Store',
    storeId: 'storePrimeCostMaster',
    model: 'sqpub.model.primeCost.master',
    data: [
        { 
            gubun: '본문',
            faction: '국횡',
            julsu: '8',
            page: '40',
            paper: '160실키카펫',
            fPriColor: '4',
            fSpcColor: '0',
            fVedColor: '0',
            fFluColor: '0',
            bPriColor: '4',
            bSpcColor: '0',
            bVedColor: '0',
            bFluColor: '0',
            prtJulsu: '1',
            daesu: '2.5',
            yeonsu: '0.01',
            extra: '0.0',
            totDosu: '8',
            extra_do: '50',
            extra_yeon: '0',
            width: '210',
            height: '250'
        }
    ]
});