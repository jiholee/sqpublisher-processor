/**
 * Created by jiho on 14. 11. 26..
 */
Ext.define('sqpub.store.primeCost.storePref', {
    extend: 'Ext.data.Store',
    storeId: 'storePrimeCostPref',
    model: 'sqpub.model.primeCost.pref',
    data: [
        {
            idx: 1,
            name: '안녕 병아리',
            unit: '권',
            cost: 10000,
            page: 46,
            format: '국배판',
            royaltyRatio: 0.1,
            faction: '국횡',
            julsu: '8',
            author: '한해숙 글/장호 그림',
            bind: '각양장(사철)',
            branch: 22,
            series: 76,
            branch_repr: '아동',
            series_repr: '유아',
            inji: '',
            isbn: '',
            clerk: '이호철',
            etc: '글 4% 그림 6%',
            reg_date: '2011-04-14',
            vatMethod: 1
        },
        {
            idx: 2,
            name: '안녕 병아리2',
            unit: '권',
            cost: 10000,
            page: 46,
            format: '국배판',
            royaltyRatio: 0.1,
            faction: '국횡',
            julsu: 8,
            author: '한해숙 글/장호 그림',
            bind: '각양장(사철)',
            branch: 22,
            series: 76,
            branch_repr: '아동',
            series_repr: '유아',
            inji: '',
            isbn: '',
            clerk: '이호철',
            etc: '글 4% 그림 6%',
            reg_date: '2011-04-14',
            vatMethod: 1
        }
    ]
});