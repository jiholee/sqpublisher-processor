/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.store.common.storeVatTotalCalc', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.common.vatTotalCalc',
    storeId: 'storeVatTotalCalc',
    data : [
        {idx: '1', name: '포함', desc: '예: 소계 + 10% = 청구금액 (부가세 포함)'},
        {idx: '2', name: '별도', desc: '예: 소계 = 청구금액 (부가세 별도)'},
        {idx: '3', name: '포함(단가에)', desc: '소계 = 청구금액 (부가세 포함)'}
    ]
});