/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.store.common.storeMoneyCalc', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.common.moneyCalc',
    storeId: 'storeMoneyCalc',
    data : [
        {idx: '1', name: '원단위 미만 절사', desc: '예: 11.8원 -> 11원'},
        {idx: '2', name: '원단위 미만 반올림', desc: '예: 11.8원 -> 12원'},
        {idx: '3', name: '원단위 절사', desc: '예: 11.8원 -> 10원'}
    ]
});
