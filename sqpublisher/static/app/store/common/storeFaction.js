/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.store.common.storeFaction', {
    extend: 'Ext.data.Store',
    model: 'sqpub.model.common.faction',
    storeId: 'storeFaction',
    data : [
        {name: '국'},
        {name: '46'},
        {name: '국횡'},
        {name: '46횡'}
    ]
});