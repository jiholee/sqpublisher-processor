/**
 * Created by jiho on 14. 11. 25..
 *
 * 대메뉴 > 용지
 */
Ext.define('sqpub.controller.Paper', {
    extend: 'Ext.app.Controller',

    stores: [],

    models: [],

    views: [],

    init: function() {
        this.control({
            'button[action=btnPaperBuy]': {
                // 용지 수불
                // pass
            },
            'button[action=btnPaperInventory]': {
                // 용지 재고
                // pass
            }
        });
    }
});