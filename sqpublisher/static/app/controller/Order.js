/**
 * Created by jiho on 14. 11. 25..
 *
 * 대메뉴 > 발주
 */
Ext.define('sqpub.controller.Order', {
    extend: 'Ext.app.Controller',

    stores: [],

    models: [],

    views: [],

    init: function() {
        this.control({
            'button[action=btnOrderListOpen]': {
                // pass
            }
        });
    }
});