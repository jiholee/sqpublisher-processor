/**
 * Created by jiho on 14. 11. 19..
 */
Ext.define('sqpub.controller.Basic', {
    extend: 'Ext.app.Controller',

    stores: [
        // 기초정보 > 기초자료
        'basicInfo.storeSettingItemList',
        'basicInfo.material.storeProcess',
        'basicInfo.material.storeCompany',
        'basicInfo.material.storeJulsu',
        'basicInfo.material.storeFormat',
        'basicInfo.material.storeSeparation',
        'basicInfo.material.storeCoating',
        'basicInfo.material.storeBreakEvenRatio',

        // 기초정보 > 분야/시리즈
        'basicInfo.branchSeries.storeBranch',
        'basicInfo.branchSeries.storeSeries',

        // 기초정보 > 단가
        'basicInfo.cost.storeUnitCostList',
        'basicInfo.cost.storePaper',
        'basicInfo.cost.storeMasterPlate',
        'basicInfo.cost.storeMasterPrint',
        'basicInfo.cost.storeScan',
        'basicInfo.cost.storeEdit',
        'basicInfo.cost.storeDraft',
        'basicInfo.cost.storeProofreading',
        'basicInfo.cost.storeFilmPrint',
        'basicInfo.cost.storeDaechub',
        'basicInfo.cost.storeSobu',
        'basicInfo.cost.storePrint',
        'basicInfo.cost.storeSewing',
        'basicInfo.cost.storeUnitFilter',
        'basicInfo.cost.storeOshi',
        'basicInfo.cost.storeCoating',
        'basicInfo.cost.storeTomson',
        'basicInfo.cost.storeBindMethod',
        'basicInfo.cost.storeTomsonUnit',
        'basicInfo.cost.storeFolding',
        'basicInfo.cost.storeBindingWire',
        'basicInfo.cost.storeBindingChul',

        // 기초정보 > 거래처목록
        'basicInfo.storeTradeCompany',
        'basicInfo.storeTradeGubun',

        // 기초정보 > 사원목록
        'basicInfo.storeEmployee'
    ],

    models: [
        // 기초정보 > 기초자료
        'basicInfo.settingItemList',
        'basicInfo.material.process',
        'basicInfo.material.company',
        'basicInfo.material.julsu',
        'basicInfo.material.format',
        'basicInfo.material.separation',
        'basicInfo.material.coating',
        'basicInfo.material.breakEvenRatio',

        // 기초정보 > 분야/시리즈
        'basicInfo.branch',
        'basicInfo.series',

        // 기초정보 > 단가
        'basicInfo.cost.unitCostList',
        'basicInfo.cost.paper',
        'basicInfo.cost.scan',
        'basicInfo.cost.proofreading',
        'basicInfo.cost.print',
        'basicInfo.cost.unitFilter',
        'basicInfo.cost.coating',
        'basicInfo.cost.bindMethod',
        'basicInfo.cost.tomson',
        'basicInfo.cost.tomsonUnit',
        'basicInfo.cost.folding',

        // 기초정보 > 거래처목록
        'basicInfo.tradeCompany',
        'basicInfo.tradeGubun',

        // 기초정보 > 사원목록
        'basicInfo.employee',

        // 기초정보 > 자사정보
        'basicInfo.baseCompanyInfo'
    ],

    views: [
        // 기초정보 > 자사자료
        'basicInfo.aboutUs.AboutUsWin',

        // 기초정보 > 기초자료
        'basicInfo.Container',
        'basicInfo.settingItemListContainer',
        'basicInfo.settingItemContainer',

        // 절수 콤보박스와 컨텍스트 메뉴
        'basicInfo.material.julsuCombo',
        'basicInfo.material.julsuContextMenu',

        // 기초정보 > 분야/시리즈
        'basicInfo.branchSeries.BranchSeries',
        
        // 기초정보 > 단가
        'basicInfo.UnitCost',
        'basicInfo.cost.paper',
        'basicInfo.cost.masterPlate',
        'basicInfo.cost.masterPrint',
        'basicInfo.cost.scan',
        'basicInfo.cost.edit',
        'basicInfo.cost.draft',
        'basicInfo.cost.proofreading',
        'basicInfo.cost.filmPrint',
        'basicInfo.cost.daechub',
        'basicInfo.cost.sobu',
        'basicInfo.cost.print',
        'basicInfo.cost.sewing',
        'basicInfo.cost.oshi',
        'basicInfo.cost.coating',
        'basicInfo.cost.tomson',
        'basicInfo.cost.folding',
        'basicInfo.cost.bindingWire',
        'basicInfo.cost.bindingChul',

        // 기초정보 > 거래처목록
        'basicInfo.tradeCompany',
        'basicInfo.tradeCompanyInfo',

        // 기초정보 > 사원목록
        'basicInfo.employee.employeeList',

        // 금옉 계산 방법 필드 컨테이너
        'basicInfo.moneyCalc',
        // 부가세 계산 방법 필드 컨테이너
        'basicInfo.vatMoneyCalc',
        // 부가세 종류
        'basicInfo.vatMethod'
    ],
    refs: [
        {
            ref: 'julsuComboBox',
            selector: 'gridpanel[name=format_panel] julsuCombo'
        },
        {
            ref: 'gubunSaveButton',
            selector: 'gridpanel[id=gubun_panel] button[action=btnGubunSave]'
        },
        {
            ref: 'formatSaveButton',
            selector: 'gridpanel[id=format_panel] button[action=btnFormatSave]'
        },
        {
            ref: 'coatingSaveButton',
            selector: 'gridpanel[id=coating_panel] button[action=btnCoatingSave]'
        },
        {
            ref: 'evenRatioSaveButton',
            selector: 'gridpanel[id=evenratio_panel] button[action=btnEvenRatioSave]'
        },
        {
            ref: 'branchSaveButton',
            selector: 'gridpanel[id=branch_panel] button[action=branchSaveBtn]'
        },
        {
            ref: 'seriesSaveButton',
            selector: 'gridpanel[id=series_panel] button[action=seriesSaveBtn]'
        },
        {
            ref: 'paperSaveButton',
            selector: 'gridpanel[id=paperCostPanel] button[action=btnSavePaperCost]'
        },
        {
            ref: 'bindingChulSaveButton',
            selector: 'button[action=btnSaveBindingChulCost]'
        },
        {
            ref: 'bindingWireSaveButton',
            selector: 'button[action=btnSaveBindingWireCost]'
        },
        {
            ref: 'coatingCostSaveButton',
            selector: 'button[action=btnSaveCoatingCost]'
        },
        {
            ref: 'daechubSaveButton',
            selector: 'button[action=btnSaveDaechubCost]'
        },
        {
            ref: 'draftSaveButton',
            selector: 'button[action=btnSaveDraftCost]'
        },
        {
            ref: 'editSaveButton',
            selector: 'button[action=btnSaveEditCost]'
        },
        {
            ref: 'filmPrintSaveButton',
            selector: 'button[action=btnSaveFilmPrintCost]'
        },
        {
            ref: 'masterPlateSaveButton',
            selector: 'button[action=btnSaveMasterPlateCost]'
        },
        {
            ref: 'masterPrintSaveButton',
            selector: 'button[action=btnSaveMasterPrintCost]'
        },
        {
            ref: 'oshiSaveButton',
            selector: 'button[action=btnSaveOshiCost]'
        },
        {
            ref: 'printSaveButton',
            selector: 'button[action=btnSavePrintCost]'
        },
        {
            ref: 'sewingSaveButton',
            selector: 'button[action=btnSaveSewingCost]'
        },
        {
            ref: 'sobuSaveButton',
            selector: 'button[action=btnSaveSobuCost]'
        },
        {
            ref: 'tomsonSaveButton',
            selector: 'button[action=btnSaveTomsonCost]'
        }
    ],
    mixins: [
        'sqpub.helper.BasicHelper'
    ],


    init: function() {
        this.control({
            'menuitem[action=btnCompanyInfoWin]': {
                click: this.onBtnCompanyInfoWin
            },
            'combo[name=vatCalcSel]': {
                change: this.changeCalcMethod
            },
            'combo[name=moneyCalcWay]': {
                change: this.changeCalcMethod
            },
            'combo[name=vatCalcWay]': {
                change: this.changeCalcMethod
            },
            'menuitem[action=btnBasicInfoTab]': {
                click: this.onBasicInfoTab
            },
            'gridpanel[name=basicData]': {
                itemclick: this.onBasicDataItemClick
            },
            'menuitem[action=btnBranchSeries]': {
                click: this.onBtnBranchSeries
            },
            'menuitem[action=btnUnitCost]': {
                click: this.onBtnUnitCost
            },
            'menuitem[action=btnClient]': {
                click: this.onBtnClient
            },
            'menuitem[action=btnEmployee]': {
                click: this.obBtnEmployee
            },
            'gridpanel[name=uniCostList]': {
                itemclick: this.onUnitCostListItemClick
            },
            'combo[name=sewingUnitCombo]': {
                select: this.onSelSewingUnitCombo,
                afterrender: this.onAfterRenderSewingUnitCombo
            },
            'combo[name=oshiUnitCombo]': {
                select: this.onSelSewingUnitCombo,
                afterrender: this.onAfterRenderSewingUnitCombo
            },
            'combo[name=coatingUnitFilterCombo]': {
                select: this.onSelSewingUnitCombo,
                afterrender: this.onAfterRenderSewingUnitCombo
            },
            'combo[name=coatingNameCombo]': {
                select: this.onSelCoatingNameCombo,
                afterrender: this.onAfterRenderCoatingNameCombo
            },
            'combo[name=tomsonUnitFilterCombo]': {
                select: this.onSelSewingUnitCombo,
                afterrender: this.onAfterRenderSewingUnitCombo
            },
            'gridpanel[name=tradeCompanyList]': {
                itemdblclick: this.onTradeCompanyDblClick
            },
            'button[action=btnTradeDelete]': {
                click: this.onBtnTradeDelete
            },
            'button[action=btnTradeCompanyAdd]': {
                click: this.onBtnTradeCompanyAdd
            },
            'button[action=btnCompanyInfoSave]': {
                click: this.onBtnCompanyInfoSave
            },
            'button[action=btnEmployeeAdd]': {
                click: this.onBtnEmployeeAdd
            },
            'button[action=btnEmployeeDel]': {
                click: this.onBtnEmployeeDel
            },
            'button[action=btnBasicCompanyInfoSave]': {
                click: this.onBtnBasicCompanyInfoSave
            },
            'form[id=baseCompanyInfoFormPanel]': {
                render: this.renderBaseCompanyInfoFormPanel
            },
            'combo[name=bar_gubun]': {
                select: this.onSelBarGubun
            },
            'textfield[name=tradeCompanyName]': {
                change: this.onChangeTradeCompanyName
            },
            'button[action=btnProcessCompanyAdd]': {
                click: this.onBtnProcessCompanyAdd
            },
            'button[action=btnProcessCompanyDel]': {
                click: this.onBtnProcessCompanyDel
            },
            'button[action=btnProcessCompanySave]': {
                click: this.onBtnProcessCompanySave
            },
            'gridpanel[id=process_panel]': {
                itemclick: this.onGridProcessClick
            },
            'gridpanel[id=process_company]': {
                render: this.onGridProcessCompanyRender
            },
            'gridpanel[id=julsu_panel]': {
                itemcontextmenu: this.onJulsuItemContextMenu,
                containercontextmenu: this.onJulsuContainerContextMenu
            },
            'menuitem[action=julsuContextMenuAddBtn]': {
                click: this.onMenuItemJulsuAddBtn
            },
            'menuitem[action=julsuContextMenuDelBtn]': {
                click: this.onMenuItemJulsuDelBtn
            },
            'menuitem[action=julsuContextMenuSaveBtn]': {
                click: this.onMenuItemJulsuSaveBtn
            },
            'gridpanel[id=format_panel] > gridview': {
                drop: this.onFormatPanelRecordDrop
            },
            'gridpanel[id=format_panel]': {
                itemcontextmenu: this.onFormatPanelItemContextMenu
            },
            'button[action=btnFormatAdd]': {
                click: this.onBtnFormatAdd
            },
            'button[action=btnFormatSave]': {
                click: this.onBtnFormatSave
            },
            'menuitem[action=formatContextMenuDelBtn]': {
                click: this.onMenuItemFormatDelBtn
            },
            'gridpanel[id=gubun_panel] > gridview': {
                drop: this.onGubunPanelRecordDrop
            },
            'gridpanel[id=gubun_panel]': {
                itemcontextmenu: this.onGubunPanelItemContextMenu
            },
            'button[action=btnGubunAdd]': {
                click: this.onBtnGubunAdd
            },
            'button[action=btnGubunSave]': {
                click: this.onBtnGubunSave
            },
            'menuitem[action=gubunContextMenuDelBtn]': {
                click: this.onMenuItemGubunDelBtn
            },
            'gridpanel[id=coating_panel]': {
                itemcontextmenu: this.onCoatingPanelItemContextMenu
            },
            'button[action=btnCoatingAdd]': {
                click: this.onBtnCoatingAdd
            },
            'button[action=btnCoatingSave]': {
                click: this.onBtnCoatingSave
            },
            'menuitem[action=coatingContextMenuDelBtn]': {
                click: this.onMenuItemCoatingDelBtn
            },
            'gridpanel[id=evenratio_panel]': {
                itemcontextmenu: this.onEvenRatioPanelItemContextMenu
            },
            'button[action=btnEvenRatioAdd]': {
                click: this.onBtnEvenRatioAdd
            },
            'button[action=btnEvenRatioSave]': {
                click: this.onBtnEvenRatioSave
            },
            'menuitem[action=evenRatioContextMenuDelBtn]': {
                click: this.onMenuItemEvenRatioDelBtn
            },
            'button[action=branchAddBtn]': {
                click: this.onBtnBranchAdd
            },
            'button[action=branchDelBtn]': {
                click: this.onBtnSeriesDel
            },
            'button[action=branchSaveBtn]': {
                click: this.onBtnBranchSave
            },
            'gridpanel[id=branch_panel] > gridview': {
                drop: this.onBranchPanelRecordDrop
            },
            'gridpanel[id=branch_panel]': {
                render: this.onGridBranchRender,
                itemclick: this.onBranchRecordClick
            },
            'button[action=seriesAddBtn]': {
                click: this.onBtnSeriesAdd
            },
            'button[action=seriesDelBtn]': {
                click: this.onBtnSeriesDel
            },
            'button[action=seriesSaveBtn]': {
                click: this.onBtnSeriesSave
            },
            'gridpanel[id=series_panel] > gridview': {
                drop: this.onSeriesPanelRecordDrop
            },
            'button[action=btnAddPaperCost]': {
                click: this.onBtnAddPaperCost
            },
            'button[action=btnDelPaperCost]': {
                click: this.onBtnDelPaperCost
            },
            'button[action=btnSavePaperCost]': {
                click: this.onBtnSavePaperCost
            },
            'textfield[name=find_textPaperCost]': {
                change: this.onChangeFindPaperCost
            },
            'button[action=btnAddBindingChulCost]': {
                click: this.onBtnAddBindingChulCost
            },
            'button[action=btnDelBindingChulCost]': {
                click: this.onBtnDelBindingChulCost
            },
            'button[action=btnSaveBindingChulCost]':  {
                click: this.onBtnSaveBindingChulCost
            },
            'button[action=btnAddBindingWireCost]':  {
                click: this.onBtnAddBindingWireCost
            },
            'button[action=btnDelBindingWireCost]':  {
                click: this.onBtnDelBindingWireCost
            },
            'button[action=btnSaveBindingWireCost]':  {
                click: this.onBtnSaveBindingWireCost
            },
            'button[action=btnAddCoatingCost]':  {
                click: this.onBtnAddCoatingCost
            },
            'button[action=btnDelCoatingCost]':  {
                click: this.onBtnDelCoatingCost
            },
            'button[action=btnSaveCoatingCost]': {
                click: this.onBtnSaveCoatingCost
            },
            'button[action=btnAddDaechubCost]': {
                click: this.onBtnAddDaechubCost
            },
            'button[action=btnDelDaechubCost]': {
                click: this.onBtnDelDaechubCost
            },
            'button[action=btnSaveDaechubCost]': {
                click: this.onBtnSaveDaechubCost
            },
            'button[action=btnAddDraftCost]': {
                click: this.onBtnAddDraftCost
            },
            'button[action=btnDelDraftCost]': {
                click: this.onBtnDelDraftCost
            },
            'button[action=btnSaveDraftCost]': {
                click: this.onBtnSaveDraftCost
            },
            'button[action=btnAddEditCost]': {
                click: this.onBtnAddEditCost
            },
            'button[action=btnDelEditCost]': {
                click: this.onBtnDelEditCost
            },
            'button[action=btnSaveEditCost]': {
                click: this.onBtnSaveEditCost
            },
            'button[action=btnAddFilmPrintCost]': {
                click: this.onBtnAddFilmPrintCost
            },
            'button[action=btnDelFilmPrintCost]': {
                click: this.onBtnDelFilmPrintCost
            },
            'button[action=btnSaveFilmPrintCost]': {
                click: this.onBtnSaveFilmPrintCost
            },
            'button[action=btnAddMasterPlateCost]': {
                click: this.onBtnAddMasterPlateCost
            },
            'button[action=btnDelMasterPlateCost]': {
                click: this.onBtnDelMasterPlateCost
            },
            'button[action=btnSaveMasterPlateCost]': {
                click: this.onBtnSaveMasterPlateCost
            },
            'button[action=btnAddMasterPrintCost]': {
                click: this.onBtnAddMasterPrintCost
            },
            'button[action=btnDelMasterPrintCost]': {
                click: this.onBtnDelMasterPrintCost
            },
            'button[action=btnSaveMasterPrintCost]': {
                click: this.onBtnSaveMasterPrintCost
            },
            'button[action=btnAddOshiCost]': {
                click: this.onBtnAddOshiCost
            },
            'button[action=btnDelOshiCost]': {
                click: this.onBtnDelOshiCost
            },
            'button[action=btnSaveOshiCost]': {
                click: this.onBtnSaveOshiCost
            },
            'button[action=btnAddPrintCost]': {
                click: this.onBtnAddPrintCost
            },
            'button[action=btnDelPrintCost]': {
                click: this.onBtnDelPrintCost
            },
            'button[action=btnSavePrintCost]': {
                click: this.onBtnSavePrintCost
            },
            'button[action=btnAddSewingCost]': {
                click: this.onBtnAddSewingCost
            },
            'button[action=btnDelSewingCost]': {
                click: this.onBtnDelSewingCost
            },
            'button[action=btnSaveSewingCost]': {
                click: this.onBtnSaveSewingCost
            },
            'button[action=btnAddSobuCost]': {
                click: this.onBtnAddSobuCost
            },
            'button[action=btnDelSobuCost]': {
                click: this.onBtnDelSobuCost
            },
            'button[action=btnSaveSobuCost]': {
                click: this.onBtnSaveSobuCost
            },
            'button[action=btnAddTomsonCost]': {
                click: this.onBtnAddTomsonCost
            },
            'button[action=btnDelTomsonCost]': {
                click: this.onBtnDelTomsonCost
            },
            'button[action=btnSaveTomsonCost]': {
                click: this.onBtnSaveTomsonCost
            },
            'combo[action=comboFactionCostPrint]': {
                change: this.onChangeFactionCostPrint
            },
            'combo[action=comboJulsuCostPrint]': {
                change: this.onChangeJulsuCostPrint
            },
            'button[action=btnAllViewCostPrint]': {
                click: this.onBtnAllViewCostPrint
            },
            'button[action=btnEmployeeInfoCreateSave]': {
                click: this.onBtnEmployeeInfoCreateSave
            },
            'button[action=btnEmployeeInfoEditSave]': {
                click: this.onBtnEmployeeInfoEditSave
            },
            'gridpanel[name=employeeList]': {
                itemdblclick: this.onEmployeeListDblClick
            },
            'textfield[name=findEmployeeName]': {
                change: this.onFindEmployeeNameChange
            }
        });
    },

    // 사원정보 텍스트박스 내용 입력
    onFindEmployeeNameChange: function(textfield, e, eOpts) {
        var store = textfield.up("gridpanel").getStore();
        var findEmployeeText = textfield.getValue();

        store.removeFilter('findEmployeeText');

        // 사원 이름 필터 만들기
        if (findEmployeeText != "") {
            var findEmployeeText = new Ext.util.Filter({
                property: 'name',
                value: findEmployeeText,
                root: 'data',
                id: 'findEmployeeText',
                anyMatch: true
            });

            store.addFilter([findEmployeeText]);
        }
    },

    // 사원정보 그리드에서 아이템 클릭
    onEmployeeListDblClick: function(gridpanel, record, item, index, e, eOpts) {
        var employeeInfoWin = Ext.getCmp('employeeInfoWin');

        // 비밀번호 빼주기
        record.set('password', '');

        if (!employeeInfoWin) {
            employeeInfoWin = Ext.create('sqpub.view.basicInfo.employee.employeeInfo', {
                title: '사원 정보 조회',
                id: 'employeeInfoWin'
            });
            employeeInfoWin.enableCreateMode(false);
            employeeInfoWin.enableModifyMode(true);
            employeeInfoWin.show(null, function() {
                employeeInfoWin.loadRecord(record);
            });
        } else {
            employeeInfoWin.toFront();
        }
    },

    // 사원정보 신규 저장
    onBtnEmployeeInfoCreateSave: function(button, e, eOpts) {
        var form = button.up("form");

        var employeeRecord = Ext.create('sqpub.model.basicInfo.employee', form.getValues());

        var gridStore = Ext.data.StoreManager.lookup('storeEmployee');
        gridStore.on('add', function(store, record, index, eOpts) {
            record[0].set('no', store.getCount() - 1);

            Ext.create('sqpub.view.ux.Notification', {
                title: 'Notification',
                position: 'tr',
                manager: 'dashboard',
                iconCls: 'ux-notification-icon-information',
                autoCloseDelay: 2000,
                spacing: 20,
                slideInDuration: 400,
                slideBackDuration: 1500,
                html: '잘 저장되었습니다.'
            }).show();

            store.sync();
            button.up("window").close();
        });
        gridStore.add(employeeRecord);
        gridStore.un('add');
    },

    // 사원정보 수정 저장
    onBtnEmployeeInfoEditSave: function(button, e, eOpts) {
        var form = button.up("form");
        var modifiedRecord = form.getRecord();

        var gridStore = Ext.data.StoreManager.lookup('storeEmployee');
        modifiedRecord.set(form.getValues());
        gridStore.sync();

        Ext.create('sqpub.view.ux.Notification', {
            title: 'Notification',
            position: 'tr',
            manager: 'dashboard',
            iconCls: 'ux-notification-icon-information',
            autoCloseDelay: 2000,
            spacing: 20,
            slideInDuration: 400,
            slideBackDuration: 1500,
            html: '잘 저장되었습니다.'
        }).show();

        button.up("window").close();
    },

    // 단가 > 인쇄 > 계열 콤보박스 클릭
    onChangeFactionCostPrint: function(combobox, newValue, oldValue, eOpts) {
        var store = combobox.up("gridpanel").getStore();

        // faction_filter 필터를 찾아서 먼저 제거
        store.removeFilter('faction_filter');

        // 계열 필터 만들기
        var faction_filter = new Ext.util.Filter({
            property: 'faction',
            value: combobox.getValue(),
            root: 'data',
            id: 'faction_filter',
            exactMatch: true
        });

        store.addFilter([faction_filter]);
    },

    // 단가 > 인쇄 > 절수 콤보박스 클릭
    onChangeJulsuCostPrint: function(combobox, newValue, oldValue, eOpts) {
        var store = combobox.up("gridpanel").getStore();

        // julsu_filter 필터를 찾아서 먼저 제거
        store.removeFilter('julsu_filter');

        // 계열 필터 만들기
        var julsu_filter = new Ext.util.Filter({
            property: 'julsu',
            value: combobox.getValue(),
            root: 'data',
            id: 'julsu_filter',
            exactMatch: true
        });

        store.addFilter([julsu_filter]);
    },

    // 단가 > 인쇄 > 전체보기 버튼 클릭
    onBtnAllViewCostPrint: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();

        // faction_filter 필터를 찾아서 먼저 제거
        store.removeFilter('faction_filter');

        // julsu_filter 필터를 찾아서 먼저 제거
        store.removeFilter('julsu_filter');

        var factionCombobox = Ext.ComponentQuery.query('combobox[action=comboFactionCostPrint]')[0];
        var julsuCombobox = Ext.ComponentQuery.query('combobox[action=comboJulsuCostPrint]')[0];

        factionCombobox.setValue('');
        julsuCombobox.setValue('');
    },

    // 제본_중철 추가
    onBtnAddBindingChulCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.bindingChul');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getBindingChulSaveButton().setIconCls('save');
    },

    // 제본_중철 삭제
    onBtnDelBindingChulCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getBindingChulSaveButton().setIconCls('save');
    },

    // 제본_중철 저장
    onBtnSaveBindingChulCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getBindingChulSaveButton().setIconCls('');
    },

    // 제본_무선 추가
    onBtnAddBindingWireCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.bindingWire');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getBindingWireSaveButton().setIconCls('save');
    },

    // 제본_무선 삭제
    onBtnDelBindingWireCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getBindingWireSaveButton().setIconCls('save');
    },

    // 제본_무선 저장
    onBtnSaveBindingWireCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getBindingWireSaveButton().setIconCls('');
    },

    // 코팅 추가
    onBtnAddCoatingCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.coating');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getCoatingCostSaveButton().setIconCls('save');
    },

    // 코팅 삭제
    onBtnDelCoatingCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getCoatingCostSaveButton().setIconCls('save');
    },

    // 코팅 저장
    onBtnSaveCoatingCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getCoatingCostSaveButton().setIconCls('');
    },

    // 대첩 추가
    onBtnAddDaechubCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.daechub');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getDaechubSaveButton().setIconCls('save');
    },

    // 대첩 삭제
    onBtnDelDaechubCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getDaechubSaveButton().setIconCls('save');
    },

    // 대첩 저장
    onBtnSaveDaechubCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getDaechubSaveButton().setIconCls('');
    },

    // 시안 추가
    onBtnAddDraftCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.draft');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getDraftSaveButton().setIconCls('save');
    },

    // 시안 삭제
    onBtnDelDraftCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getDraftSaveButton().setIconCls('save');
    },

    // 시안 저장
    onBtnSaveDraftCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getDraftSaveButton().setIconCls('');
    },

    // 편집 추가
    onBtnAddEditCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.edit');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getEditSaveButton().setIconCls('save');
    },

    // 편집 삭제
    onBtnDelEditCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getEditSaveButton().setIconCls('save');
    },

    // 편집 저장
    onBtnSaveEditCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getEditSaveButton().setIconCls('');
    },

    // 필름출력 추가
    onBtnAddFilmPrintCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.filmPrint');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getFilmPrintSaveButton().setIconCls('save');
    },

    // 필름출력 삭제
    onBtnDelFilmPrintCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getFilmPrintSaveButton().setIconCls('save');
    },

    // 필름출력 저장
    onBtnSaveFilmPrintCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getFilmPrintSaveButton().setIconCls('');
    },

    // 마스타제본 추가
    onBtnAddMasterPlateCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.masterPlate');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getMasterPlateSaveButton().setIconCls('save');
    },

    // 마스타제본 삭제
    onBtnDelMasterPlateCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getMasterPlateSaveButton().setIconCls('save');
    },

    // 마스타제본 저장
    onBtnSaveMasterPlateCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getMasterPlateSaveButton().setIconCls('');
    },

    // 마스타인쇄 추가
    onBtnAddMasterPrintCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.masterPrint');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getMasterPrintSaveButton().setIconCls('save');
    },

    // 마스타인쇄 삭제
    onBtnDelMasterPrintCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getMasterPrintSaveButton().setIconCls('save');
    },

    // 마스타인쇄 저장
    onBtnSaveMasterPrintCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getMasterPrintSaveButton().setIconCls('');
    },

    // 오시 추가
    onBtnAddOshiCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.oshi');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getOshiSaveButton().setIconCls('save');
    },

    // 오시 삭제
    onBtnDelOshiCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getOshiSaveButton().setIconCls('save');
    },

    // 오시 저장
    onBtnSaveOshiCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getOshiSaveButton().setIconCls('');
    },

    // 인쇄 추가
    onBtnAddPrintCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.print');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getPrintSaveButton().setIconCls('save');
    },

    // 인쇄 삭제
    onBtnDelPrintCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getPrintSaveButton().setIconCls('save');
    },

    // 인쇄 저장
    onBtnSavePrintCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getPrintSaveButton().setIconCls('');
    },

    // 미싱 추가
    onBtnAddSewingCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.sewing');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getSewingSaveButton().setIconCls('save');
    },

    // 미싱 삭제
    onBtnDelSewingCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getSewingSaveButton().setIconCls('save');
    },

    // 미싱 저장
    onBtnSaveSewingCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getSewingSaveButton().setIconCls('');
    },

    // 소부 추가
    onBtnAddSobuCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.sobu');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getSobuSaveButton().setIconCls('save');
    },

    // 소부 삭제
    onBtnDelSobuCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getSobuSaveButton().setIconCls('save');
    },

    // 소부 저장
    onBtnSaveSobuCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getSobuSaveButton().setIconCls('');
    },

    // 톰슨 추가
    onBtnAddTomsonCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.tomson');

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getTomsonSaveButton().setIconCls('save');
    },

    // 톰슨 삭제
    onBtnDelTomsonCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getTomsonSaveButton().setIconCls('save');
    },

    // 톰슨 저장
    onBtnSaveTomsonCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getTomsonSaveButton().setIconCls('');
    },

    // 종이 단가 찾기창에 내용 입력할 때마다 내용 찾기
    onChangeFindPaperCost: function(textfield, e, eOpts) {
        var store = textfield.up("gridpanel").getStore();
        var findText = textfield.getValue();

        // 단위 필터 만들기
        if (findText != "") {
            var paperKind_filter = new Ext.util.Filter({
                property: 'kind',
                value: findText,
                root: 'data',
                id: 'paperKind_filter',
                anyMatch: true
            });

            store.addFilter([paperKind_filter]);
        }
    },

    // 종이 단가 저장
    onBtnSavePaperCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getPaperSaveButton().setIconCls('');
    },

    // 종이 단가 삭제
    onBtnDelPaperCost: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getPaperSaveButton().setIconCls('save');
    },

    // 종이 단가 추가
    onBtnAddPaperCost: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var model = Ext.create('sqpub.model.basicInfo.cost.paper', {
            unit: '연'
        });

        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getPaperSaveButton().setIconCls('save');
    },

    // 분야 패널의 GridView Drop 이벤트
    onBranchPanelRecordDrop: function(node, data, overModel, dropPosition, eOpts) {
        // 드래그해서 조정한 순서대로 model의 no 속성을 조정해둔다.
        // 이렇게 한 이유는 하나씩 드래그 하면 드래그 바로 이전이나 이후의 것은 순서 조정이 되는데 반해
        // 그 외에 있는 것은 순서 조정이 되지 않기 때문.
        var store = data.view.up("gridpanel").getStore();

        Ext.each(store.data.items, function(record, index, allItems) {
            record.set('no', index);
        });

        store.sync();
    },

    // 분야 추가 버튼
    onBtnBranchAdd: function(button, e, eOpts) {
        var branchNameText = button.previousNode("textfield[name=branchName]");

        var model = Ext.create('sqpub.model.basicInfo.branch', {
            name: branchNameText.getValue()
        });

        branchNameText.setValue('');

        var store = button.up("gridpanel").getStore();
        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getBranchSaveButton().setIconCls('save');
    },

    // 분야 삭제 버튼
    onBtnBranchDel: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        var selectionModel = store.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getBranchSaveButton().setIconCls('save');
    },

    // 분야 저장 버튼
    onBtnBranchSave: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getBranchSaveButton().setIconCls('');
    },

    // 분야 패널이 처음 불려지면
    onGridBranchRender: function(gridpanel, eOpts) {
        // 분야 목록의 첫번째 행을 가져온다.
        var processStore = gridpanel.getStore();
        var firstRecord = processStore.getAt(0);

        var seriesStore = gridpanel.nextNode().getStore();

        // branch_filter 필터를 찾아서 먼저 제거
        seriesStore.removeFilter('branch_filter');

        if (firstRecord) {
            // 단위 필터 만들기
            var branch_filter = new Ext.util.Filter({
                property: 'branch_idx',
                value: firstRecord.get('idx'),
                root: 'data',
                id: 'branch_filter',
                exactMatch: true
            });

            seriesStore.addFilter([branch_filter]);
        }
    },

    // 분야 항목 선택
    onBranchRecordClick: function(gridpanel, record, item, index, e, eOpts) {
        var series_panel = gridpanel.nextNode();

        var seriesStore = series_panel.getStore();

        // branch_filter 필터를 찾아서 먼저 제거
        seriesStore.removeFilter('branch_filter');

        // 단위 필터 만들기
        var branch_filter = new Ext.util.Filter({
            property: 'branch_idx',
            value: record.get('idx'),
            root: 'data',
            id: 'branch_filter',
            exactMatch: true
        });

        seriesStore.addFilter([branch_filter]);
    },

    // 시리즈 패널의 GridView Drop 이벤트
    onSeriesPanelRecordDrop: function(node, data, overModel, dropPosition, eOpts) {
        // 드래그해서 조정한 순서대로 model의 no 속성을 조정해둔다.
        // 이렇게 한 이유는 하나씩 드래그 하면 드래그 바로 이전이나 이후의 것은 순서 조정이 되는데 반해
        // 그 외에 있는 것은 순서 조정이 되지 않기 때문.
        var store = data.view.up("gridpanel").getStore();

        Ext.each(store.data.items, function(record, index, allItems) {
            record.set('no', index);
        });

        store.sync();
    },

    // 시리즈 추가 버튼
    onBtnSeriesAdd: function(button, e, eOpts) {
        var seriesNameText = button.previousNode("textfield[name=seriesName]");

        var branch_panel = button.up("gridpanel").previousNode();
        var branch_panel_store = branch_panel.getStore();
        var branch_grid_selection_model = branch_panel.getSelectionModel();

        var record = null;
        if ( branch_grid_selection_model.hasSelection() ) {
            record = branch_grid_selection_model.getLastSelected();
        } else {
            record = branch_panel_store.getAt(0);
        }

        var model = Ext.create('sqpub.model.basicInfo.series', {
            name: seriesNameText.getValue(),
            branch_idx: record.get('idx')
        });

        seriesNameText.setValue('');

        var store = button.up("gridpanel").getStore();
        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getSeriesSaveButton().setIconCls('save');
    },

    // 시리즈 삭제 버튼
    onBtnSeriesDel: function(button, e, eOpts) {
        var store = button.up("gridpanel");
        var selectionModel = store.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }

        this.getSeriesSaveButton().setIconCls('save');
    },

    // 시리즈 저장 버튼
    onBtnSeriesSave: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getSeriesSaveButton().setIconCls('');
    },

    // 손익분기 패널의 레코드 컨텍스트 메뉴에서 삭제 선택
    onMenuItemEvenRatioDelBtn: function(item, e, eOpts) {
        var record = this.getRecord();

        if(record.get('idx') == 1) {
            Ext.Msg.show({
                title: '공급율 삭제 실패!',
                msg: '공급율은 삭제할 수 없습니다',
                icon: Ext.window.MessageBox.ERROR
            });

            e.stopEvent();
            return false;
        }

        var gridStore = this.getParent().getStore();
        gridStore.remove(record);

        this.getEvenRatioSaveButton().setIconCls('save');
    },

    // 손익분기 패널의 레코드에 대한 컨텍스트 메뉴 추가
    onEvenRatioPanelItemContextMenu: function(gridpanel, record, item, index, e, eOpts) {
        e.stopEvent();

        var contextMenu = Ext.create('sqpub.view.basicInfo.material.evenRatioContextMenu');

        this.setParent(gridpanel);
        this.setRecord(record);

        contextMenu.showAt(e.getXY());
    },

    // 손익분기 추가 버튼
    onBtnEvenRatioAdd: function(button, e, eOpts) {
        var evenRatioNameText = button.previousNode("textfield[name=evenRatioName]");
        var evenRatioText = button.previousNode("textfield[name=evenRatio]");

        var model = Ext.create('sqpub.model.basicInfo.material.breakEvenRatio', {
            name: evenRatioNameText.getValue(),
            ratio: parseFloat(evenRatioText.getValue() / 100)
        });

        evenRatioNameText.setValue('');
        evenRatioText.setValue('');

        var store = button.up("gridpanel").getStore();
        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getEvenRatioSaveButton().setIconCls('save');
    },

    // 손익분기 저장 버튼
    onBtnEvenRatioSave: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getEvenRatioSaveButton().setIconCls('');
    },

    // 코팅 패널의 레코드 컨텍스트 메뉴에서 삭제 선택
    onMenuItemCoatingDelBtn: function(item, e, eOpts) {
        var record = this.getRecord();
        var gridStore = this.getParent().getStore();
        gridStore.remove(record);

        this.getCoatingSaveButton().setIconCls('save');
    },

    // 코팅 패널의 레코드에 대한 컨텍스트 메뉴 추가
    onCoatingPanelItemContextMenu: function(gridpanel, record, item, index, e, eOpts) {
        e.stopEvent();

        var contextMenu = Ext.create('sqpub.view.basicInfo.material.coatingContextMenu');

        this.setParent(gridpanel);
        this.setRecord(record);

        contextMenu.showAt(e.getXY());
    },

    // 코팅 추가 버튼
    onBtnCoatingAdd: function(button, e, eOpts) {
        var coatingNameText = button.previousNode("textfield[name=coatingName]");

        var model = Ext.create('sqpub.model.basicInfo.material.coating', {
            name: coatingNameText.getValue()
        });

        coatingNameText.setValue('');

        var store = button.up("gridpanel").getStore();
        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getCoatingSaveButton().setIconCls('save');
    },

    // 코팅 저장 버튼
    onBtnCoatingSave: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getCoatingSaveButton().setIconCls('');
    },

    // 구분 패널의 레코드 컨텍스트 메뉴에서 삭제 선택
    onMenuItemGubunDelBtn: function(item, e, eOpts) {
        var record = this.getRecord();
        var gridStore = this.getParent().getStore();
        gridStore.remove(record);

        this.getGubunSaveButton().setIconCls('save');
    },

    // 구분 패널의 레코드에 대한 컨텍스트 메뉴 추가
    onGubunPanelItemContextMenu: function(gridpanel, record, item, index, e, eOpts) {
        e.stopEvent();

        var contextMenu = Ext.create('sqpub.view.basicInfo.material.gubunContextMenu');

        this.setParent(gridpanel);
        this.setRecord(record);

        contextMenu.showAt(e.getXY());
    },

    // 구분 패널의 GridView Drop 이벤트
    onGubunPanelRecordDrop: function(node, data, overModel, dropPosition, eOpts) {
        // 드래그해서 조정한 순서대로 model의 no 속성을 조정해둔다.
        // 이렇게 한 이유는 하나씩 드래그 하면 드래그 바로 이전이나 이후의 것은 순서 조정이 되는데 반해
        // 그 외에 있는 것은 순서 조정이 되지 않기 때문.
        var store = data.view.up("gridpanel").getStore();

        Ext.each(store.data.items, function(record, index, allItems) {
            record.set('no', index);
        });

        store.sync();
    },

    // 구분 추가 버튼
    onBtnGubunAdd: function(button, e, eOpts) {
        var gubunNameText = button.previousNode("textfield[name=gubunName]");

        var model = Ext.create('sqpub.model.basicInfo.material.separation', {
            name: gubunNameText.getValue()
        });

        gubunNameText.setValue('');

        var store = button.up("gridpanel").getStore();
        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getGubunSaveButton().setIconCls('save');
    },

    // 구분 저장 버튼
    onBtnGubunSave: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getGubunSaveButton().setIconCls('');
    },

    // 판형과 구분 패널에 아이템 추가 후
    onPanelStoreItemAdd: function(store, record, index, eOpts) {
        record[0].set('no', store.getCount() - 1);
    },

    // 판형 패널의 레코드 컨텍스트 메뉴에서 삭제 선택
    onMenuItemFormatDelBtn: function(item, e, eOpts) {
        var record = this.getRecord();
        var gridStore = this.getParent().getStore();
        gridStore.remove(record);

        this.getFormatSaveButton().setIconCls('save');
        //var saveButton2 = Ext.ComponentQuery.query('button[action=btnFormatSave]');
        //window.alpha = saveButton2;
        //saveButton2.setIconCls('save');
    },

    // 판형 패널의 레코드에 대한 컨텍스트 메뉴 추가
    onFormatPanelItemContextMenu: function(gridpanel, record, item, index, e, eOpts) {
        e.stopEvent();

        var contextMenu = Ext.create('sqpub.view.basicInfo.material.formatContextMenu');

        this.setParent(gridpanel);
        this.setRecord(record);

        contextMenu.showAt(e.getXY());
    },

    // 판형 패널의 GridView Drop 이벤트
    onFormatPanelRecordDrop: function(node, data, overModel, dropPosition, eOpts) {
        // 드래그해서 조정한 순서대로 model의 no 속성을 조정해둔다.
        // 이렇게 한 이유는 하나씩 드래그 하면 드래그 바로 이전이나 이후의 것은 순서 조정이 되는데 반해
        // 그 외에 있는 것은 순서 조정이 되지 않기 때문.
        var store = data.view.up("gridpanel").getStore();

        Ext.each(store.data.items, function(record, index, allItems) {
            record.set('no', index);
        });

        store.sync();
    },

    // 판형 추가 버튼
    onBtnFormatAdd: function(button, e, eOpts) {
        var formatNameText = button.previousNode("textfield[name=formatName]");
        var factionCombo = button.previousNode("combobox[name=factionCombo]");
        var julsuCombo = button.previousNode("combobox[name=julsuCombo]");

        var model = Ext.create('sqpub.model.basicInfo.material.format', {
            name: formatNameText.getValue(),
            faction: factionCombo.getValue(),
            julsu: julsuCombo.getValue()
        });

        formatNameText.setValue('');
        factionCombo.setValue('');
        julsuCombo.setValue('');

        var store = button.up("gridpanel").getStore();
        store.on('add', this.onPanelStoreItemAdd);
        store.insert(store.getCount(), model);
        store.un('add');

        this.getFormatSaveButton().setIconCls('save');
    },

    // 판형 저장 버튼
    onBtnFormatSave: function(button, e, eOpts) {
        var store = button.up("gridpanel").getStore();
        store.sync();

        this.getFormatSaveButton().setIconCls('');
    },

    // 자사정보 폼이 렌더링되면 서버로부터 정보 가져와서 출력하기
    renderBaseCompanyInfoFormPanel: function(form, eOpts) {
        sqpub.model.basicInfo.baseCompanyInfo.load(1, {
            success: function(record) {
                form.getForm().loadRecord(record);
            }
        });
    },

    // 절수 그리드 패널의 개별 레코드에서 Context Menu 띄우기
    onJulsuItemContextMenu: function(gridpanel, record, item, index, e, eOpts) {
        e.stopEvent();

        var contextMenu = Ext.create('sqpub.view.basicInfo.material.julsuContextMenu');
        contextMenu.items.get(0).hide();

        this.setParent(gridpanel);
        this.setRecord(record);

        contextMenu.showAt(e.getXY());
    },

    // 절수 그리드 패널에서 Context Menu 띄우기
    onJulsuContainerContextMenu: function(gridpanel, e, eOpts) {
        e.stopEvent();

        var contextMenu = Ext.create('sqpub.view.basicInfo.material.julsuContextMenu');
        contextMenu.items.get(1).hide();

        this.setParent(gridpanel);

        contextMenu.showAt(e.getXY());
    },

    // 절수 컨텍스트 추가 클릭
    onMenuItemJulsuAddBtn: function (item, e, eOpts) {
        var gridStore = this.getParent().getStore();

        var new_record = Ext.create('sqpub.store.basicInfo.material.storeJulsu');

        gridStore.add(new_record);

        this.getJulsuComboBox().getStore().load();
    },

    // 절수 컨텍스트 삭제 클릭
    onMenuItemJulsuDelBtn: function (item, e, eOpts) {
        var record = this.getRecord();
        var gridStore = this.getParent().getStore();
        gridStore.remove(record);
    },

    // 절수 컨텍스트 저장 클릭
    onMenuItemJulsuSaveBtn: function (item, e, eOpts) {
        var gridStore = this.getParent().getStore();
        gridStore.sync();
    },

    // 공정별 작업업체 리스트에서 저장 버튼 클릭한 경우
    onBtnProcessCompanySave: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        gridpanel.getStore().sync();
    },

    // 공정별 화면에서 공정 아이템을 클릭하면 오른쪽 공정별 회사 목록의 타이틀 변경하도록..
    onGridProcessClick: function(gridpanel, record, item, index, e, eOpts) {
        var process_company_panel = gridpanel.nextNode();

        var processCompanyStore = Ext.data.StoreManager.lookup('processCompanyStore');
        processCompanyStore.setProcess(record);

        // process_idx_filter 필터를 찾아서 먼저 제거
        processCompanyStore.removeFilter('process_idx_filter');

        // 단위 필터 만들기
        var process_idx_filter = new Ext.util.Filter({
            property: 'process_idx',
            value: record.get('idx'),
            root: 'data',
            id: 'process_idx_filter',
            exactMatch: true
        });

        processCompanyStore.addFilter([process_idx_filter]);

        process_company_panel.setTitle('공정별 작업업체 - ' + record.get('name'));
    },

    // 공정별 화면이 처음 렌더링되었을때 첫번째 공정의 작업 업체를 가져오기 위한 이벤트
    onGridProcessCompanyRender: function(gridpanel, eOpts) {
        // 공정 목록의 첫번째 행을 가져온다.
        var processStore = Ext.data.StoreManager.lookup('storeProcess');
        var firstRecord = processStore.getAt(0);

        var processCompanyStore = Ext.data.StoreManager.lookup('processCompanyStore');
        processCompanyStore.setProcess(firstRecord);

        // process_idx_filter 필터를 찾아서 먼저 제거
        processCompanyStore.removeFilter('process_idx_filter');

        var record_property_name = "";
        if (firstRecord) {
            // 단위 필터 만들기
            var process_idx_filter = new Ext.util.Filter({
                property: 'process_idx',
                value: firstRecord.get('idx'),
                root: 'data',
                id: 'process_idx_filter',
                exactMatch: true
            });

            processCompanyStore.addFilter([process_idx_filter]);

            record_property_name = " - " + firstRecord.get('name');
        }

        gridpanel.setTitle('공정별 작업업체' + record_property_name);
    },

    // 공정별 작업업체 그리드 패널에서 추가 버튼 클릭
    onBtnProcessCompanyAdd: function(button, e, eOpts) {
        var combobox = button.previousNode();

        var gridpanel = button.up("gridpanel");

        var currentGridStore = gridpanel.getStore();
        var process_record = currentGridStore.getProcess();

        var new_record = Ext.create('sqpub.model.basicInfo.material.company', {
            process_idx: process_record.get('idx'),
            trade_idx: combobox.getValue()
        });

        combobox.setValue('');

        currentGridStore.add(new_record);
        currentGridStore.sync();
    },

    // 공정별 작업업체 그리드 패널에서 삭제 버튼 클릭
    onBtnProcessCompanyDel: function(button, e, eOpts) {
        var gridpanel = button.up("gridpanel");
        var currentGridStore = gridpanel.getStore();

        var selModel = gridpanel.getSelectionModel();
        var selections = selModel.getSelection();

        currentGridStore.remove(selections);
        currentGridStore.sync();
    },

    // 거래처 목록에서 거래처를 입력했을때 자동으로 검색해주기
    onChangeTradeCompanyName: function(textfield, newValue, oldValue, eOpts) {
        var store = textfield.up("gridpanel").getStore();

        // 구분 필터를 찾아서 먼저 제거
        store.removeFilter('nameFilter');

        // 단위 필터 만들기
        var nameFilter = new Ext.util.Filter({
            property: 'name',
            value: newValue,
            root: 'data',
            id: 'nameFilter',
            anyMatch: true
        });

        store.addFilter([nameFilter]);
    },

   // 거래처 목록에서 구분 선택했을때에 대한 이벤트 처리
    onSelBarGubun: function(combo, records, eOpts) {
        var store = combo.up("gridpanel").getStore();

        // 구분 필터를 찾아서 먼저 제거
        store.removeFilter('gubunFilter');

        // 단위 필터 만들기
        var gubunFilter = new Ext.util.Filter({
            property: 'gubun',
            value: records[0].get('gubun'),
            root: 'data',
            id: 'gubunFilter'
        });

        store.addFilter([gubunFilter]);
    },

    // 자사정보 저장
    onBtnBasicCompanyInfoSave: function(button, e, eOpts) {
        var form = button.up("form");
        var basicCompanyRecord = form.getRecord();
        form.getForm().updateRecord(basicCompanyRecord);
        basicCompanyRecord.save({
            callback: function(records, operation, success) {
                Ext.create('sqpub.view.ux.Notification', {
                    title: 'Notification',
                    position: 'tr',
                    manager: 'dashboard',
                    iconCls: 'ux-notification-icon-information',
                    autoCloseDelay: 2000,
                    spacing: 20,
                    slideInDuration: 400,
                    slideBackDuration: 1500,
                    html: '잘 저장되었습니다.'
                }).show();
            }
        });
    },

    // 기초데이터 아이템 클릭하면 발생하는 이벤트
    onBasicDataItemClick: function(view, record, item, index, e, eOpts ) {
        var dependPanel = view.up().up().next().getLayout();
        dependPanel.setActiveItem(index);
    },

    // 자사정보 탭 띄우기
    onBtnCompanyInfoWin: function() {
        //Ext.create('sqpub.view.basicInfo.aboutUs.AboutUsWin').show();
        var addedTab = Ext.getCmp('center').add(Ext.create('sqpub.view.basicInfo.aboutUs.AboutUsWin', {
            closable: true
        }));
        Ext.getCmp('center').setActiveTab(addedTab);
    },

    // 기초정보 탭 띄우기
    onBasicInfoTab: function() {
        var menuSelTab = Ext.getCmp('basicInfoTab');

        if (!menuSelTab) {
            var addedTab = Ext.getCmp('center').add({
                xtype: 'BasicInfoContainer',
                closable: true,
                id: 'basicInfoTab'
            });

            Ext.getCmp('center').setActiveTab(addedTab);
        } else {
            Ext.getCmp('center').setActiveTab(menuSelTab);
        }
    },

    // 분야/시리즈 탭 띄우기
    onBtnBranchSeries: function() {
        var menuSelTab = Ext.getCmp('BranchSeries');

        if (!menuSelTab) {
            var addedTab = Ext.getCmp('center').add({
                xtype: 'BranchSeries',
                closable: true,
                id: 'BranchSeries'
            });

            Ext.getCmp('center').setActiveTab(addedTab);
        } else {
            Ext.getCmp('center').setActiveTab(menuSelTab);
        }
    },

    // 단가 탭 띄우기
    onBtnUnitCost: function() {
        var menuSelTab = Ext.getCmp('UnitCost');

        if (!menuSelTab) {
            var addedTab = Ext.getCmp('center').add(
                Ext.create('sqpub.view.basicInfo.UnitCost', {
                    closable: true,
                    id: 'UnitCost'
                })
            );

            Ext.getCmp('center').setActiveTab(addedTab);
        } else {
            Ext.getCmp('center').setActiveTab(menuSelTab);
        }
    },

    // 단가에서 세부항목 띄우기
    onUnitCostListItemClick: function(view, record, item, index, e, eOpts ) {
        // 단가 > 세부항목들 보여줄 때 사용..
        var dependPanel = view.up().up().next().getLayout();
        dependPanel.setActiveItem(index);
    },

    // 거래처 항목 띄우기
    onBtnClient: function() {
        var menuSelTab = Ext.getCmp('tradeCompany');

        if (!menuSelTab) {
            var addedTab = Ext.getCmp('center').add(
                Ext.create('sqpub.view.basicInfo.tradeCompany', {
                    closable: true,
                    id: 'tradeCompany'
                })
            );

            Ext.getCmp('center').setActiveTab(addedTab);
        } else {
            Ext.getCmp('center').setActiveTab(menuSelTab);
        }
    },

    // 사원목록 띄우기
    obBtnEmployee: function() {
        var menuSelTab = Ext.getCmp('tabEmployeeList');

        if (!menuSelTab) {
            var addedTab = Ext.getCmp('center').add(
                Ext.create('sqpub.view.basicInfo.employee.employeeList', {
                    closable: true,
                    id: 'tabEmployeeList'
                })
            );

            Ext.getCmp('center').setActiveTab(addedTab);
        } else {
            Ext.getCmp('center').setActiveTab(menuSelTab);
        }
    },

    // 미싱항목 콤보박스 선택
    onSelSewingUnitCombo: function(combo, records, eOpts) {
        var store = combo.up().up().getStore();

        // 단위 필터를 찾아서 먼저 제거
        store.removeFilter('unitFilter');

        // 단위 필터 만들기
        var unitFilter = new Ext.util.Filter({
            property: 'unit',
            value: records[0].get('value'),
            root: 'data',
            id: 'unitFilter'
        });

        store.addFilter([unitFilter]);
    },

    // 미시항목 렌더링 한 다음에 수행
    onAfterRenderSewingUnitCombo: function(combo, eOpts) {
        var store = combo.up().up().getStore();

        // 단위 필터 만들기
        var unitFilter = new Ext.util.Filter({
            property: 'unit',
            value: combo.getValue(),
            root: 'data',
            id: 'unitFilter'
        });

        store.addFilter([unitFilter]);
    },

    // 코팅 콤보박스 선택
    onSelCoatingNameCombo: function(combo, records, eOpts) {
        var store = combo.up().up().getStore();

        // 코팅 필터를 찾아서 먼저 제거(코팅 필터가 전체-필터없음-로 되어 있어도 수행)
        store.removeFilter('coating');

        if (records[0].get('name') != '전체') {
            // 코팅 필터 만들기
            var coatingFilter = new Ext.util.Filter({
                property: 'name',
                value: records[0].get('name'),
                root: 'data',
                id: 'coating'
            });

            store.addFilter([coatingFilter]);
        }
    },

    // 코팅 콤보박스 렌더링 한 다음에 수행
    onAfterRenderCoatingNameCombo: function(combo, eOpts) {
        var store = combo.getStore();

        store.insert(0, { name: '전체' });
    },

    // 거래처 그리드에서 더블 클릭
    onTradeCompanyDblClick: function ( view, record, item, index, e, eOpts ) {
        var tradeComapnyInfoWin = Ext.getCmp('idTradeCompanyInfoWin');

        if (!tradeComapnyInfoWin) {
            tradeCompanyInfoWin = Ext.create('sqpub.view.basicInfo.tradeCompanyInfo', {
                title: '거래처 정보 조회',
                id: 'idTradeCompanyInfoWin'
            });
            tradeCompanyInfoWin.show(null, function() {
                tradeCompanyInfoWin.loadRecord(record);
            });
        } else {
            tradeComapnyInfoWin.toFront();
        }
    },

    // 거래처 그리드에서 삭제 버튼 클릭
    onBtnTradeDelete: function(button, e, opts) {
        var selModel = button.up("gridpanel").getSelectionModel();
        var store = button.up("gridpanel").getStore();

        if ( selModel.hasSelection() ) {
            var selectionModels = selModel.getSelection();

            Ext.Msg.show({
                title:'거래처를 정말 삭제하시겠습니까?',
                msg: selectionModels[0].get('name') + ' 를 삭제합니까?<br>' +
                    ' 주의: 이 거래처와 관련된 모든 자료 단가표, 견적서, 거래명세표,<br>' +
                    ' 매입매출, 작업지시, 인쇄, 용지수불도 함께 삭제됩니다.',
                buttons: Ext.Msg.YESNO,
                icon: Ext.Msg.QUESTION,
                fn: function(buttonId, text, opt) {
                    if (buttonId == 'yes') {
                        selectionModels[0].destroy();
                        store.remove(selectionModels);
                    }
                }
            });
        } else {
            Ext.Msg.alert('삭제오류!', '삭제할 거래처가 선택되지 않았습니다');
        }
    },

    // 거래처 정보 추가 윈도우 띄우기
    onBtnTradeCompanyAdd: function() {
        var tradeComapnyAddWin = Ext.getCmp('idTradeCompanAddWin');

        if (!tradeComapnyAddWin) {
            tradeComapnyAddWin = Ext.create('sqpub.view.basicInfo.tradeCompanyInfo', {
                title: '거래처 추가',
                id: 'idTradeCompanAddWin'
            });
            tradeComapnyAddWin.show();
        } else {
            tradeComapnyAddWin.toFront();
        }
    },

    // 거래처 정보 저장 버튼 Click
    onBtnCompanyInfoSave: function(button, e, opts) {
        var form = button.up("form").getForm();
        // form에서 getRecord를 바로 호출하면 예상하는 대로 동작을 하지 않아서 부득이하게 우회 처리함
        // 스토어를 통하면 반응을 안하는 것 같기도...
        var tradeCompanyRecord = Ext.create('sqpub.model.basicInfo.tradeCompany', form.getValues());

        var gridStore = Ext.data.StoreManager.lookup('storeTradeCompany');

        if (tradeCompanyRecord.get('idx') != "") {
            // 거래처 목록의 리스트 내용 갱신
            var findRecord = gridStore.findRecord('idx', tradeCompanyRecord.get('idx'));

            findRecord.setRecord(tradeCompanyRecord);
            findRecord.commit();

            tradeCompanyRecord.save({
                callback: function(records, operation, success) {
                    Ext.create('sqpub.view.ux.Notification', {
                        title: 'Notification',
                        position: 'tr',
                        manager: 'dashboard',
                        iconCls: 'ux-notification-icon-information',
                        autoCloseDelay: 2000,
                        spacing: 20,
                        slideInDuration: 400,
                        slideBackDuration: 1500,
                        html: '잘 저장되었습니다.'
                    }).show();

                    button.up("window").close();
                }
            });

        } else {
            tradeCompanyRecord.save({
                callback: function(records, operation, success) {
                    Ext.create('sqpub.view.ux.Notification', {
                        title: 'Notification',
                        position: 'tr',
                        manager: 'dashboard',
                        iconCls: 'ux-notification-icon-information',
                        autoCloseDelay: 2000,
                        spacing: 20,
                        slideInDuration: 400,
                        slideBackDuration: 1500,
                        html: '잘 저장되었습니다.'
                    }).show();

                    button.up("window").close();
                }
            });
        }
    },

    // 사원 추가
    onBtnEmployeeAdd: function(button, e, opts) {
        var employeeAddWin = Ext.getCmp('employeeAddWin');

        if (!employeeAddWin) {
            employeeAddWin = Ext.create('sqpub.view.basicInfo.employee.employeeInfo', {
                title: '사원 추가',
                id: 'employeeAddWin'
            });
            employeeAddWin.enableCreateMode(true);
            employeeAddWin.enableModifyMode(false);
            employeeAddWin.show();
        } else {
            employeeAddWin.toFront();
        }
    },

    // 사원 삭제
    onBtnEmployeeDel: function(button, e, opts) {
        var gridpanel = button.up("gridpanel");
        var store = gridpanel.getStore();
        var selectionModel = gridpanel.getSelectionModel();

        if (selectionModel.hasSelection()) {
            var record = selectionModel.getLastSelected();
            store.remove(record);
            store.sync();

            Ext.create('sqpub.view.ux.Notification', {
                title: 'Notification',
                position: 'tr',
                manager: 'dashboard',
                iconCls: 'ux-notification-icon-information',
                autoCloseDelay: 2000,
                spacing: 20,
                slideInDuration: 400,
                slideBackDuration: 1500,
                html: '잘 삭제되었습니다.'
            }).show();
        } else {
            Ext.Msg.show({
                title: '삭제 실패!',
                msg: '삭제할 항목을 선택해주십시오',
                icon: Ext.window.MessageBox.ERROR
            });
        }
    },

    // 부가세, 금액계산 콤보박스 선택했을때 수행
    changeCalcMethod: function (combo, newValue, oldValue, eOpts ) {
        var record = combo.findRecordByValue(newValue);
        var comboDescLabel = combo.next();

        if(record)
            comboDescLabel.setText(record.get('desc'));
    }
});