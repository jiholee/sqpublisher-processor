/**
 * Created by jiho on 14. 11. 25..
 *
 * 대메뉴 > 원가 목록 / 발주
 */
Ext.define('sqpub.controller.PrimeCost', {
    extend: 'Ext.app.Controller',

    stores: [
        'primeCost.storePref',
        'primeCost.storeMaster',
        'primeCost.storeDetail',
        'primeCost.storeSrchField',

        // 기초정보 > 분야/시리즈
        'basicInfo.branchSeries.storeBranch',
        'basicInfo.branchSeries.storeSeries'
    ],

    models: [
        'primeCost.pref',
        'primeCost.master',
        'primeCost.detail',
        'primeCost.srchField',

        // 기초정보 > 분야/시리즈
        'basicInfo.branch',
        'basicInfo.series'
    ],

    views: [
        'primeCost.list',
        'primeCost.order',

        'basicInfo.branchSeries.branchCombo',
        'basicInfo.branchSeries.seriesCombo',
        'basicInfo.material.formatCombo'
    ],

    init: function() {
        this.control({
            'button[action=btnPrimeCostListOpen]': {
                click: this.onBtnPrimeCostListOpen
            },
            'gridpanel[name=primeCostList]': {
                itemdblclick: this.onPrimeCostListItemDblClick,
                itemclick: this.onPrimeCostListItemClick,
                show: this.onPrimeCostListShow
            },
            'button[action=btnOrdering]': {
                click: this.onBtnOrdering
            },
            'panel[id=east]': {
                collapse: this.panelEastCollapse
            },
            'button[action=btnPrimeCostItemCopy]': {
                click: this.onBtnPrimeCostItemCopy
            },
            'button[action=btnPrimeCostItemDelete]': {
                click: this.onBtnPrimeCostItemDelete
            },
            'button[action=btnPrimeCostItemAdd]': {
                click: this.onBtnPrimeCostItemAdd
            },
            'combobox[name=branchCombo]': {
                render: this.onBranchComboRender,
                change: this.onBranchComboChange
            },
            'combobox[name=seriesCombo]': {
                render: this.onSeriesComboRender,
                change: this.onSeriesComboChange
            },
            'button[action=btnOrderProcessStart]': {
                click:this.onBtnOrderProcessStart
            },
            'button[action=btnOrderPanelClose]': {
                click: this.onBtnOrderPanelClose
            },
            'combo[name=vatCalcSel]': {
                change: this.changeCalcMethod
            },
            'branchCombo': {
                changeBranch: this.onChangeBranch
            }
        });
    },

    // 원가등록에서 분야 선택시 할 일
    onChangeBranch: function(comp, target_comp, newValue, oldValue, eOpts) {
        var seriesStore = target_comp.getStore();

        // branch_filter 필터를 찾아서 먼저 제거
        seriesStore.removeFilter('branch_filter');

        // 분야 필터 만들기
        var branch_filter = new Ext.util.Filter({
            property: 'branch_idx',
            value: newValue,
            root: 'data',
            id: 'branch_filter',
            exactMatch: true
        });

        seriesStore.addFilter([branch_filter]);
    },

    // 원가목록에서 시리즈 내용을 변경할 경우 수행
    onSeriesComboChange: function(comp, newValue, oldValue, eOpts) {
        // 원가목록 시리즈 필터링
        var prime_series_filter = new Ext.util.Filter({
            property: 'series',
            value: newValue,
            root: 'data',
            id: 'prime_series_filter',
            exactMatch: true
        });

        var primeCostStore = comp.up("gridpanel").getStore();
        primeCostStore.removeFilter('prime_series_filter');
        if (newValue != 0) {
            primeCostStore.addFilter([prime_series_filter]);
        }
    },

    // 원가목록에서 분야 콤보박스가 처음 렌더링된 경우 수행
    onBranchComboRender: function(comp, eOpts) {
        var store = comp.getStore();

        store.on("load", function(tStore, records, successful, eOpts) {
            var itemAllRecord = Ext.create('sqpub.model.basicInfo.branch', {
                idx: 0,
                no: 0,
                name: '전체'
            });
            tStore.insert(0, itemAllRecord);
            comp.setValue(0);
        });
    },

    // 원가목록에서 시리즈 콤보박스가 처음 렌더링된 경우 수행
    onSeriesComboRender: function(comp, eOpts) {
        var store = comp.getStore();

        store.on("load", function(tStore, records, successful, eOpts) {
            var itemAllRecord = Ext.create('sqpub.model.basicInfo.series', {
                idx: 0,
                no: 0,
                branch_idx: 0,
                name: '전체'
            });
            tStore.insert(0, itemAllRecord);
            comp.setValue(0);
        });
    },

    // 원가등록 1단계에서 부가세 종류를 선택한 경우에 수행
    changeCalcMethod: function (combo, newValue, oldValue, eOpts ) {
        var record = combo.findRecordByValue(newValue);
        var comboDescLabel = combo.next();

        if(record) {
            comboDescLabel.setText(record.get('desc'));
        }
    },

    // 발주창에서 발주하기
    onBtnOrderProcessStart: function(comp, e, eOpts) {
        console.log(comp.up("form").getValues());
        alert("기능 준비중입니다");
    },

    // 발주창에서 닫기 버튼 클릭
    onBtnOrderPanelClose: function(comp, e, eOpts) {
        var window = comp.up("window");
        window.close();
    },

    // 원가 리스트에서 분야 콤보박스 항목을 변경할 경우 실행
    onBranchComboChange: function(comp, newValue, oldValue, eOpts) {
        var series_combo = comp.nextNode();
        series_combo.setValue(0);

        var seriesStore = series_combo.getStore();

        var exists_series_index = seriesStore.find('idx', 0);
        if (exists_series_index > -1) {
            var ItemAllRecord = seriesStore.getAt(exists_series_index);
            ItemAllRecord.set('branch_idx', newValue);
        }

        // branch_filter 필터를 찾아서 먼저 제거
        seriesStore.removeFilter('branch_filter');

        var branch_filter = new Ext.util.Filter({
            property: 'branch_idx',
            value: newValue,
            root: 'data',
            id: 'branch_filter',
            exactMatch: true
        });

        seriesStore.addFilter([branch_filter]);

        // 원가목록 분야 필터링
        var prime_branch_filter = new Ext.util.Filter({
            property: 'branch',
            value: newValue,
            root: 'data',
            id: 'prime_branch_filter',
            exactMatch: true
        });

        var primeCostStore = comp.up("gridpanel").getStore();
        primeCostStore.removeFilter('prime_branch_filter');
        if (newValue != 0) {
            primeCostStore.addFilter([prime_branch_filter]);
        }
    },

    onBtnPrimeCostItemCopy: function(button, e, opts) {
        // gridPanel tbar에 있는 button item

        // 선택되어 있는 아이템 가져오기
        var gridSelModel = button.up('grid').getSelectionModel();
        var selRecord = gridSelModel.getLastSelected();

        Ext.Msg.show({
            title: '원가 복사',
            msg: '[' + selRecord.get('name') + ']<br><br>선택된 항목을 복사할까요?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                // yes, no에 대한 처리
            }
        });
    },

    onBtnPrimeCostItemDelete: function(button, e, opts) {
        // gridPanel tbar에 있는 button item
        // 선택되어 있는 아이템 가져오기
        var gridSelModel = button.up('grid').getSelectionModel();
        var selRecord = gridSelModel.getLastSelected();

        Ext.Msg.show({
            title: '원가 삭제',
            msg: '[' + selRecord.get('name') + ']<br><br>선택된 항목을 삭제할까요?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                // yes, no에 대한 처리
            }
        });
    },

    onBtnPrimeCostItemAdd: function(button, e, opts) {
        var primeCostAddWin = Ext.getCmp('primeCostAddWin');

        if (!primeCostAddWin) {
            primeCostAddWin = Ext.create('sqpub.view.primeCost.info', {
                title: '원가 추가',
                id: 'primeCostAddWin'
            });
            primeCostAddWin.show();
        } else {
            primeCostAddWin.toFront();
        }
    },

    onBtnPrimeCostListOpen: function(button, e, opts) {
        var menuSelTab = Ext.getCmp('tabPrimeCostList');

        if (!menuSelTab) {
            var addedTab = Ext.getCmp('center').add(
                Ext.create('sqpub.view.primeCost.list', {
                    closable: true,
                    id: 'tabPrimeCostList'
                })
            );

            Ext.getCmp('center').setActiveTab(addedTab);
        } else {
            Ext.getCmp('center').setActiveTab(menuSelTab);
        }
    },

    onPrimeCostListItemDblClick: function ( view, record, item, index, e, eOpts ) {
        console.log('item을 더블 클릭하셨네요');
    },

    onPrimeCostListItemClick: function ( view, record, item, index, e, eOpts ) {
        // 발주 창이 열려져 있는지 확인해서 열려 있으면 선택한 원가 목록의 발주 내용을 보여준다.
        var gridPanel = view.up('gridpanel[name=primeCostList]');
        var gridSelModel = gridPanel.getSelectionModel();

        var optionalPanel = Ext.getCmp('east');

        if (!optionalPanel.isHidden() && optionalPanel.title == '발주') {
            // 선택된 레코드 저장
            var selRecord = gridSelModel.getSelection()[0];

            // 발주 폼 가져오기
            var orderForm = optionalPanel.down('primeCostOrder');
            if (orderForm) {
                orderForm.loadRecord(selRecord);
            }
        }
    },

    onPrimeCostListShow: function (gridpanel, eOpts) {
        var gridSelModel = gridpanel.getSelectionModel();

        // 항상 최상위 레코드 선택하기
        gridSelModel.select(0);
    },

    onBtnOrdering: function (button, e, eOpts) {
        var orderWin = Ext.getCmp('orderWin');

        // 선택된 레코드 저장
        var gridSelModel = button.up("gridpanel").getSelectionModel();
        var selRecord = gridSelModel.getLastSelected();

        if (!orderWin) {
            orderWin = Ext.create('sqpub.view.primeCost.order', {
                title: '발주',
                id: 'orderWin'
            });
            orderWin.loadRecord(selRecord);
            orderWin.show();
        } else {
            orderWin.toFront();
        }
    },

    panelEastCollapse: function(panel, eOpts) {
        Ext.getCmp('east').setTitle('Optional');
        Ext.getCmp('east').removeAll();
    }
});