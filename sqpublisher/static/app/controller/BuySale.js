/**
 * Created by jiho on 14. 11. 25..
 *
 * 대메뉴 > 매입매출
 */
Ext.define('sqpub.controller.BuySale', {
    extend: 'Ext.app.Controller',

    stores: [],

    models: [],

    views: [],

    init: function() {
        this.control({
            'button[action=btnBuySaleListOpen]': {
                // pass
            }
        });
    }
});