/**
 * Created by jiho on 14. 11. 18..
 */
Ext.define('sqpub.controller.Dashboard', {
    extend: 'Ext.app.Controller',
    stores: [],
    models: [],

    views: ['Dashboard'],

    init: function() {
        this.control({
            'menuitem[action=btn_logout]': {
                click: this.onBtnLogout
            },
            'menuitem[action=btn_member_modify]': {
                click: this.onBtnMemberModify
            }
        });
    },

    onBtnLogout: function() {
        Ext.Ajax.request({
            url: '/req_logout',
            method: 'post',
            success: function(response) {
                var json = Ext.JSON.decode(response.responseText);

                if (json['success']) {
                    location.href = '/';
                }
            }
        });
    },

    onBtnMemberModify: function() {
        alert('기능 준비중입니다');
    }
});