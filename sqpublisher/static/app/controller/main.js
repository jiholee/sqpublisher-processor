/**
 * Created by jiho on 14. 12. 4..
 *
 * 메인 컨트롤러(어디에도 포함되기 어려워보이는 것만 담아둠)
 */
Ext.define('sqpub.controller.main', {
    extend: 'Ext.app.Controller',

    stores: [
        // 금액계산방법
        'common.storeMoneyCalc',
        'common.storeVatTotalCalc',

        // 계열
        'common.storeFaction'
    ],

    models: [
        // 금액계산방법
        'common.moneyCalc',
        'common.vatTotalCalc',

        // 계열
        'common.faction'
    ],

    views: [
        // 계열 콤보박스
        'factionCombo'
    ],

    init: function() {
        this.control({
            'tabpanel[id=center]': {
                tabchange: function ( tabPanel, newCard, oldCard, eOpts ) {
                    Ext.getCmp('east').collapse();
                    Ext.getCmp('east').removeAll();
                }
            }
        });

        this.callParent(arguments);
    }
});