/**
 * Created by jiho on 14. 11. 25..
 *
 * 대메뉴 > 손익분기
 */
Ext.define('sqpub.controller.BreakEven', {
    extend: 'Ext.app.Controller',

    stores: [],

    models: [],

    views: [],

    init: function() {
        this.control({
            'button[action=btnBreakEvenListOpen]': {
                // pass
            }
        });
    }
});