/**
 * Created by jiho on 14. 11. 25..
 *
 * 대메뉴 > 외주
 */
Ext.define('sqpub.controller.OutsideOrder', {
    extend: 'Ext.app.Controller',

    stores: [],

    models: [],

    views: [],

    init: function() {
        this.control({
            'button[action=btnOutsideOrderTotalListOpen]': {
                // 외주 전체
                // pass
            },
            'button[action=btnOutsideOrderTradeListOpen]': {
                // 외주 거래처별
                // pass
            }
        });
    }
});