/**
 * Created by jiho on 14. 12. 3..
 *
 * 원가 목록에서 발주 화면 띄우기
 */
Ext.define('sqpub.view.primeCost.order', {
    extend: 'Ext.window.Window',
    alias: 'widget.primeCostOrder',

    initComponent: function() {
        Ext.apply(this, {
            layout: 'fit',
            width: 310,
            border: false,
            style: {
                borderColor: 'white'
            },
            items: {
                xtype: 'form',
                margin: '10 10 10 10',
                border: false,
                style: {
                    borderColor: 'white'
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'idx'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: '도서명',
                        name: 'name',
                        labelAlign: 'left'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: '발주수량',
                        labelAlign: 'left',
                        name: 'orderNum'
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            pack: 'end'
                        },
                        width: 260,
                        items: [
                            {
                                xtype: 'button',
                                text: '발주',
                                action: 'btnOrderProcessStart',
                                margin: '10 10 0 0'
                            },
                            {
                                xtype: 'button',
                                text: '닫기',
                                action: 'btnOrderPanelClose',
                                margin: '10 0 0 0'
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        margin: '10 0 0 0',
                        html: '수량에 따른 금액을 재계산하여 발주목록에' + '<br>' +
                        ' 기록합니다. 기본수량이나 기본금액을 적용하는 공정의' + '<br>' +
                        ' 경우, 계산된 값과 금액이 정확한지 반드시 확인하시기' + '<br>' +
                        ' 바랍니다.'
                    }
                ]
            }
        });

        this.callParent(arguments);
    },

    loadRecord: function (record) {
        this.down("form").getForm().loadRecord(record);
    }
});