/**
 * Created by jiho on 14. 12. 2..
 *
 * 원가 목록 리스트
 */
Ext.define('sqpub.view.primeCost.list', {
    extend: 'Ext.grid.Panel',
    title: '원가 목록',
    name: 'primeCostList',
    id: 'primeCostList',
    requires: [
        'sqpub.store.basicInfo.branchSeries.storeBranch',
        'sqpub.store.basicInfo.branchSeries.storeSeries'
    ],

    initComponent: function() {
        Ext.apply(this, {
            tbar: [
                {
                    xtype: 'combo',
                    store: Ext.create('sqpub.store.primeCost.storeSrchField'),
                    queryMode: 'local',
                    displayField: 'text',
                    valueField: 'value',
                    width: 140,
                    value: '품명',
                    fieldLabel: '검색어',
                    labelWidth: 60,
                    name: 'srchFieldCombo'
                },
                {
                    xtype: 'textfield',
                    size: 160,
                    name: 'srchFieldName'
                },
                {
                    xtype: 'button',
                    text: '찾기',
                    action: 'btnPrimeCostFind'
                },
                {
                    xtype: 'combo',
                    store: Ext.create('sqpub.store.basicInfo.branchSeries.storeBranch'),
                    queryMode: 'local',
                    displayField: 'name',
                    valueField: 'idx',
                    width: 160,
                    fieldLabel: '분야',
                    labelWidth: 40,
                    name: 'branchCombo'
                },
                {
                    xtype: 'combo',
                    store: Ext.create('sqpub.store.basicInfo.branchSeries.storeSeries'),
                    queryMode: 'local',
                    displayField: 'name',
                    valueField: 'idx',
                    fieldLabel: '시리즈',
                    width: 230,
                    labelWidth: 60,
                    name: 'seriesCombo'
                },
                '->',
                {
                    xtype: 'button',
                    text: '발주',
                    action: 'btnOrdering'
                },
                {
                    xtype: 'button',
                    text: '복사',
                    action: 'btnPrimeCostItemCopy'
                },
                {
                    xtype: 'button',
                    text: '삭제',
                    action: 'btnPrimeCostItemDelete'
                },
                {
                    xtype: 'button',
                    text: '추가',
                    action: 'btnPrimeCostItemAdd'
                }
            ],
            store: Ext.create('sqpub.store.primeCost.storePref'),
            columns: [
                {
                    text: '품명',
                    dataIndex: 'name'
                },
                {
                    text: '판형',
                    dataIndex: 'format'
                },
                {
                    text: '쪽수',
                    dataIndex: 'page'
                },
                {
                    text: '제본',
                    dataIndex: 'bind'
                },
                {
                    text: '가격',
                    dataIndex: 'cost',
                    renderer: function(value) {
                        return Ext.util.Format.number(value, "0,0");
                    }
                },
                {
                    text: '인세율',
                    dataIndex: 'royaltyRatio',
                    renderer: function(value) {
                        var ratio = parseInt(parseFloat(value) * 100);
                        return ratio.toString() + '%';
                    }
                },
                {
                    text: '저자',
                    dataIndex: 'author'
                },
                {
                    text: '분야',
                    dataIndex: 'branch',
                    renderer: function(value, metaData, record) {
                        return record.get('branch_repr');
                    }
                },
                {
                    text: '시리즈',
                    dataIndex: 'series',
                    renderer: function(value, metaData, record) {
                        return record.get('series_repr');
                    }
                },
                {
                    text: '비고',
                    dataIndex: 'etc'
                },
                {
                    text: '인지',
                    dataIndex: 'inji'
                },
                {
                    text: 'ISBN',
                    dataIndex: 'isbn'
                },
                {
                    text: '등록일',
                    dataIndex: 'reg_date'
                }
            ]
        });

        this.callParent(arguments);
    }
});