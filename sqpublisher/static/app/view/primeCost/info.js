/**
 * Created by jiho on 15. 7. 24..
 *
 * 원가 추가/수정 기초사항
 */
Ext.define('sqpub.view.primeCost.info', {
    extend: 'Ext.window.Window',
    alias: 'widget.primeCostInfo',

    initComponent: function () {
        Ext.apply(this, {
            layout: 'fit',
            width: 800,
            border: false,
            style: {
                borderColor: 'white'
            },
            items: {
                xtype: 'form',
                margin: '10 10 10 10',
                border: false,
                style: {
                    borderColor: 'white'
                },
                layout: {
                    type: 'table',
                    columns: 2,
                        tdAttrs: {
                            style: {
                                width: '385px'
                            }
                        }
                },
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: '품명',
                        labelWidth: 70,
                        width: 770,
                        colspan: 2
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: '단위',
                        width: 265,
                        labelWidth: 70
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: '가격',
                        width: 385,
                        labelWidth: 70
                    },
                    {
                        xtype: 'formatCombo',
                        fieldLabel: '판형',
                        labelWidth: 70,
                        width: 265
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: '인세율',
                        width: 385,
                        labelWidth: 70
                    },
                    {
                        xtype: 'fieldcontainer',
                        fieldLabel: '계열 / 절수',
                        labelWidth: 70,
                        layout: 'hbox',
                        width: 385,
                        items: [
                            {
                                xtype: 'factionCombo',
                                width: 120
                            },
                            {
                                xtype: 'julsuCombo',
                                width: 60,
                                margin: '0 0 0 10'
                            }
                        ]
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: '저자',
                        rowspan: 2,
                        labelWidth: 70,
                        width: 385,
                        rows: 3
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: '제본',
                        labelWidth: 70,
                        width: 265
                    },
                    {
                        xtype: 'branchCombo',
                        fieldLabel: '분야',
                        width: 370,
                        labelWidth: 70,
                        seriesCombo: 'series'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: '인지',
                        width: 385,
                        labelWidth: 70
                    },
                    {
                        xtype: 'seriesCombo',
                        fieldLabel: '시리즈',
                        width: 370,
                        labelWidth: 70,
                        name: 'series'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'ISBN',
                        width: 385,
                        labelWidth: 70
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: '견적인',
                        labelWidth: 70,
                        width: 265
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: '비고',
                        rowspan: 2,
                        labelWidth: 70,
                        width: 385,
                        rows: 3
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: '등록일',
                        labelWidth: 70,
                        width: 265
                    },
                    {
                        xtype: 'basicVatMethod',
                        labelWidth: 70,
                        colspan: 2,
                        width: 770,
                        fieldWidth: 190,
                        ComboFieldName: 'vatCalcSel'
                    },
                    {
                        xtype: 'gridpanel',
                        store: Ext.create('sqpub.store.primeCost.storeMaster'),
                        colspan: 2,
                        height: 150,
                        columns: [
                            { text: '구분', locked: true, dataIndex: 'gubun' },
                            { text: '계열', locked: true, dataIndex: 'faction' },
                            { text: '절수', width: 53, locked: true, dataIndex: 'julsu' },
                            { text: '쪽수', width: 53, locked: true, dataIndex: 'page' },
                            { text: '종이종류', locked: true, dataIndex: 'paper' },
                            { text: '계열', locked: true, dataIndex: 'paper_paction' },
                            { text: '원색', width: 53, dataIndex: 'fPriColor' },
                            { text: '별색', width: 53, dataIndex: 'fSpcColor' },
                            { text: '베다', width: 53, dataIndex: 'fVedColor' },
                            { text: '형광', width: 53, dataIndex: 'fFluColor' },
                            { text: '원색', width: 53, dataIndex: 'bPriColor' },
                            { text: '별색', width: 53, dataIndex: 'bSpcColor' },
                            { text: '베다', width: 53, dataIndex: 'bVedColor' },
                            { text: '형광', width: 53, dataIndex: 'bFluColor' },
                            { text: '인쇄절수', width: 73, dataIndex: 'prtJulsu' },
                            { text: '가로(mm)', width: 73, dataIndex: 'width' },
                            { text: '세로(mm)', width: 73, dataIndex: 'height' }
                        ],
                        bbar: [
                            {
                                xtype: 'button',
                                text: '추가'
                            },
                            {
                                xtype: 'button',
                                text: '삭제'
                            }
                        ],
                        margin: '0 0 10 0'
                    }
                ],
                bbar: [
                    '->',
                    {
                        xtype: 'button',
                        text: '취소/삭제'
                    },
                    {
                        xtype: 'button',
                        text: '다음'
                    },
                    {
                        xtype: 'button',
                        text: '마침'
                    }
                ]
            }
        });

        this.callParent(arguments);
    }
});
