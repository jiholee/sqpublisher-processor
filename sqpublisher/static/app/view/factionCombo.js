/**
 * Created by jiho on 14. 12. 7..
 *
 * 계열 콤보 박스
 */
Ext.define('sqpub.view.factionCombo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.factionCombo',

    initComponent: function() {
        Ext.apply(this, {
            store: Ext.create('sqpub.store.common.storeFaction'),
            queryMode: 'local',
            displayField: 'name',
            valueField: 'name'
        });

        this.callParent(arguments);
    }
});