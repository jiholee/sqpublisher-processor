/**
 * Created by jiho on 14. 11. 24..
 *
 * 거래처 목록
 */
Ext.define('sqpub.view.basicInfo.tradeCompany', {
    extend: 'Ext.container.Container',
    alias: 'widget.tradeCompany',

    initComponent: function() {
        Ext.apply(this, {
            layout: 'fit',
            title: '거래처 목록',
            items: [
                {
                    xtype: 'gridpanel',
                    store: Ext.create('sqpub.store.basicInfo.storeTradeCompany'),
                    tbar: [
                        {
                            xtype: 'textfield',
                            fieldLabel: '거래처명',
                            labelWidth: 80,
                            name: 'tradeCompanyName'
                        },
                        {
                            xtype: 'combo',
                            store: Ext.create('sqpub.store.basicInfo.storeTradeGubun'),
                            queryMode: 'local',
                            displayField: 'gubun',
                            valueField: 'gubun',
                            name: 'bar_gubun',
                            fieldLabel: '구분',
                            labelWidth: 60
                        },
                        '->',
                        {
                            xtype: 'button',
                            text: '추가',
                            action: 'btnTradeCompanyAdd'
                        },
                        {
                            xtype: 'button',
                            text: '삭제',
                            action: 'btnTradeDelete'
                        }
                    ],
                    name: 'tradeCompanyList',
                    columns: [
                        { text: '고유번호', dataIndex: 'idx' },
                        { text: '상호', dataIndex: 'name' },
                        { text: '전화번호', dataIndex: 'tel' },
                        { text: '팩스', dataIndex: 'fax' },
                        { text: 'EMail', dataIndex: 'email' },
                        { text: '담당자', dataIndex: 'manager' },
                        { text: '핸드폰', dataIndex: 'mobile' },
                        { text: '비고', dataIndex: 'etc' },
                        { text: '등록번호', dataIndex: 'num' },
                        { text: '성명', dataIndex: 'ceo' },
                        { text: '주소', dataIndex: 'addr' },
                        { text: '업태', dataIndex: 'uptae' },
                        { text: '종목', dataIndex: 'jm' },
                        {
                            text: '금액계산방법',
                            dataIndex: 'moneyCalcWay',
                            renderer: function(value) {
                                if (!!!value) return '';

                                var store = Ext.data.StoreManager.lookup('storeMoneyCalc');
                                if (!store) {
                                    store = Ext.create('sqpub.store.common.storeMoneyCalc');
                                }

                                var record = store.findRecord('idx', value);

                                return record.get('name');
                            }
                        },
                        {
                            text: '부가세계산방법',
                            dataIndex: 'vatCalcWay',
                            renderer: function(value) {
                                if (!!!value) return '';

                                var store = Ext.data.StoreManager.lookup('storeMoneyCalc');

                                var record = store.findRecord('idx', value);

                                return record.get('name');
                            }
                        },
                        {
                            text: '부가세종류',
                            dataIndex: 'vatCalcSel',
                            renderer: function(value) {
                                if (!!!value) return '';

                                var store = Ext.data.StoreManager.lookup('storeVatTotalCalc');
                                if (!store) {
                                    store = Ext.create('sqpub.store.common.storeVatTotalCalc');
                                }

                                var record = store.findRecord('idx', value);

                                return record.get('name');
                            }
                        },
                        {
                            text: '구분',
                            dataIndex: 'gubun',
                            renderer: function(value) {
                                if (!!!value) return '';

                                var store = Ext.data.StoreManager.lookup('storeTradeGubun');
                                if (!store) {
                                    store = Ext.create('sqpub.store.basicInfo.storeTradeGubun');
                                }

                                var record = store.findRecord('idx', value);

                                return record.get('gubun');
                            }
                        }
                    ]
                }
            ]
        });

        this.callParent(arguments);
    }
});
