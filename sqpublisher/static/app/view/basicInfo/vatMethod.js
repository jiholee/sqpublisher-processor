/**
 * Created by jiho on 14. 12. 8..
 *
 * 부가세 포함 방법 필드 컨테이너
 */
Ext.define('sqpub.view.basicInfo.vatMethod', {
    extend: 'Ext.form.FieldContainer',
    alias: 'widget.basicVatMethod',
    width: 700,
    layout: {
        type: 'hbox',
        align: 'middle'
    },
    fieldLabel: '부가세 종류',
    labelWidth: 100,

    initComponent: function() {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'combo',
                    store: Ext.create('sqpub.store.common.storeVatTotalCalc'),
                    queryMode: 'local',
                    displayField: 'name',
                    valueField: 'idx',
                    name: this.ComboFieldName,
                    width: this.fieldWidth ? this.fieldWidth : 140,
                    value: this.value ? this.value : ''
                },
                {
                    xtype: 'label',
                    text: '',
                    margin: '0 0 0 8px'
                }
            ]
        });

        this.callParent(arguments);
    }
});