/**
 * Created by jiho on 14. 12. 7..
 *
 * 기초정보 > 기초자료 > 설정 항목 컨테이너
 *
 * 작성이유: 컨테이너 파일 분리
 */
Ext.define('sqpub.view.basicInfo.settingItemListContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.basicInfoSettingItemListContainer',
    width: 150,
    layout: 'fit',
    items: [
        {
            xtype: 'gridpanel',
            hideHeaders: true,
            height: '100%',
            store:
                Ext.create('sqpub.store.basicInfo.storeSettingItemList'),
            columns: [
                { dataIndex: 'name', width: '100%' }
            ],
            name: 'basicData'
        }
    ]
});