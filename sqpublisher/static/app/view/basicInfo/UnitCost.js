/**
 * Created by jiho on 14. 11. 21..
 */
Ext.define('sqpub.view.basicInfo.UnitCost', {
    extend: 'Ext.container.Container',
    alias: 'widget.UnitCost',
    layout: 'hbox',

    initComponent: function() {
        Ext.apply(this, {
            title: '단가',
            items: [
                {
                    xtype: 'container',
                    width: 150,
                    layout: 'fit',
                    items: [
                        {
                            xtype: 'gridpanel',
                            hideHeaders: true,
                            height: '100%',
                            store: Ext.create('sqpub.store.basicInfo.cost.storeUnitCostList'),
                            columns: [
                                { dataIndex: 'name', width: '100%' }
                            ],
                            name: 'uniCostList'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    layout: 'card',
                    height: '100%',
                    flex: 1,
                    items: [
                        {
                            xtype: 'unitCost.paper'
                        },
                        {
                            xtype: 'unitCost.masterPlate'
                        },
                        {
                            xtype: 'unitCost.masterPrint'
                        },
                        {
                            xtype: 'unitCost.scan'
                        },
                        {
                            xtype: 'unitCost.edit'
                        },
                        {
                            xtype: 'unitCost.draft'
                        },
                        {
                            xtype: 'unitCost.proofreading'
                        },
                        {
                            xtype: 'unitCost.filmPrint'
                        },
                        {
                            xtype: 'unitCost.daechub'
                        },
                        {
                            xtype: 'unitCost.sobu'
                        },
                        {
                            xtype: 'unitCost.print'
                        },
                        {
                            xtype: 'unitCost.sewing'
                        },
                        {
                            xtype: 'unitCost.oshi'
                        },
                        {
                            xtype: 'unitCost.coating'
                        },
                        {
                            xtype: 'unitCost.tomson'
                        },
                        {
                            xtype: 'unitCost.folding'
                        },
                        {
                            xtype: 'unitCost.bindingWire'
                        },
                        {
                            xtype: 'unitCost.bindingChul'
                        }
                    ]
                }
            ]
        });

        this.callParent(arguments);
    }
});