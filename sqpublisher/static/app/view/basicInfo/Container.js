/**
 * Created by jiho on 14. 11. 21..
 *
 * 기초정보 > 기초자료 메인 탭
 */
Ext.define('sqpub.view.basicInfo.Container', {
    extend: 'Ext.container.Container',
    alias: 'widget.BasicInfoContainer',
    layout: 'hbox',

    initComponent: function() {
        Ext.apply(this, {
            title: '기초자료',
            items: [
                {
                    xtype: 'basicInfoSettingItemListContainer'
                },
                {
                    xtype: 'basicInfoSettingItemContainer'
                }
            ]
        });

        this.callParent(arguments);
    }
});