/**
 * Created by jiho on 15. 7. 25..
 *
 * 판형 콤보
 */
Ext.define('sqpub.view.basicInfo.material.formatCombo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.formatCombo',

    initComponent: function () {
        Ext.apply(this, {
            store: Ext.create('sqpub.store.basicInfo.material.storeFormat'),
            queryMode: 'local',
            displayField: 'name',
            valueField: 'idx',
            fieldLabel: '판형'
        });

        this.callParent(arguments);
    }
});
