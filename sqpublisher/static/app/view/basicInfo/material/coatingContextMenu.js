/**
 * Created by jiho on 15. 7. 14..
 *
 * 코팅 > 그리드 내 컨텍스트 메뉴
 */
Ext.define('sqpub.view.basicInfo.material.coatingContextMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.coatingContextMenu',

    initComponent: function() {
        Ext.apply(this, {
            width: 100,
            items: [
                {
                    text: '삭제',
                    action: 'coatingContextMenuDelBtn'
                }
            ]
        });

        this.callParent(arguments);
    }
});