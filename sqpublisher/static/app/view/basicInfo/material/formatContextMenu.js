/**
 * Created by jiho on 15. 7. 14..
 *
 * 판형 > 그리드 내 컨텍스트 메뉴
 */
Ext.define('sqpub.view.basicInfo.material.formatContextMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.formatContextMenu',

    initComponent: function() {
        Ext.apply(this, {
            width: 100,
            items: [
                {
                    text: '삭제',
                    action: 'formatContextMenuDelBtn'
                }
            ]
        });

        this.callParent(arguments);
    }
});