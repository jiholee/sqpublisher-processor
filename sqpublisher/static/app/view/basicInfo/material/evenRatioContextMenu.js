/**
 * Created by jiho on 15. 7. 15..
 *
 * 손익분기 > 그리드 내 컨텍스트 메뉴
 */
Ext.define('sqpub.view.basicInfo.material.evenRatioContextMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.evenRatioContextMenu',

    initComponent: function() {
        Ext.apply(this, {
            width: 100,
            items: [
                {
                    text: '삭제',
                    action: 'evenRatioContextMenuDelBtn'
                }
            ]
        });

        this.callParent(arguments);
    }
});