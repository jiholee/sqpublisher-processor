/**
 * Created by jiho on 15. 7. 12..
 *
 * 절수 > 그리드 내 컨텍스트 메뉴
 */
Ext.define('sqpub.view.basicInfo.material.julsuContextMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.julsuContextMenu',

    initComponent: function() {
        Ext.apply(this, {
            width: 100,
            items: [
                {
                    text: '추가',
                    action: 'julsuContextMenuAddBtn'
                },
                {
                    text: '삭제',
                    action: 'julsuContextMenuDelBtn'
                },
                {
                    text: '저장',
                    action: 'julsuContextMenuSaveBtn'
                }
            ]
        });

        this.callParent(arguments);
    }
});