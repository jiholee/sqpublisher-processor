/**
 * Created by jiho on 14. 12. 7..
 *
 * 절수 콤보
 */
Ext.define('sqpub.view.basicInfo.material.julsuCombo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.julsuCombo',

    initComponent: function() {
        Ext.apply(this, {
            store: Ext.create('sqpub.store.basicInfo.material.storeJulsu'),
            queryMode: 'local',
            displayField: 'julsu',
            valueField: 'julsu'
        });

        this.callParent(arguments);
    }
});