/**
 * Created by jiho on 15. 7. 14..
 *
 * 구분 > 그리드 내 컨텍스트 메뉴
 */
Ext.define('sqpub.view.basicInfo.material.gubunContextMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.gubunContextMenu',

    initComponent: function() {
        Ext.apply(this, {
            width: 100,
            items: [
                {
                    text: '삭제',
                    action: 'gubunContextMenuDelBtn'
                }
            ]
        });

        this.callParent(arguments);
    }
});