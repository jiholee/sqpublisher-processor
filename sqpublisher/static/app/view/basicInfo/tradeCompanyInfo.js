/**
 * Created by jiho on 14. 11. 24..
 *
 * 거래처 상세 윈도우
 */
Ext.define('sqpub.view.basicInfo.tradeCompanyInfo', {
    extend: 'Ext.window.Window',
    alias: 'widget.basicInfo.tradeCompanyInfo',
    layout: 'fit',

    initComponent: function() {
        Ext.apply(this, {
            width: 580,
            border: false,
            style: {
                borderColor: 'white'
            },
            items: {
                xtype: 'form',
                layout: 'vbox',
                border: false,
                style: {
                    borderColor: 'white'
                },
                fieldDefaults: {
                    cls: 'field-margin',
                    width: 240
                },
                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'idx'
                    },
                    {
                        xtype: 'container',
                        layout: 'hbox',
                        margin: '15px 0 0 15px',
                        items: [
                            {
                                xtype: 'container',
                                layout: 'vbox',
                                margin: '0 10px 0 0',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: '상호',
                                        labelWidth: 60,
                                        name: 'name'
                                    }, {
                                        xtype: 'textfield',
                                        fieldLabel: '전화번호',
                                        labelWidth: 60,
                                        name: 'tel'
                                    }, {
                                        xtype: 'textfield',
                                        fieldLabel: '팩스',
                                        labelWidth: 60,
                                        name: 'fax'
                                    }, {
                                        xtype: 'textfield',
                                        fieldLabel: 'EMail',
                                        labelWidth: 60,
                                        name: 'email'
                                    }, {
                                        xtype: 'textfield',
                                        fieldLabel: '담당자',
                                        labelWidth: 60,
                                        name: 'manager'
                                    }, {
                                        xtype: 'textfield',
                                        fieldLabel: '핸드폰',
                                        labelWidth: 60,
                                        name: 'mobile'
                                    }, {
                                        xtype: 'textarea',
                                        fieldLabel: '비고',
                                        labelWidth: 60,
                                        width: 240,
                                        height: 50,
                                        name: 'etc'
                                    }
                                ]
                            }, {
                                xtype: 'container',
                                layout: 'vbox',
                                margin: '0 0 0 10px',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: '등록번호',
                                        labelWidth: 60,
                                        name: 'num'
                                    }, {
                                        xtype: 'textfield',
                                        fieldLabel: '성명',
                                        labelWidth: 60,
                                        name: 'ceo'
                                    }, {
                                        xtype: 'textarea',
                                        fieldLabel: '주소',
                                        labelWidth: 60,
                                        width: 240,
                                        height: 50,
                                        name: 'addr'
                                    }, {
                                        xtype: 'textfield',
                                        fieldLabel: '업태',
                                        labelWidth: 60,
                                        name: 'uptae'
                                    }, {
                                        xtype: 'textfield',
                                        fieldLabel: '종목',
                                        labelWidth: 60,
                                        name: 'jm'
                                    }, {
                                        xtype: 'combo',
                                        store: Ext.create('sqpub.store.basicInfo.storeTradeGubun'),
                                        queryMode: 'local',
                                        displayField: 'gubun',
                                        valueField: 'idx',
                                        name: 'gubun',
                                        fieldLabel: '구분',
                                        labelWidth: 60
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        layout: 'vbox',
                        margin: '15px 0 0 15px',
                        items: [
                            {
                                xtype: 'fieldcontainer',
                                width: 400,
                                layout: {
                                    type: 'hbox',
                                    align: 'middle'
                                },
                                fieldLabel: '금액 계산 방법',
                                labelWidth: 100,
                                items: [
                                    {
                                        xtype: 'combo',
                                        store: Ext.create('sqpub.store.common.storeMoneyCalc'),
                                        queryMode: 'local',
                                        displayField: 'name',
                                        valueField: 'idx',
                                        name: 'moneyCalcWay',
                                        width: 140
                                    },
                                    {
                                        xtype: 'label',
                                        text: '',
                                        margin: '0 0 0 8px'
                                    }
                                ]
                            }, {
                                xtype: 'fieldcontainer',
                                width: 400,
                                layout: {
                                    type: 'hbox',
                                    align: 'middle'
                                },
                                fieldLabel: '부가세 계산 방법',
                                labelWidth: 100,
                                items: [
                                    {
                                        xtype: 'combo',
                                        store: Ext.create('sqpub.store.common.storeMoneyCalc'),
                                        queryMode: 'local',
                                        displayField: 'name',
                                        valueField: 'idx',
                                        name: 'vatCalcWay',
                                        width: 140
                                    },
                                    {
                                        xtype: 'label',
                                        text: '',
                                        margin: '0 0 0 8px'
                                    }
                                ]
                            }, {
                                xtype: 'fieldcontainer',
                                width: 700,
                                layout: {
                                    type: 'hbox',
                                    align: 'middle'
                                },
                                fieldLabel: '부가세 종류',
                                labelWidth: 100,
                                items: [
                                    {
                                        xtype: 'combo',
                                        store: Ext.create('sqpub.store.common.storeVatTotalCalc'),
                                        queryMode: 'local',
                                        displayField: 'name',
                                        valueField: 'idx',
                                        name: 'vatCalcSel',
                                        width: 140
                                    },
                                    {
                                        xtype: 'label',
                                        text: '',
                                        margin: '0 0 0 8px'
                                    }
                                ]
                            }
                        ]
                    }
                ],
                bbar: [
                    '->',
                    {
                        text: '저장',
                        action: 'btnCompanyInfoSave'
                    }
                ]
            }
        });

        this.callParent(arguments);
    },

    loadRecord: function (record) {
        this.down("form").getForm().loadRecord(record);
    }
});