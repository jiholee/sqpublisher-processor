/**
 * Created by jiho on 14. 11. 24..
 *
 * 단가 > 제본_중철
 */
Ext.define('sqpub.view.basicInfo.cost.bindingChul', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.unitCost.bindingChul',
    title: '제본_중철',
    layout: 'vbox',
    items: [
        {
            xtype: 'container',
            html: '단가는 소비자에게 적용되며, 단가_원가는 매입원가나 외주시의 금액입니다.<br>' +
                '목록에 없는 판형은 먼저 판형등록을 하세요.<br>' +
                '단가는 꼭지당 단가를 말하며, 기본단가는 기본 꼭지가 되지 않을 경우 1부(권)당 단가입니다.<br>' +
                '판형, 종류가 한 쌍으로 중복되지 않도록 하세요',
            height: 90,
            width: '100%',
            cls: 'unit-cost-desc'
        },
        {
            xtype: 'gridpanel',
            store: Ext.create('sqpub.store.basicInfo.cost.storeBindingChul'),
            flex: 1,
            width: '100%',
            columns: [
                {
                    text: '판형',
                    dataIndex: 'format',
                    editor: {
                        xtype: 'combo',
                        store: Ext.create('sqpub.store.basicInfo.material.storeFormat'),
                        queryMode: 'local',
                        displayField: 'name'
                    }
                }, {
                    text: '종류',
                    dataIndex: 'method',
                    editor: {
                        xtype: 'combo',
                        store: Ext.create('sqpub.store.basicInfo.cost.storeBindMethod'),
                        queryMode: 'local',
                        displayField: 'method'
                    }
                }, {
                    text: '단가',
                    dataIndex: 'unitCost',
                    renderer: function(value) {
                        return Ext.util.Format.number(value, "0,0")
                    },
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '기본꼭지',
                    dataIndex: 'dltPage',
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '기본단가',
                    dataIndex: 'dltCost',
                    renderer: function(value) {
                        return Ext.util.Format.number(value, "0,0")
                    },
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '기본수량',
                    dataIndex: 'dltQuantity',
                    editor: {
                        xtype: 'textfield'
                    }
                }
            ],
            selType: 'cellmodel',
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 2
                })
            ],
            tbar: [
                {
                    xtype: 'tbspacer',
                    width: 668
                },
                {
                    xtype: 'button',
                    text: '추가',
                    action: 'btnAddBindingChulCost'
                },
                {
                    xtype: 'button',
                    text: '삭제',
                    action: 'btnDelBindingChulCost'
                },
                {
                    xtype: 'button',
                    text: '저장',
                    action: 'btnSaveBindingChulCost'
                }
            ]
        }
    ]
});