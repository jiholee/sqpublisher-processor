/**
 * Created by jiho on 14. 11. 22..
 *
 * 단가 > 인쇄
 */
Ext.define('sqpub.view.basicInfo.cost.print', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.unitCost.print',
    title: '인쇄',
    layout: 'vbox',
    items: [
        {
            xtype: 'container',
            html: '[시작연]은 그 연수부터 단가 얼마라는 뜻이며, 반드시 제일 처음은 0 연이 들어가 있어야 합니다.',
            height: 20,
            width: '100%',
            cls: 'unit-cost-desc'
        },
        {
            xtype: 'gridpanel',
            store: Ext.create('sqpub.store.basicInfo.cost.storePrint'),
            flex: 1,
            width: '100%',
            tbar: [
                {
                    xtype: 'factionCombo',
                    fieldLabel: '계열',
                    labelWidth: 40,
                    action: 'comboFactionCostPrint'
                },
                {
                    xtype: 'julsuCombo',
                    fieldLabel: '절수',
                    labelWidth: 40,
                    action: 'comboJulsuCostPrint'
                },
                {
                    xtype: 'button',
                    text: '전체보기',
                    action: 'btnAllViewCostPrint'
                },
                {
                    xtype: 'tbspacer',
                    width: 180
                },
                {
                    xtype: 'button',
                    text: '추가',
                    action: 'btnAddPrintCost'
                },
                {
                    xtype: 'button',
                    text: '삭제',
                    action: 'btnDelPrintCost'
                },
                {
                    xtype: 'button',
                    text: '저장',
                    action: 'btnSavePrintCost'
                }
            ],
            columns: [
                {
                    text: '계열',
                    dataIndex: 'faction',
                    editor: {
                        xtype: 'factionCombo'
                    }
                }, {
                    text: '절수',
                    dataIndex: 'julsu',
                    editor: {
                        xtype: 'julsuCombo'
                    }
                }, {
                    text: '시작연',
                    dataIndex: 'start',
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '단가',
                    dataIndex: 'unitCost',
                    renderer: function(value) {
                        return Ext.util.Format.number(value, "0,0")
                    },
                    editor: {
                        xtype: 'textfield'
                    }
                }
            ],
            selType: 'cellmodel',
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 2
                })
            ]
        }
    ]
});