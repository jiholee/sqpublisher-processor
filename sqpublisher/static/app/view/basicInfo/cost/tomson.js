/**
 * Created by jiho on 14. 11. 24..
 *
 * 톰슨
 */
Ext.define('sqpub.view.basicInfo.cost.tomson', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.unitCost.tomson',
    title: '톰슨',
    layout: 'vbox',
    items: [
        {
            xtype: 'container',
            html: '단가는 소비자에게 적용되며, 단가_원가는 매입원가나 외주시의 금액입니다.<br>' +
                '목록에 없는 절수는 직접 입력하시고, 톰슨종류, 계열, 절수가 한 쌍으로 중복되지 않도록 하세요.<br>' +
                '0절은 절수별 공통 단가로, 그외 경우는 해당 절수의 단가를 입력하시면 됩니다.',
            height: 70,
            width: '100%',
            cls: 'unit-cost-desc'
        },
        {
            xtype: 'gridpanel',
            store: Ext.create('sqpub.store.basicInfo.cost.storeTomson'),
            flex: 1,
            width: '100%',
            tbar: [
                {
                    xtype: 'combo',
                    store: Ext.create('sqpub.store.basicInfo.cost.storeUnitFilter'),
                    queryMode: 'local',
                    displayField: 'name',
                    valueField: 'value',
                    name: 'tomsonUnitFilterCombo',
                    fieldLabel: '종류',
                    value: '연',
                    labelWidth: 40
                },
                {
                    xtype: 'tbspacer',
                    width: 458
                },
                {
                    xtype: 'button',
                    text: '추가',
                    action: 'btnAddTomsonCost'
                },
                {
                    xtype: 'button',
                    text: '삭제',
                    action: 'btnDelTomsonCost'
                },
                {
                    xtype: 'button',
                    text: '저장',
                    action: 'btnSaveTomsonCost'
                }
            ],
            columns: [
                {
                    text: '톰슨종류',
                    dataIndex: 'method',
                    editor: {
                        xtype: 'combo',
                        store: Ext.create('sqpub.store.basicInfo.cost.storeTomsonUnit'),
                        queryMode: 'local',
                        displayField: 'method'
                    }
                }, {
                    text: '계열',
                    dataIndex: 'faction',
                    editor: {
                        xtype: 'factionCombo'
                    }
                }, {
                    text: '절수',
                    dataIndex: 'julsu',
                    editor: {
                        xtype: 'julsuCombo'
                    }
                }, {
                    text: '기본수량',
                    dataIndex: 'quantity',
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '단위',
                    dataIndex: 'unit',
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '단가',
                    dataIndex: 'unitCost',
                    renderer: function(value) {
                        return Ext.util.Format.number(value, "0,0")
                    },
                    editor: {
                        xtype: 'textfield'
                    }
                }
            ],
            selType: 'cellmodel',
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 2
                })
            ]
        }
    ]
});

