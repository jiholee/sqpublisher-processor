/**
 * Created by jiho on 14. 11. 22..
 *
 * 단가 > 스캔
 */
Ext.define('sqpub.view.basicInfo.cost.scan', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.unitCost.scan',
    title: '스캔',
    layout: 'vbox',
    items: [
        {
            xtype: 'container',
            html: '단가는 소비자에게 적용되며, 단가_원가는 매입원가나 외주시의 금액입니다.<br>' +
                '기본 Size는 가로 cm * 세로 cm를 말합니다<br><br>' +
                '이 단가표는 한 줄입니다.',
            height: 90,
            width: '100%',
            cls: 'unit-cost-desc'
        },
        {
            xtype: 'gridpanel',
            store: Ext.create('sqpub.store.basicInfo.cost.storeScan'),
            flex: 1,
            width: '100%',
            columns: [
                {
                    text: '기본 Size',
                    dataIndex: 'size',
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '기본 Size 단가',
                    dataIndex: 'sizeUnitCost',
                    renderer: function(value) {
                        return Ext.util.Format.number(value, "0,0")
                    },
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '평당단가',
                    dataIndex: 'pUnitCost',
                    renderer: function(value) {
                        return Ext.util.Format.number(value, "0,0")
                    },
                    editor: {
                        xtype: 'textfield'
                    }
                }
            ],
            selType: 'cellmodel',
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 2
                })
            ]
        }
    ]
});