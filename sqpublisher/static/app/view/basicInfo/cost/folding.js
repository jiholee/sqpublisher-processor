/**
 * Created by jiho on 14. 11. 24..
 *
 * 단가 > 접지
 */
Ext.define('sqpub.view.basicInfo.cost.folding', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.unitCost.folding',
    title: '접지',
    layout: 'vbox',
    items: [
        {
            xtype: 'container',
            html: '단가는 소비자에게 적용되며, 단가_원가는 매입원가나 외주시의 금액입니다.<br><br>' +
                '접지종류는 수정할 수 없습니다.<br>' +
                '이 단가표는 4줄입니다.',
            height: 90,
            width: '100%',
            cls: 'unit-cost-desc'
        },
        {
            xtype: 'gridpanel',
            store: Ext.create('sqpub.store.basicInfo.cost.storeFolding'),
            flex: 1,
            width: '100%',
            columns: [
                {
                    text: '접지종류',
                    dataIndex: 'name',
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '기본수량',
                    dataIndex: 'quantity',
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '단가',
                    dataIndex: 'unitCost',
                    renderer: function(value) {
                        return Ext.util.Format.number(value, "0,0")
                    },
                    editor: {
                        xtype: 'textfield'
                    }
                }
            ],
            selType: 'cellmodel',
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 2
                })
            ]
        }
    ]
});
