/**
 * Created by jiho on 14. 11. 22..
 *
 * 단가 > 편집
 */
Ext.define('sqpub.view.basicInfo.cost.edit', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.unitCost.edit',
    title: '편집',
    layout: 'vbox',
    items: [
        {
            xtype: 'container',
            html: '단가는 소비자에게 적용되며, 단가_원가는 매입원가나 외주시의 금액입니다.<br>' +
                '목록에 없는 절수는 직접 입력하시고, 계열, 절수가 한 쌍으로 중복되지 않도록 하세요.<br>' +
                '0절은 절수별 공통 단가로, 그외 경우는 해당 절수의 단가를 입력하시면 됩니다.',
            height: 70,
            width: '100%',
            cls: 'unit-cost-desc'
        },
        {
            xtype: 'gridpanel',
            store: Ext.create('sqpub.store.basicInfo.cost.storeEdit'),
            flex: 1,
            width: '100%',
            columns: [
                {
                    text: '계열',
                    dataIndex: 'faction',
                    editor: {
                        xtype: 'factionCombo'
                    }
                }, {
                    text: '절수',
                    dataIndex: 'julsu',
                    editor: {
                        xtype: 'julsuCombo'
                    }
                }, {
                    text: '단가',
                    dataIndex: 'unitCost',
                    renderer: function(value) {
                        return Ext.util.Format.number(value, "0,0")
                    },
                    editor: {
                        xtype: 'textfield'
                    }
                }
            ],
            selType: 'cellmodel',
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 2
                })
            ],
            tbar: [
                {
                    xtype: 'tbspacer',
                    width: 668
                },
                {
                    xtype: 'button',
                    text: '추가',
                    action: 'btnAddEditCost'
                },
                {
                    xtype: 'button',
                    text: '삭제',
                    action: 'btnDelEditCost'
                },
                {
                    xtype: 'button',
                    text: '저장',
                    action: 'btnSaveEditCost'
                }
            ]
        }
    ]
});