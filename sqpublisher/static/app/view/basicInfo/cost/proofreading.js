/**
 * Created by jiho on 14. 11. 22..
 *
 * 단가 > 교정인쇄
 */
Ext.define('sqpub.view.basicInfo.cost.proofreading', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.unitCost.proofreading',
    title: '교정인쇄',
    layout: 'vbox',
    items: [
        {
            xtype: 'container',
            html: '단가는 소비자에게 적용되며, 단가_원가는 매입원가나 외주시의 금액입니다.<br><br>' +
                '이 단가표는 한 줄입니다.',
            height: 70,
            width: '100%',
            cls: 'unit-cost-desc'
        },
        {
            xtype: 'gridpanel',
            store: Ext.create('sqpub.store.basicInfo.cost.storeProofreading'),
            flex: 1,
            width: '100%',
            columns: [
                {
                    text: '도수',
                    dataIndex: 'colorCnt',
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '단가',
                    dataIndex: 'unitCost',
                    renderer: function(value) {
                        return Ext.util.Format.number(value, "0,0")
                    },
                    editor: {
                        xtype: 'textfield'
                    }
                }
            ],
            selType: 'cellmodel',
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 2
                })
            ]
        }
    ]
});