/**
 * Created by jiho on 14. 11. 22..
 *
 * 단가 > 용지
 */
Ext.define('sqpub.view.basicInfo.cost.paper', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.unitCost.paper',
    title: '용지',
    layout: 'vbox',
    items: [
        {
            xtype: 'container',
            html: '종이종류에는 평량을 붙여서 입력하시면 되며, 중복되지 않도록 주의하세요.<br/>' +
                '할인율에 의해 단가는 자동계산됩니다.',
            height: 50,
            width: '100%',
            cls: 'unit-cost-desc'
        },
        {
            xtype: 'gridpanel',
            id: 'paperCostPanel',
            store: Ext.create('sqpub.store.basicInfo.cost.storePaper'),
            flex: 1,
            width: '100%',
            tbar: [
                {
                    xtype: 'textfield',
                    fieldLabel: '찾기',
                    labelWidth: 40,
                    name: 'find_textPaperCost'
                },
                {
                    xtype: 'tbspacer',
                    width: 458
                },
                {
                    xtype: 'button',
                    text: '추가',
                    action: 'btnAddPaperCost'
                },
                {
                    xtype: 'button',
                    text: '삭제',
                    action: 'btnDelPaperCost'
                },
                {
                    xtype: 'button',
                    text: '저장',
                    action: 'btnSavePaperCost'
                }
            ],
            columns: [
                {
                    text: '종이종류',
                    dataIndex: 'name',
                    width: 160,
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '계열',
                    dataIndex: 'faction',
                    editor: {
                        xtype: 'factionCombo'
                    }
                }, {
                    text: '단위',
                    dataIndex: 'paper_unit',
                    editor: {
                        xtype: 'textfield'
                    }
                }, {
                    text: '공장도가',
                    dataIndex: 'free_at_factory',
                    editor: {
                        xtype: 'textfield'
                    },
                    renderer: function(value) {
                        return Ext.util.Format.number(value, "0,0")
                    }
                }, {
                    text: '할인율',
                    dataIndex: 'discount',
                    editor: {
                        xtype: 'textfield'
                    },
                    renderer: function(value) {
                        return value.toString() + '%';
                    }
                }, {
                    text: '단가',
                    dataIndex: 'unitCost',
                    renderer: function(value) {
                        return Ext.util.Format.number(value, "0,0")
                    }
                }, {
                    text: '비고(가로*세로)',
                    dataIndex: 'etc',
                    width: 160,
                    editor: {
                        xtype: 'textfield'
                    }
                }
            ],
            selType: 'cellmodel',
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 2,
                    listeners: {
                        edit: function(editor, e, eOpts) {
                            // 단가 계산
                            var unit_cost_association_field = ["factory_price", "discount"];
                            if (unit_cost_association_field.indexOf(e.field) > -1) {
                                // 단가 계산에 필요한 field 값이 수정된 경우에 실행

                                var discount_value = e.record.get('discount');
                                var factory_price_value = e.record.get('free_at_factory');
                                var discount_price_value = factory_price_value * discount_value;

                                e.record.set('unitCost', factory_price_value - discount_price_value);
                            }
                        }
                    }
                })
            ]
        }
    ]
});