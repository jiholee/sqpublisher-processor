/**
 * Created by jiho on 14. 11. 25..
 *
 * 직원목록
 */
Ext.define('sqpub.view.basicInfo.employee.employeeList', {
    extend: 'Ext.container.Container',
    alias: 'widget.basicInfo.employeeList',

    initComponent: function() {
        Ext.apply(this, {
            layout: 'fit',
            title: '사원 목록',
            items: [
                {
                    xtype: 'gridpanel',
                    store: Ext.create('sqpub.store.basicInfo.storeEmployee'),
                    tbar: [
                        {
                            xtype: 'textfield',
                            fieldLabel: '사원명',
                            labelWidth: 80,
                            name: 'findEmployeeName'
                        },
                        '->',
                        {
                            xtype: 'button',
                            text: '추가',
                            action: 'btnEmployeeAdd'
                        },
                        {
                            xtype: 'button',
                            text: '삭제',
                            action: 'btnEmployeeDel'
                        }
                    ],
                    name: 'employeeList',
                    columns: [
                        {
                            text: 'ID',
                            dataIndex: 'uid'
                        },
                        {
                            text: '이름',
                            dataIndex: 'name'
                        },
                        {
                            text: '부서',
                            dataIndex: 'dept'
                        },
                        {
                            text: '직위',
                            dataIndex: 'position'
                        },
                        {
                            text: '전화번호',
                            dataIndex: 'tel'
                        },
                        {
                            text: '핸드폰',
                            dataIndex: 'hp'
                        },
                        {
                            text: 'EMail',
                            dataIndex: 'email'
                        },
                        {
                            text: '주소',
                            dataIndex: 'addr'
                        },
                        {
                            text: '우편번호',
                            dataIndex: 'post'
                        },
                        {
                            text: '비고',
                            dataIndex: 'etc'
                        }
                    ],
                    selType: 'cellmodel',
                    plugins: [
                        Ext.create('Ext.grid.plugin.CellEditing', {
                            clicksToEdit: 1
                        })
                    ]
                }
            ]
        });

        this.callParent(arguments);
    }
});