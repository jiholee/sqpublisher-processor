/**
 * Created by jiho on 15. 07. 20..
 *
 * 사원 목록
 */
Ext.define('sqpub.view.basicInfo.employee.employeeInfo', {
    extend: 'Ext.window.Window',
    alias: 'widget.basicInfo.employeeInfo',
    layout: 'fit',

    initComponent: function() {
        Ext.apply(this, {
            width: 400,
            border: false,
            style: {
                borderColor: 'white'
            },
            items: {
                xtype: 'form',
                layout: 'vbox',
                border: false,
                style: {
                    borderColor: 'white'
                },
                fieldDefaults: {
                    cls: 'field-margin',
                    width: 370
                },
                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'idx'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'ID',
                        labelWidth: 60,
                        name: 'uid',
                        width: '90%'
                    }, {
                        xtype: 'textfield',
                        fieldLabel: '비밀번호',
                        inputType: 'password',
                        labelWidth: 60,
                        name: 'password',
                        width: '90%'
                    }, {
                        xtype: 'textfield',
                        fieldLabel: '이름',
                        labelWidth: 60,
                        name: 'name',
                        width: '90%'
                    }, {
                        xtype: 'textfield',
                        fieldLabel: '부서',
                        labelWidth: 60,
                        name: 'dept',
                        width: '90%'
                    }, {
                        xtype: 'textfield',
                        fieldLabel: '직위',
                        labelWidth: 60,
                        name: 'position',
                        width: '90%'
                    }, {
                        xtype: 'textfield',
                        fieldLabel: '전화',
                        labelWidth: 60,
                        name: 'tel',
                        width: '90%'
                    }, {
                        xtype: 'textfield',
                        fieldLabel: '휴대전화',
                        labelWidth: 60,
                        name: 'hp',
                        width: '90%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: '이메일',
                        labelWidth: 60,
                        name: 'email',
                        width: '90%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: '주소',
                        labelWidth: 60,
                        name: 'addr',
                        width: '90%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: '우편번호',
                        labelWidth: 60,
                        name: 'post',
                        width: '90%'
                    }
                ],
                bbar: [
                    '->',
                    {
                        text: '저장',
                        action: 'btnEmployeeInfoCreateSave'
                    },
                    {
                        text: '저장',
                        action: 'btnEmployeeInfoEditSave'
                    }
                ]
            }
        });

        this.callParent(arguments);
    },

    loadRecord: function (record) {
        this.down("form").getForm().loadRecord(record);
    },
    
    enableCreateMode: function (createMode) {
        this.down("button[action=btnEmployeeInfoEditSave]").setVisible(!createMode);
    },

    enableModifyMode: function (modifyMode) {
        this.down("button[action=btnEmployeeInfoCreateSave]").setVisible(!modifyMode);
    }
});