/**
 * Created by jiho on 15. 7. 25..
 *
 * 분야 콤보박스
 */
Ext.define('sqpub.view.basicInfo.branchSeries.branchCombo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.branchCombo',

    initComponent: function () {
        Ext.apply(this, {
            store: Ext.create('sqpub.store.basicInfo.branchSeries.storeBranch'),
            queryMode: 'local',
            displayField: 'name',
            valueField: 'idx',
            fieldLabel: '분야'
        });

        this.addEvents("changeBranch");

        // 내부 리스너 생성
        this.on("change", function(comp, newValue, oldValue, eOpts) {
            var seriesCombo = Ext.ComponentQuery.query('seriesCombo[name='+this.seriesCombo + ']');
            this.fireEvent("changeBranch", comp, seriesCombo[0], newValue, oldValue, eOpts);
        });

        this.callParent(arguments);
    }
});
