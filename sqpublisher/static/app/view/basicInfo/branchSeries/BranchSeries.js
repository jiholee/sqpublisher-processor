/**
 * Created by jiho on 14. 11. 22..
 */
Ext.define('sqpub.view.basicInfo.branchSeries.BranchSeries', {
    extend: 'Ext.container.Container',
    alias: 'widget.BranchSeries',
    layout: 'hbox',

    title: '분야/시리즈',
    items: [
        {
            xtype: 'gridpanel',
            id: 'branch_panel',
            title: '분야',
            flex: 1,
            height: '100%',
            store: Ext.create('sqpub.store.basicInfo.branchSeries.storeBranch'),
            columns: [
                { text: '분야', dataIndex: 'name', flex: 1 }
            ],
            tbar: [
                '->',
                {
                    xtype: 'textfield',
                    fieldLabel: '분야명',
                    labelWidth: 60,
                    width: 220,
                    name: 'branchName'
                },
                {
                    xtype: 'button',
                    text: '추가',
                    action: 'branchAddBtn'
                },
                {
                    xtype: 'button',
                    text: '삭제',
                    action: 'branchDelBtn'
                },
                {
                    xtype: 'button',
                    text: '저장',
                    action: 'branchSaveBtn'
                }
            ],
            viewConfig: {
                plugins: [
                    {
                        ptype: 'gridviewdragdrop'
                    }
                ]
            }
        },
        {
            xtype: 'gridpanel',
            id: 'series_panel',
            title: '시리즈',
            flex: 1,
            height: '100%',
            store: Ext.create('sqpub.store.basicInfo.branchSeries.storeSeries'),
            columns: [
                { text: '시리즈', dataIndex: 'name', flex: 1 }
            ],
            tbar: [
                '->',
                {
                    xtype: 'textfield',
                    fieldLabel: '시리즈명',
                    labelWidth: 60,
                    width: 220,
                    name: 'seriesName'
                },
                {
                    xtype: 'button',
                    text: '추가',
                    action: 'seriesAddBtn'
                },
                {
                    xtype: 'button',
                    text: '삭제',
                    action: 'seriesDelBtn'
                },
                {
                    xtype: 'button',
                    text: '저장',
                    action: 'seriesSaveBtn'
                }
            ],
            viewConfig: {
                plugins: [
                    {
                        ptype: 'gridviewdragdrop'
                    }
                ]
            }
        }
    ]
});