/**
 * Created by jiho on 15. 7. 25..
 *
 * 시리즈 콤보
 */
Ext.define('sqpub.view.basicInfo.branchSeries.seriesCombo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.seriesCombo',

    initComponent: function () {
        Ext.apply(this, {
            store: Ext.create('sqpub.store.basicInfo.branchSeries.storeSeries'),
            queryMode: 'local',
            displayField: 'name',
            valueField: 'idx',
            fieldLabel: '시리즈'
        });

        this.callParent(arguments);
    }
});
