/**
 * Created by jiho on 14. 12. 7..
 *
 * 기초정보 > 기초자료 > 설정 컨테이너
 *
 * 작성이유: 컨테이너 파일 분리
 */
Ext.define('sqpub.view.basicInfo.settingItemContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.basicInfoSettingItemContainer',
    layout: 'card',
    height: '100%',
    flex: 1,
    items: [
        {
            xtype: 'panel',
            layout: 'hbox',
            items: [
                {
                    xtype: 'gridpanel',
                    id: 'process_panel',
                    flex: 1,
                    height: '100%',
                    title: '공정',
                    bbar: [
                        '[작업일수]만 수정하실 수 있습니다'
                    ],
                    store: Ext.create('sqpub.store.basicInfo.material.storeProcess'),
                    columns: [
                        { text: 'No',  dataIndex: 'idx' },
                        { text: '공정', dataIndex: 'name'},
                        { text: 'Type', dataIndex: 'p_type'},
                        {
                            text: '작업일수',
                            dataIndex: 'work_day',
                            editor: {
                                xtype: 'textfield'
                            }
                        }
                    ],
                    selType: 'cellmodel',
                    plugins: [
                        Ext.create('Ext.grid.plugin.CellEditing', {
                            clicksToEdit: 1
                        })
                    ]
                },
                {
                    xtype: 'gridpanel',
                    id: 'process_company',
                    flex: 1,
                    height: '100%',
                    title: '공정별 작업업체',
                    tbar: [
                        '->',
                        {
                            xtype: 'combo',
                            store: Ext.create('sqpub.store.basicInfo.storeTradeCompany'),
                            queryMode: 'local',
                            displayField: 'name',
                            valueField: 'idx',
                            name: 'trade_company_idx',
                            width: 250
                        },
                        {
                            xtype: 'button',
                            text: '추가',
                            action: 'btnProcessCompanyAdd'
                        },
                        {
                            xtype: 'button',
                            text: '삭제',
                            action: 'btnProcessCompanyDel'
                        },
                        {
                            xtype: 'button',
                            text: '저장',
                            action: 'btnProcessCompanySave'
                        }
                    ],
                    bbar: [
                        '지정되어 있지 않으면 자사가 기본 작업업체입니다.'
                    ],
                    store: Ext.create('sqpub.store.basicInfo.material.storeCompany'),
                    columns: [
                        {
                            text: '거래처',
                            dataIndex: 'trade_idx',
                            flex: 1,
                            editor: {
                                xtype: 'combo',
                                store: Ext.create('sqpub.store.basicInfo.storeTradeCompany'),
                                queryMode: 'local',
                                displayField: 'name',
                                valueField: 'idx',
                                name: 'trade_company_idx',
                                width: 250
                            },
                            renderer: function(value, metaData) {
                                var tradeStore = Ext.data.StoreManager.lookup('storeTradeCompany');
                                var find_record = tradeStore.getById(value);
                                return find_record.get('name');
                            }
                        }
                    ],
                    selType: 'cellmodel',
                    plugins: [
                        Ext.create('Ext.grid.plugin.CellEditing', {
                            clicksToEdit: 2
                        })
                    ]
                }
            ]
        }, {
            xtype: 'panel',
            title: '절수',
            layout: 'fit',
            items: [
                {
                    xtype: 'gridpanel',
                    id: 'julsu_panel',
                    bbar: [
                        '가끔 쓰는 절수는 추가하지 않아도 됩니다. 수정하실 때는 주의를 요합니다.'
                    ],
                    store: Ext.create('sqpub.store.basicInfo.material.storeJulsu'),
                    columns: [
                        {
                            text: '절수',
                            dataIndex: 'julsu',
                            editor: {
                                xtype: 'textfield',
                                allowBlank: false
                            }
                        }
                    ],
                    selType: 'cellmodel',
                    plugins: [
                        Ext.create('Ext.grid.plugin.CellEditing', {
                            clicksToEdit: 2
                        })
                    ]
                }
            ]
        }, {
            xtype: 'panel',
            title: '판형',
            layout: 'fit',
            items: [
                {
                    xtype: 'gridpanel',
                    id: 'format_panel',
                    bbar: [
                        '화면 순서를 바꾸시려면 드래그앤드롭 하세요. 목록에 없는 절수는 직접 입력하세요'
                    ],
                    store: Ext.create('sqpub.store.basicInfo.material.storeFormat'),
                    columns: [
                        {
                            text: 'No',
                            dataIndex: 'no',
                            renderer: function(value, metaData) {
                                return value + 1;
                            }
                        },
                        {
                            text: '판형',
                            dataIndex: 'name',
                            editor: {
                                xtype: 'textfield'
                            }
                        },
                        {
                            text: '계열',
                            dataIndex: 'faction',
                            editor: {
                                xtype: 'factionCombo'
                            }
                        },
                        {
                            text: '절수',
                            dataIndex: 'julsu',
                            editor: {
                                xtype: 'julsuCombo'
                            }
                        }
                    ],
                    selType: 'cellmodel',
                    plugins: [
                        Ext.create('Ext.grid.plugin.CellEditing', {
                            clicksToEdit: 2
                        })
                    ],
                    viewConfig: {
                        plugins: [
                            {
                                ptype: 'gridviewdragdrop',
                                dragGroup: 'format_panel',
                                dropGroup: 'format_panel'
                            }
                        ]
                    },
                    tbar: [
                        {
                            fieldLabel: '판형명',
                            labelWidth: 60,
                            xtype: 'textfield',
                            name: 'formatName',
                            width: 220
                        },
                        {
                            fieldLabel: '계열',
                            labelWidth: 40,
                            xtype: 'factionCombo',
                            name: 'factionCombo',
                            width: 130
                        },
                        {
                            fieldLabel: '절수',
                            labelWidth: 40,
                            xtype: 'julsuCombo',
                            name: 'julsuCombo',
                            width: 130
                        },
                        {
                            xtype: 'button',
                            action: 'btnFormatAdd',
                            text: '추가'
                        },
                        {
                            xtype: 'button',
                            action: 'btnFormatSave',
                            text: '저장'
                        }
                    ]
                }
            ]
        }, {
            xtype: 'panel',
            title: '구분',
            layout: 'fit',
            items: [
                {
                    xtype: 'gridpanel',
                    id: 'gubun_panel',
                    bbar: [
                        '화면 순서를 바꾸시려면 드래그앤드롭 하세요. 수정하실 때는 주의를 요합니다.'
                    ],
                    store: Ext.create('sqpub.store.basicInfo.material.storeSeparation'),
                    columns: [
                        {
                            text: 'No',
                            dataIndex: 'no',
                            renderer: function(value, metaData) {
                                return value + 1;
                            }
                        },
                        {
                            text: '구분',
                            dataIndex: 'name',
                            editor: {
                                xtype: 'textfield',
                                allowBlank: false
                            },
                            width: 200
                        }
                    ],
                    selType: 'cellmodel',
                    plugins: [
                        Ext.create('Ext.grid.plugin.CellEditing', {
                            clicksToEdit: 2
                        })
                    ],
                    viewConfig: {
                        plugins: [
                            {
                                ptype: 'gridviewdragdrop',
                                dragGroup: 'gubun_panel',
                                dropGroup: 'gubun_panel'
                            }
                        ]
                    },
                    tbar: [
                        {
                            fieldLabel: '구분명',
                            labelWidth: 60,
                            xtype: 'textfield',
                            name: 'gubunName',
                            width: 220
                        },
                        {
                            xtype: 'button',
                            action: 'btnGubunAdd',
                            text: '추가'
                        },
                        {
                            xtype: 'button',
                            action: 'btnGubunSave',
                            text: '저장'
                        }
                    ]
                }
            ]
        }, {
            xtype: 'panel',
            title: '코팅',
            layout: 'fit',
            items: [
                {
                    xtype: 'gridpanel',
                    id: 'coating_panel',
                    bbar: [
                        '수정하실 때는 주의를 요합니다.'
                    ],
                    store: Ext.create('sqpub.store.basicInfo.material.storeCoating'),
                    columns: [
                        {
                            text: '코팅종류',
                            dataIndex: 'name',
                            editor: {
                                xtype: 'textfield',
                                allowBlank: false
                            },
                            flex: 1
                        }
                    ],
                    selType: 'cellmodel',
                    plugins: [
                        Ext.create('Ext.grid.plugin.CellEditing', {
                            clicksToEdit: 2
                        })
                    ],
                    tbar: [
                        {
                            fieldLabel: '코팅명',
                            labelWidth: 60,
                            xtype: 'textfield',
                            name: 'coatingName',
                            width: 220
                        },
                        {
                            xtype: 'button',
                            action: 'btnCoatingAdd',
                            text: '추가'
                        },
                        {
                            xtype: 'button',
                            action: 'btnCoatingSave',
                            text: '저장'
                        }
                    ]
                }
            ]
        }, {
            xtype: 'panel',
            title: '손익분기',
            layout: 'fit',
            items: [
                {
                    xtype: 'gridpanel',
                    id: 'evenratio_panel',
                    bbar: [
                        '[공급율]은 삭제할 수 없습니다. 백분율은 소수 2자리까지 입니다.'
                    ],
                    store: Ext.create('sqpub.store.basicInfo.material.storeBreakEvenRatio'),
                    columns: [
                        {
                            text: 'Name',
                            dataIndex: 'name',
                            editor: {
                                xtype: 'textfield',
                                allowBlank: false
                            }
                        },
                        {
                            text: '비율',
                            dataIndex: 'ratio',
                            editor: {
                                xtype: 'textfield',
                                allowBlank: false
                            },
                            renderer: function(value) {
                                var ratio = parseInt(parseFloat(value) * 100);
                                return ratio.toString() + '%';
                            }
                        }
                    ],
                    selType: 'cellmodel',
                    plugins: [
                        Ext.create('Ext.grid.plugin.CellEditing', {
                            clicksToEdit: 2
                        })
                    ],
                    tbar: [
                        {
                            fieldLabel: '손익분기명',
                            labelWidth: 60,
                            xtype: 'textfield',
                            name: 'evenRatioName',
                            width: 220
                        },
                        {
                            fieldLabel: '비율',
                            labelWidth: 40,
                            xtype: 'textfield',
                            name: 'evenRatio',
                            width: 90
                        },
                        {
                            xtype: 'button',
                            action: 'btnEvenRatioAdd',
                            text: '추가'
                        },
                        {
                            xtype: 'button',
                            action: 'btnEvenRatioSave',
                            text: '저장'
                        }
                    ]
                }
            ]
        }
    ]
});