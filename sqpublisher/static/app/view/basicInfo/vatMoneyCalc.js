/**
 * Created by jiho on 14. 12. 8..
 *
 * 부가세 계산 방법 필드 컨테이너
 */
Ext.define('sqpub.view.basicInfo.vatMoneyCalc', {
    extend: 'Ext.form.FieldContainer',
    alias: 'widget.basicVatMoneyCalc',
    width: 400,
    layout: {
        type: 'hbox',
        align: 'middle'
    },
    fieldLabel: '부가세 계산 방법',
    labelWidth: 100,

    initComponent: function() {
        Ext.apply(this, {
            items: [
                {
                    xtype: 'combo',
                    store: Ext.create('sqpub.store.common.storeMoneyCalc'),
                    queryMode: 'local',
                    displayField: 'name',
                    valueField: 'idx',
                    name: this.ComboFieldName,
                    width: 140
                },
                {
                    xtype: 'label',
                    text: '',
                    margin: '0 0 0 8px'
                }
            ]
        });

        this.callParent(arguments);
    }
});