/**
 * Created by jiho on 14. 11. 19..
 */
Ext.define('sqpub.view.basicInfo.aboutUs.AboutUsWin', {
    extend: 'Ext.form.Panel',
    alias: 'widget.AboutUsWin',

    title: '자사정보',
    id: 'baseCompanyInfoFormPanel',
    layout: 'vbox',
    fieldDefaults: {
        cls: 'field-margin',
        width: 240
    },

    initComponent: function() {
        Ext.apply(this, {
            items: [{
                xtype: 'container',
                layout: 'hbox',
                margin: '15px 0 0 15px',
                items: [
                    {
                        xtype: 'container',
                        layout: 'vbox',
                        margin: '0 10px 0 0',
                        items: [
                            {
                                xtype: 'textfield',
                                fieldLabel: '상호',
                                name: 'name',
                                labelWidth: 60
                            }, {
                                xtype: 'textfield',
                                fieldLabel: '전화번호',
                                name: 'tel',
                                labelWidth: 60
                            }, {
                                xtype: 'textfield',
                                fieldLabel: '팩스',
                                name: 'fax',
                                labelWidth: 60
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'EMail',
                                name: 'email',
                                labelWidth: 60
                            }, {
                                xtype: 'textfield',
                                fieldLabel: '담당자',
                                name: 'manager',
                                labelWidth: 60
                            }, {
                                xtype: 'textfield',
                                fieldLabel: '핸드폰',
                                name: 'mobile',
                                labelWidth: 60
                            }
                        ]
                    }, {
                        xtype: 'container',
                        layout: 'vbox',
                        margin: '0 0 0 10px',
                        items: [
                            {
                                xtype: 'textfield',
                                fieldLabel: '등록번호',
                                name: 'num',
                                labelWidth: 60
                            }, {
                                xtype: 'textfield',
                                fieldLabel: '성명',
                                name: 'ceo',
                                labelWidth: 60
                            }, {
                                xtype: 'textarea',
                                fieldLabel: '주소',
                                name: 'addr',
                                labelWidth: 60,
                                width: 240,
                                height: 50
                            }, {
                                xtype: 'textfield',
                                fieldLabel: '업태',
                                name: 'uptae',
                                labelWidth: 60
                            }, {
                                xtype: 'textfield',
                                fieldLabel: '종목',
                                name: 'jm',
                                labelWidth: 60
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'container',
                layout: 'vbox',
                margin: '15px 0 0 15px',
                items: [
                    {
                        xtype: 'basicMoneyCalc',
                        ComboFieldName: 'moneyCalcWay'
                    }, {
                        xtype: 'basicVatMoneyCalc',
                        ComboFieldName: 'vatCalcWay'
                    }, {
                        xtype: 'basicVatMethod',
                        ComboFieldName: 'vatCalcSel'
                    }
                ]
            }],
            tbar: [
                {
                    text: '저장',
                    action: 'btnBasicCompanyInfoSave'
                }
            ]
        });

        this.callParent(arguments);
    }
});