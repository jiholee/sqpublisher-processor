/**
 * filename rename
 */
Ext.define('sqpub.view.Dashboard', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.Dashboard',

    initComponent: function() {
        Ext.apply(this, {
            title: '대시보드'
        });

        this.callParent(arguments);
    }
});