__author__ = 'jiho'
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import Column, Integer, String, Sequence, ForeignKey, Text, Boolean, asc, desc, Float
from sqlalchemy.orm import relationship, backref
from sqpublisher.database import Base, db_session
from hashlib import sha1


class SQPublisherBaseMixin:
    sortDirectionKey = {"ASC": asc, "DESC": desc}

    @classmethod
    def sort_column(cls, column_name):
        column_attr = getattr(cls, column_name, '')
        if column_attr:
            return column_attr
        else:
            raise KeyError('%s Colums not in Model' % column_name)

    @classmethod
    def sort_direction(cls, direction_str):
        return cls.sortDirectionKey[direction_str]


class UserRoles(SQPublisherBaseMixin, Base):
    __tablename__ = "tblUserRoles"

    id = Column(Integer(), primary_key=True)
    user_id = Column(String(50), ForeignKey('tblUsers.uid', ondelete='CASCADE'))
    role_id = Column(Integer(), ForeignKey('tblRole.idx', ondelete='CASCADE'))


# 사원정보
class User(SQPublisherBaseMixin, Base):
    __tablename__ = 'tblUsers'
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    uid = Column(String(50), primary_key=True)
    password = Column(String(40))
    name = Column(String(50))
    email = Column(String(120), unique=True)
    dept = Column(String(50))
    position = Column(String(50))
    tel = Column(String(50))
    hp = Column(String(50))
    addr = Column(String(50))
    post = Column(String(50))
    etc = Column(String(50))

    roles = relationship('Role', secondary='tblUserRoles',
                         backref=backref('users', lazy='dynamic'))

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

        self.password = sha1(kwargs['password'].encode("utf-8")).hexdigest()

    def populate(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

        if 'password' in kwargs:
            self.password = sha1(kwargs['password'].encode("utf-8")).hexdigest()

    def password_encryption(self, password):
        self.password = sha1(password.encode("utf-8")).hexdigest()

    def __repr__(self):
        return '<User %r>' % self.uid

    # 사용자가 입력한 패스워드를 받아 일치하는지 확인
    def password_eq(self, try_password):
        try_password = sha1(try_password.encode("utf-8")).hexdigest()

        if try_password == self.password:
            return True
        else:
            return False

    def to_dict(self):
        return dict(
            idx=self.idx,
            uid=self.uid,
            name=self.name,
            email=self.email,
            dept=self.dept,
            position=self.position,
            tel=self.tel,
            hp=self.hp,
            addr=self.addr,
            post=self.post
        )


class Role(SQPublisherBaseMixin, Base):
    __tablename__ = "tblRole"

    idx = Column(Integer(), Sequence('role_idx_seq'), primary_key=True)
    name = Column(String(50), unique=True)

    def __init__(self, name):
        self.name = name


# 자사정보
class BaseCompanyMdl(SQPublisherBaseMixin, Base):
    __tablename__ = 'tblBaseCompanyInfo'
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    name = Column(String(40), doc="회사명")
    ceo = Column(String(40), doc="대표이사명")
    tel = Column(String(40), doc="전화번호")
    fax = Column(String(40), doc="팩스")
    num = Column(String(40), doc="사업자번호")
    email = Column(String(60), doc="이메일")
    addr = Column(String(100), doc="주소")
    manager = Column(String(40), doc="담당자")
    mobile = Column(String(40), doc="휴대전화")
    uptae = Column(String(40), doc="업태")
    jm = Column(String(40), doc="종목")
    moneyCalcWay = Column(String(1), doc="금액 계산 방법")
    vatCalcWay = Column(String(1), doc="부가세 계산 방법")
    vatCalcSel = Column(String(1), doc="부가세 종류")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            name=self.name or '',
            ceo=self.ceo or '',
            tel=self.tel or '',
            fax=self.fax or '',
            num=self.num or '',
            email=self.email or '',
            addr=self.addr or '',
            manager=self.manager or '',
            mobile=self.mobile or '',
            uptae=self.uptae or '',
            jm=self.jm or '',
            moneyCalcWay=self.moneyCalcWay or '',
            vatCalcWay=self.vatCalcWay or '',
            vatCalcSel=self.vatCalcSel or ''
        )


# 거래처정보
class TradeCompanyMdl(SQPublisherBaseMixin, Base):
    __tablename__ = 'tblTradeCompanyInfo'
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    name = Column(String(40), doc="회사명")
    ceo = Column(String(40), doc="대표이사명")
    tel = Column(String(40), doc="전화번호")
    fax = Column(String(40), doc="팩스")
    num = Column(String(40), doc="사업자번호")
    email = Column(String(60), doc="이메일")
    addr = Column(String(100), doc="주소")
    manager = Column(String(40), doc="담당자")
    mobile = Column(String(40), doc="휴대전화")
    uptae = Column(String(40), doc="업태")
    jm = Column(String(40), doc="종목")
    etc = Column(Text, doc="비고")
    gubun = Column(String(10), doc="거래처 구분")
    moneyCalcWay = Column(String(1), doc="금액 계산 방법")
    vatCalcWay = Column(String(1), doc="부가세 계산 방법")
    vatCalcSel = Column(String(1), doc="부가세 종류")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx or '',
            name=self.name or '',
            ceo=self.ceo or '',
            tel=self.tel or '',
            fax=self.fax or '',
            num=self.num or '',
            email=self.email or '',
            addr=self.addr or '',
            manager=self.manager or '',
            mobile=self.mobile or '',
            uptae=self.uptae or '',
            jm=self.jm or '',
            etc=self.etc or '',
            gubun=self.gubun or '',
            moneyCalcWay=self.moneyCalcWay or '',
            vatCalcWay=self.vatCalcWay or '',
            vatCalcSel=self.vatCalcSel or ''
        )


# 공정 목록
class ProcessMdl(SQPublisherBaseMixin, Base):
    __tablename__ = 'tblProcess'
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    name = Column(String(40), doc="공정명")
    p_type = Column(String(10), doc="공정 Type")
    cost_using = Column(Boolean, doc="단가표 사용")
    work_day = Column(Integer, doc="작업일수")

    def __init__(self, name, p_type, cost_using, work_day):
        self.name = name
        self.p_type = p_type
        self.cost_using = cost_using
        self.work_day = work_day

    def to_dict(self):
        return dict(
            idx=self.idx or '',
            name=self.name or '',
            p_type=self.p_type or '',
            cost_using=self.cost_using or False,
            work_day=self.work_day or ''
        )


# 공정별 작업 업체
class ProcessCompanyMdl(SQPublisherBaseMixin, Base):
    __tablename__ = 'tblProcessCompany'
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    process_idx = Column(Integer, doc="공정 고유번호")
    trade_idx = Column(Integer, doc="거래처 고유번호")

    def to_dict(self):
        return dict(
            idx=self.idx,
            process_idx=self.process_idx,
            trade_idx=self.trade_idx,
            trade_name=self.trade_name
        )

    @hybrid_property
    def trade_name(self):
        trade_entry = db_session.query(TradeCompanyMdl).filter(TradeCompanyMdl.idx == self.trade_idx).first()
        if trade_entry:
            return trade_entry.name
        else:
            return ''


# 절수
class JulsuMdl(SQPublisherBaseMixin, Base):
    __tablename__ = 'tblJulsu'
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    julsu = Column(Integer, doc="절수")

    def to_dict(self):
        return dict(
            idx=self.idx,
            julsu=self.julsu
        )


# 판형
class FormatMdl(SQPublisherBaseMixin, Base):
    __tablename__ = 'tblFormat'
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    no = Column(Integer, doc="화면 순서")
    name = Column(String(20), doc="판형")
    faction = Column(String(10), doc="계열")
    julsu = Column(Integer, doc="절수")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            no=self.no,
            name=self.name,
            faction=self.faction,
            julsu=self.julsu
        )


# 구분
class GubunMdl(SQPublisherBaseMixin, Base):
    __tablename__ = 'tblGubun'
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    no = Column(Integer, doc="화면 순서")
    name = Column(String(30), doc="구분명")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            no=self.no,
            name=self.name
        )


# 코팅
class CoatingMdl(SQPublisherBaseMixin, Base):
    __tablename__ = 'tblCoating'
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    name = Column(String(30), doc="코팅명")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            name=self.name
        )


# 손익분기
class BreakEvenRatioMdl(SQPublisherBaseMixin, Base):
    __tablename__ = 'tblBreakEvenRatio'
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    name = Column(String(30), doc="Name")
    ratio = Column(Float, doc="손익분기 비율")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            name=self.name,
            ratio=self.ratio
        )


# 분야
class BranchMdl(SQPublisherBaseMixin, Base):
    __tablename__ = 'tblBranch'
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    no = Column(Integer, doc="화면 순서")
    name = Column(String(50), doc="분야명")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            no=self.no,
            name=self.name
        )


# 시리즈
class SeriesMdl(SQPublisherBaseMixin, Base):
    __tablename__ = 'tblSeries'
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    branch_idx = Column(Integer, doc="분야 코드")
    no = Column(Integer, doc="화면 순서")
    name = Column(String(50), doc="시리즈명")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            branch_idx=self.branch_idx,
            no=self.no,
            name=self.name
        )


# 단가 - 종이
class CostPaperMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostPaper"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    name = Column(String(100), doc="종이종류")
    faction = Column(String(10), doc="계열")
    paper_unit = Column(String(10), doc="단위")
    free_at_factory = Column(Integer, doc="공장도가")
    discount = Column(Float, doc="할인율")
    unitCost = Column(Float, doc="단가_원가")
    etc = Column(Text, doc="비고")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            name=self.name,
            faction=self.faction,
            paper_unit=self.paper_unit,
            free_at_factory=self.free_at_factory,
            discount=self.discount,
            unitCost=self.unitCost,
            etc=self.etc
        )


# 단가 - 마스타제판
class CostMasterPlateMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostMasterPlate"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    faction = Column(String(40), doc="계열")
    julsu = Column(Integer, doc="절수")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            faction=self.faction,
            julsu=self.julsu,
            unitCost=self.unitCost
        )


# 단가 - 마스타인쇄
class CostMasterPrintMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostMasterPrint"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    faction = Column(String(40), doc="계열")
    julsu = Column(Integer, doc="절수")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            faction=self.faction,
            julsu=self.julsu,
            unitCost=self.unitCost
        )


# 단가 - 스캔
class CostScanMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostScan"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    size = Column(Integer, doc="기본 Size")
    sizeUnitCost = Column(Integer, doc="기본 Size 단가")
    pUnitCost = Column(Integer, doc="평당 단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            size=self.size,
            sizeUnitCost=self.sizeUnitCost,
            pUnitCost=self.pUnitCost
        )


# 단가 - 편집
class CostEditMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostEdit"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    faction = Column(String(40), doc="계열")
    julsu = Column(Integer, doc="절수")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            faction=self.faction,
            julsu=self.julsu,
            unitCost=self.unitCost
        )


# 단가 - 시안
class CostDraftMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostDraft"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    faction = Column(String(40), doc="계열")
    julsu = Column(Integer, doc="절수")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            faction=self.faction,
            julsu=self.julsu,
            unitCost=self.unitCost
        )


# 단가 - 교정인쇄
class CostProofreadingMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostProofreading"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    colorCnt = Column(Integer, doc="도수")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            colorCnt=self.colorCnt,
            unitCost=self.unitCost
        )


# 단가 - 필름출력
class CostFilmPrintMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostFilmPrint"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    faction = Column(String(40), doc="계열")
    julsu = Column(Integer, doc="절수")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            faction=self.faction,
            julsu=self.julsu,
            unitCost=self.unitCost
        )


# 단가 - 대첩
class CostDaechubMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostDaechub"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    faction = Column(String(40), doc="계열")
    julsu = Column(Integer, doc="절수")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            faction=self.faction,
            julsu=self.julsu,
            unitCost=self.unitCost
        )


# 단가 - 소부
class CostSobuMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostSobu"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    faction = Column(String(40), doc="계열")
    julsu = Column(Integer, doc="절수")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            faction=self.faction,
            julsu=self.julsu,
            unitCost=self.unitCost
        )


# 단가 - 인쇄
class CostPrintMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostPrint"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    faction = Column(String(40), doc="계열")
    julsu = Column(Integer, doc="절수")
    start = Column(Integer, doc="시작연")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            faction=self.faction,
            julsu=self.julsu,
            start=self.start,
            unitCost=self.unitCost
        )


# 단가 - 미싱
class CostSewingMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostSewing"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    faction = Column(String(40), doc="계열")
    julsu = Column(Integer, doc="절수")
    quantity = Column(Integer, doc="기본수량")
    unit = Column(String(40), doc="단위")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            faction=self.faction,
            julsu=self.julsu,
            quantity=self.quantity,
            unit=self.unit,
            unitCost=self.unitCost
        )


# 단가 - 오시
class CostOshiMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostOshi"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    faction = Column(String(40), doc="계열")
    julsu = Column(Integer, doc="절수")
    quantity = Column(Integer, doc="기본수량")
    unit = Column(String(40), doc="단위")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            faction=self.faction,
            julsu=self.julsu,
            quantity=self.quantity,
            unit=self.unit,
            unitCost=self.unitCost
        )


# 단가 - 코팅
class CostCoatingMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostCoating"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    name = Column(String(40), doc="코팅종류")
    faction = Column(String(40), doc="계열")
    julsu = Column(Integer, doc="절수")
    quantity = Column(Integer, doc="기본수량")
    unit = Column(String(40), doc="단위")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            name=self.name,
            faction=self.faction,
            julsu=self.julsu,
            quantity=self.quantity,
            unit=self.unit,
            unitCost=self.unitCost
        )


# 단가 - 톰슨
class CostTomsonMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostTomson"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    method = Column(String(40), doc="톰슨종류")
    faction = Column(String(40), doc="계열")
    julsu = Column(Integer, doc="절수")
    quantity = Column(Integer, doc="기본수량")
    unit = Column(String(40), doc="단위")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            method=self.method,
            faction=self.faction,
            julsu=self.julsu,
            quantity=self.quantity,
            unit=self.unit,
            unitCost=self.unitCost
        )


# 단가 - 접지
class CostFoldingMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostFolding"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    name = Column(String(40), doc="접지종류")
    quantity = Column(Integer, doc="기본수량")
    unitCost = Column(Integer, doc="단가")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            name=self.name,
            quantity=self.quantity,
            unitCost=self.unitCost
        )


# 단가 - 제본_무선
class CostBindingWireMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostBindingWire"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    format = Column(String(40), doc="판형")
    method = Column(String(20), doc="종류")
    unitCost = Column(Integer, doc="단가")
    dltPage = Column(Integer, doc="기본쪽수")
    dltCost = Column(Integer, doc="기본단가")
    dltQuantity = Column(Integer, doc="기본수량")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            format=self.format,
            method=self.method,
            unitCost=self.unitCost,
            dltPage=self.dltPage,
            dltCost=self.dltCost,
            dltQuantity=self.dltQuantity
        )


# 단가 - 제본_중철
class CostBindingChulMdl(SQPublisherBaseMixin, Base):
    __tablename__ = "tblCostBindingChul"
    __table_args__ = {'postgresql_with_oids': True}

    idx = Column(Integer, primary_key=True)
    format = Column(String(40), doc="판형")
    method = Column(String(20), doc="종류")
    unitCost = Column(Integer, doc="단가")
    dltPage = Column(Integer, doc="기본꼭지")
    dltCost = Column(Integer, doc="기본단가")
    dltQuantity = Column(Integer, doc="기본수량")

    def __init__(self, **kwargs):
        for entry_name, entry_value in kwargs.items():
            setattr(self, entry_name, entry_value)

    def to_dict(self):
        return dict(
            idx=self.idx,
            format=self.format,
            method=self.method,
            unitCost=self.unitCost,
            dltPage=self.dltPage,
            dltCost=self.dltCost,
            dltQuantity=self.dltQuantity
        )
