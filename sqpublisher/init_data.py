__author__ = 'jiho'

# 초기화 데이터 생성용
from sqpublisher.database import db_session
from sqpublisher.models import ProcessMdl

# 공정별 초기화 데이터
def process_init_data():
    insert_ready_data = [
        dict(name='용지', p_type='용지', cost_using=True, work_day=2),
        dict(name='마스타제판', p_type='PrePress', cost_using=True, work_day=1),
        dict(name='마스타인쇄', p_type='인쇄', cost_using=True, work_day=1),
        dict(name='스캔', p_type='PrePress', cost_using=True, work_day=2),
        dict(name='이미지대여', p_type='PrePress', cost_using=False, work_day=1),
        dict(name='편집', p_type='PrePress', cost_using=True, work_day=1),
        dict(name='시안', p_type='PrePress', cost_using=True, work_day=1),
        dict(name='교정인쇄', p_type='PrePress', cost_using=True, work_day=1),
        dict(name='필름출력', p_type='PrePress', cost_using=True, work_day=1),
        dict(name='대첩', p_type='PrePress', cost_using=True, work_day=1),
        dict(name='소부', p_type='PrePress', cost_using=True, work_day=1),
        dict(name='인쇄', p_type='인쇄', cost_using=True, work_day=2),
        dict(name='재단', p_type='후가공', cost_using=False, work_day=1),
        dict(name='금박', p_type='후가공', cost_using=False, work_day=2),
        dict(name='형압', p_type='후가공', cost_using=False, work_day=2),
        dict(name='미싱', p_type='후가공', cost_using=True, work_day=2),
        dict(name='오시', p_type='후가공', cost_using=True, work_day=2),
        dict(name='코팅', p_type='후가공', cost_using=True, work_day=2),
        dict(name='톰슨', p_type='후가공', cost_using=True, work_day=2),
        dict(name='접지', p_type='제본', cost_using=True, work_day=2),
        dict(name='제본_떡제본', p_type='제본', cost_using=False, work_day=2),
        dict(name='제본_무선', p_type='제본', cost_using=True, work_day=3),
        dict(name='제본_중철', p_type='제본', cost_using=True, work_day=3),
        dict(name='특수제본', p_type='제본', cost_using=False, work_day=5),
        dict(name='기타', p_type='기타', cost_using=False, work_day=1),
    ]

    for entry in insert_ready_data:
        entry_model = ProcessMdl(**entry)
        db_session.add(entry_model)

    db_session.commit()

if __name__ == "__main__":
    process_init_data()
